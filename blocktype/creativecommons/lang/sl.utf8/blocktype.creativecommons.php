<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['alttext'] = 'Licenca Creative Commons';
$string['blockcontent'] = 'Vsebina bloka';
$string['by'] = 'Priznanje avtorstva';
$string['by-nc'] = 'Priznanje avtorstva-Nekomercialno';
$string['by-nc-nd'] = 'Priznanje avtorstva-Nekomercialno-Brez predelav';
$string['by-nc-sa'] = 'Priznanje avtorstva-Nekomercialno-Deljenje pod istimi pogoji';
$string['by-nd'] = 'Priznanje avtorstva-Brez predelav';
$string['by-sa'] = 'Priznanje avtorstva-Deljenje pod istimi pogoji';
$string['config:noderivatives'] = 'Dovolite predelave vašega dela?';
$string['config:noncommercial'] = 'Dovolite komercialno uporabo vašega dela?';
$string['config:sharealike'] = 'Da, dokler drugi delijo pod istimi pogoji';
$string['description'] = 'K pogledu pripnite licenco Creative Commons';
$string['licensestatement'] = 'To delo je licencirano pod <a rel="license" href="%s">Creative Commons %s 3.0 neprenosljivo licenco</a>.';
$string['sealalttext'] = 'Ta licenca je sprejemljiva za Prosto dostopna kulturna dela';
$string['title'] = 'Licenca Creative Commons';
?>
