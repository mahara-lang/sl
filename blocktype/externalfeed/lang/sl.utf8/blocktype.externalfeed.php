<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Zunanji vir';
$string['description'] = 'Vgradite zunanji RSS ali ATOM vir';
$string['feedlocation'] = 'Lokacija vira';
$string['feedlocationdesc'] = 'URL veljavnega RSS ali ATOM vira';
$string['showfeeditemsinfull'] = 'Prikažem celotne enote vira?';
$string['showfeeditemsinfulldesc'] = 'Ali prikažem povzetke enot vira, ali naj vključim tudi opis vsake enote';
$string['invalidurl'] = 'URL ne neustrezen. Ogledate si lahko le vire s http in https URLji.';
$string['invalidfeed'] = 'Izgleda, da gre za neveljaven vir. Gre za naslednjo napako: %s';
$string['lastupdatedon'] = 'Zadnja posodobitev %s';
$string['defaulttitledescription'] = 'Če pustite tole prazno, bo uporabljen naslov vira';
?>
