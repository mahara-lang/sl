<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Post'] = 'Objava';
$string['backtoprofile'] = 'Nazaj na profil';
$string['delete'] = 'izbriši objavo';
$string['deletepost'] = 'Izbriši objavo';
$string['deletepostsuccess'] = 'Objava uspešno izbrisana';
$string['deletepostsure'] = 'Ali ste prepričani? Tega ne bo mogoče razveljaviti.';
$string['description'] = 'Prostor za komentarje';
$string['makeyourpostprivate'] = 'Naj bo objava zasebna?';
$string['maxcharacters'] = 'Največ %s znakov na objavo.';
$string['noposts'] = 'Na zidu ni nobenih objav';
$string['otherusertitle'] = 'Zid osebe %s';
$string['postsizelimit'] = 'Omejitev velikosti objave';
$string['postsizelimitdescription'] = 'Tukaj lahko omejite velikost objav na zidu. Obstoječe objave ne bodo spremenjene.';
$string['postsizelimitinvalid'] = 'To ni veljavno število.';
$string['postsizelimitmaxcharacters'] = 'Največje dovoljeno število znakov';
$string['postsizelimittoosmall'] = 'Omejitev ne more biti manjša kot nič.';
$string['reply'] = 'odgovori';
$string['sorrymaxcharacters'] = 'Oprostite, vaša objava ne sme biti daljša od %s znakov.';
$string['title'] = 'Zid';
$string['viewwall'] = 'Ogled zidu';
$string['wall'] = 'Zid';
$string['wholewall'] = 'Ogled celega zidu';
?>
