<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['absolutebottom'] = 'Na absolutno dno';
$string['absolutemiddle'] = 'Na absolutno sredino';
$string['addblog'] = 'Dodaj blog';
$string['addpost'] = 'Dodaj objavo';
$string['alignment'] = 'Poravnava';
$string['attach'] = 'Pripni';
$string['attachedfilelistloaded'] = 'Seznam pripetih datotek naložen';
$string['attachedfiles'] = 'Pripete datoteke';
$string['attachments'] = 'Priponke';
$string['baseline'] = 'Na osnovnico';
$string['blog'] = 'Blog';
$string['blogcopiedfromanotherview'] = 'Pozor: Blog je kopiran iz drugega pogleda. Lahko ga premikate ali izbrišete, ne morete pa spremeniti %s, ki je v njem.';
$string['blogdeleted'] = 'Blog izbrisan';
$string['blogdesc'] = 'Opis';
$string['blogdescdesc'] = 'npr.: ‘Zapiski Julijinih doživetij in razmišljanj’.';
$string['blogdoesnotexist'] = 'Blog ne obstaja';
$string['blogfilesdirdescription'] = 'Datoteke naložene kot priponke objave bloga';
$string['blogfilesdirname'] = 'datoteke bloga';
$string['blogpost'] = 'Objava bloga';
$string['blogpostdeleted'] = 'Objava bloga izbrisana';
$string['blogpostdoesnotexist'] = 'Objava bloga ne obstaja';
$string['blogpostpublished'] = 'Objava bloga objavljena';
$string['blogpostsaved'] = 'Objava bloga shranjena';
$string['blogs'] = 'Blogi';
$string['blogsettings'] = 'Nastavitve bloga';
$string['blogtitle'] = 'Naslov';
$string['blogtitledesc'] = 'npr.: ‘Julijin dnevnik opravljanja prakse’.';
$string['border'] = 'Rob';
$string['bottom'] = 'Na dno';
$string['browsemyfiles'] = 'Prebrskaj moje datoteke';
$string['cancel'] = 'Prekliči';
$string['cannotdeleteblogpost'] = 'Pri odstranjevanju te objave bloga se je zgodila napaka.';
$string['commentsallowed'] = 'Komentarji';
$string['commentsallowedno'] = 'Ne dovoli komentiranja v tem blogu';
$string['commentsallowedyes'] = 'Dovoli prijavljenim uporabnikom komentiranje v tem blogu';
$string['commentsnotify'] = 'Obvestilo o komentarjih';
$string['commentsnotifydesc'] = 'Če želite, lahko prejmete obvestilo, kadarkoli kdo doda komentar k eni izmed objav bloga.';
$string['commentsnotifyno'] = 'Ne obveščaj me o komentarjih tega bloga';
$string['commentsnotifyyes'] = 'Obveščaj me o komentarjih tega bloga';
$string['copyfull'] = 'Drugi bodo dobili kopijo %s';
$string['copynocopy'] = 'Ko kopiraš pogled, preskoči ta blok v celoti';
$string['copyreference'] = 'Drugi lahko prikažejo %s v svojih pogledih';
$string['createandpublishdesc'] = 'To bo ustvarilo objavo bloga, ki bo dostopna drugim.';
$string['createasdraftdesc'] = 'To bo ustvarilo objavo bloga, ki ne bo dostopna drugim, dokler je ne boš objavil/a.';
$string['createblog'] = 'Ustvari blog';
$string['dataimportedfrom'] = 'Podatki uvoženi iz %s';
$string['defaultblogtitle'] = 'Blog osebe %s';
$string['delete'] = 'Izbriši';
$string['deleteblog?'] = 'Ali zares želite izbrisati ta blog?';
$string['deleteblogpost?'] = 'Ali zares želite izbrisati to objavo bloga?';
$string['description'] = 'Opis';
$string['dimensions'] = 'Dimenzije';
$string['draft'] = 'Osnutek';
$string['edit'] = 'Uredi';
$string['editblogpost'] = 'Uredi objavo bloga';
$string['entriesimportedfromleapexport'] = 'Vnosi uvoženi iz LEAP izvoza, ki jih ni bilo mogoče uvoziti drugje';
$string['erroraccessingblogfilesfolder'] = 'Napaka pri dostopanju do mape z datotekami bloga';
$string['errorsavingattachments'] = 'Napaka se je zgodila pri shranjevanju priponk objave bloga';
$string['horizontalspace'] = 'Vodoravni razmik';
$string['insert'] = 'Vstavi';
$string['insertimage'] = 'Vstavi sliko';
$string['left'] = 'Na levo';
$string['middle'] = 'Na sredino';
$string['moreoptions'] = 'Več možnosti';
$string['mustspecifycontent'] = 'Navesti morate vsebino objave bloga';
$string['mustspecifytitle'] = 'Navesti morate naslov objave bloga';
$string['myblogs'] = 'Moji blogi';
$string['name'] = 'Ime';
$string['newattachmentsexceedquota'] = 'Skupna velikost novih datotek, ki ste jih naložili s to objavo, bi presegla vašo kvoto. Objavo lahko vseeno shranite, če odstranite nekaj priponk, ki ste jih ravnokar dodali.';
$string['newblog'] = 'Nov blog';
$string['newblogpost'] = 'Nova objava v blogu "%s"';
$string['newerposts'] = 'Novejše objave';
$string['nofilesattachedtothispost'] = 'Ni pripetih datotek';
$string['noimageshavebeenattachedtothispost'] = 'Objava nima pripetih slik. Sliko morate naložiti ali pripeti k objavi, preden jo lahko vstavite.';
$string['nopostsaddone'] = 'Trenutno še brez objav. %sDodajte objavo%s!';
$string['noresults'] = 'Ne najdem nobene objave bloga';
$string['olderposts'] = 'Starejše objave';
$string['pluginname'] = 'Blogi';
$string['postbody'] = 'Telo';
$string['postedbyon'] = 'Objavil/a %s v %s';
$string['postedon'] = 'Objavljeno v';
$string['posts'] = 'objave';
$string['postscopiedfromview'] = 'Objave kopirane iz %s';
$string['posttitle'] = 'Naslov';
$string['publish'] = 'Objavi';
$string['publishblogpost?'] = 'Ali zares želite objaviti to objavo bloga?';
$string['published'] = 'Objavljeno';
$string['publishfailed'] = 'Zaradi napake objava bloga ni bila objavljena';
$string['remove'] = 'Ostrani';
$string['right'] = 'Na desno';
$string['save'] = 'Shrani';
$string['saveandpublish'] = 'Shrani in Objavi';
$string['saveasdraft'] = 'Shrani kot osnutek';
$string['savepost'] = 'Shrani objavo';
$string['savesettings'] = 'Shrani nastavitve';
$string['settings'] = 'Nastavitve';
$string['textbottom'] = 'Na dno besedila';
$string['texttop'] = 'Na vrh besedila';
$string['thisisdraft'] = 'Objava je osnutek';
$string['thisisdraftdesc'] = 'Ko je objava v stanju osnutka, je ne more videti nihče, razen vas.';
$string['title'] = 'Naslov';
$string['top'] = 'Na vrh';
$string['update'] = 'Posodobi';
$string['verticalspace'] = 'Navpični razmik';
$string['viewblog'] = 'Ogled bloga';
$string['viewposts'] = 'Kopirane objave (%s)';
$string['youarenottheownerofthisblog'] = 'Niste lastnik tega bloga';
$string['youarenottheownerofthisblogpost'] = 'Niste lastnik te objave bloga';
$string['youhaveblogs'] = 'Imate %s blogov.';
$string['youhavenoblogs'] = 'Nimate blogov.';
$string['youhaveoneblog'] = 'Imate 1 blog.';
?>
