<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['academicgoal'] = 'Akademski cilji';
$string['academicskill'] = 'Akademske spretnosti';
$string['backtoresume'] = 'Nazaj v Moj življenjepis';
$string['book'] = 'Knjige in publikacije';
$string['careergoal'] = 'Poklicni cilji';
$string['certification'] = 'Certifikati, pooblastila in nagrade';
$string['citizenship'] = 'Državljanstvo';
$string['compositedeleteconfirm'] = 'Ali res želite to izbrisati?';
$string['compositedeleted'] = 'Uspešno izbrisano';
$string['compositesaved'] = 'Uspešno shranjeno';
$string['compositesavefailed'] = 'Shranjevanje neuspešno';
$string['contactinformation'] = 'Kontaktni podatki';
$string['contribution'] = 'Prispevek';
$string['coverletter'] = 'Spremno pismo';
$string['current'] = 'Trenutno';
$string['date'] = 'Datum';
$string['dateofbirth'] = 'Datum rojstva';
$string['description'] = 'Opis';
$string['detailsofyourcontribution'] = 'Podrobnosti vašega prispevka';
$string['educationhistory'] = 'Zgodovina izobraževanja';
$string['employer'] = 'Delodajalec';
$string['employeraddress'] = 'Naslov delodajalca';
$string['employmenthistory'] = 'Zgodovina zaposlovanja';
$string['enddate'] = 'Konec';
$string['female'] = 'ženski';
$string['gender'] = 'Spol';
$string['goalandskillsaved'] = 'Uspešno shranjeno';
$string['institution'] = 'Ustanova';
$string['institutionaddress'] = 'Naslov ustanove';
$string['interest'] = 'Interesi';
$string['jobdescription'] = 'Opis zaposlitve';
$string['jobtitle'] = 'Naziv zaposlitve';
$string['male'] = 'moški';
$string['maritalstatus'] = 'Zakonski stan';
$string['membership'] = 'Poklicna članstva';
$string['movedown'] = 'Premakni dol';
$string['moveup'] = 'Premakni gor';
$string['mygoals'] = 'Moji cilji';
$string['myresume'] = 'Moj življenjepis';
$string['myskills'] = 'Moje spretnosti';
$string['personalgoal'] = 'Osebni cilji';
$string['personalinformation'] = 'Osebni podatki';
$string['personalskill'] = 'Osebne spretnosti';
$string['placeofbirth'] = 'Kraj rojstva';
$string['pluginname'] = 'Življenjepis';
$string['position'] = 'Položaj';
$string['qualdescription'] = 'Opis usposobljenosti';
$string['qualification'] = 'Usposobljenost';
$string['qualname'] = 'Naziv usposobljenosti';
$string['qualtype'] = 'Tip usposobljenosti';
$string['resume'] = 'Življenjepis';
$string['resumeofuser'] = 'Življenjepis osebe %s';
$string['resumesaved'] = 'Življenjepis uspešno shranjen';
$string['resumesavefailed'] = 'Neuspešno shranjevanje življenjepisa';
$string['startdate'] = 'Začetek';
$string['title'] = 'Naslov';
$string['viewyourresume'] = 'Ogled življenjepisa';
$string['visastatus'] = 'Stanje vizuma';
$string['workskill'] = 'Delovne spretnosti';
?>
