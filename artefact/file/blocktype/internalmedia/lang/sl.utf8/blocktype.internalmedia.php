<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-internalmedia
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Vgrajena predstavnost';
$string['description'] = 'Izberite datoteko za vgrajen ogled';

$string['media'] = 'Predstavnost';
$string['flashanimation'] = 'Flash animacija';

$string['typeremoved'] = 'Blok prikazuje tipe predstavnosti, ki jih skrbnik ne dovoli';
$string['configdesc'] = 'Nastavite, katere tipe datotek lahko uporabniki vgradijo v ta blok. Če onemogočite tip datoteke, ki je v bloku že bil uporabljen, potem te datoteke ne bodo več vidne.';
?>
