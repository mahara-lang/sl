<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Contents'] = 'Vsebina';
$string['Continue'] = 'Nadaljuj';
$string['Created'] = 'Ustvarjeno';
$string['Date'] = 'Datum';
$string['Default'] = 'Privzeto';
$string['Description'] = 'Opis';
$string['Details'] = 'Podrobnosti';
$string['Download'] = 'Prenos';
$string['File'] = 'Datoteka';
$string['Files'] = 'Datoteke';
$string['Folder'] = 'Mapa';
$string['Folders'] = 'Mape';
$string['Name'] = 'Ime';
$string['Owner'] = 'Lastnik';
$string['Preview'] = 'Predogled';
$string['Size'] = 'Velikost';
$string['Title'] = 'Naslov';
$string['Type'] = 'Tip';
$string['Unzip'] = 'Razširi';
$string['addafile'] = 'Dodaj datoteko';
$string['ai'] = 'Adobe Illustrator vektorska risba';
$string['aiff'] = 'AIFF zvočna datoteka';
$string['application'] = 'datoteka neznanega programa';
$string['archive'] = 'Arhiv';
$string['au'] = 'AU zvočna datoteka';
$string['avi'] = 'AVI video datoteka';
$string['bmp'] = 'Bitmap slikovna datoteka';
$string['bytes'] = 'bajtov';
$string['bz2'] = 'Bzip2 stisnjena datoteka';
$string['cannoteditfolder'] = 'Nimate dovoljenj za dodajanje vsebine v to mapo';
$string['cantcreatetempprofileiconfile'] = 'Zapisovanje začasne slike za ikono profila v %s ni mogoče';
$string['changessaved'] = 'Spremembe shranjene';
$string['clickanddragtomovefile'] = 'Kliknite in %s odvlecite drugam';
$string['confirmdeletefile'] = 'Ali ste prepričani, da želite izbrisati datoteko?';
$string['confirmdeletefolder'] = 'Ali ste prepričani, da želite izbrisati mapo?';
$string['confirmdeletefolderandcontents'] = 'Ali ste prepričani, da želite izbrisati mapo in vso njeno vsebino?';
$string['contents'] = 'Vsebina';
$string['copyrightnotice'] = 'Obvestilo o avtorskih pravicah';
$string['create'] = 'Ustvari';
$string['createfolder'] = 'Ustvari mapo';
$string['customagreement'] = 'Lasten sporazum';
$string['defaultagreement'] = 'Privzeti sporazum';
$string['defaultquota'] = 'Privzeta kvota';
$string['defaultquotadescription'] = 'Tukaj lahko nastavite količino prosotra na disku, ki bo kot kvota, dodeljena novim uporabnikom. Kvote obstoječih uporabnikov se ne bodo spremenile.';
$string['deletefile?'] = 'Ali zares želite izbrisati to datoteko?';
$string['deletefolder?'] = 'Ali zares želite izbrisati to mapo?';
$string['deleteselectedicons'] = 'Izbriši izbrane ikone';
$string['destination'] = 'Ciljna mapa';
$string['doc'] = 'Microsoft Wordov dokument';
$string['downloadfile'] = 'Prenesi %s';
$string['downloadoriginalversion'] = 'Prenesi izvirno različico';
$string['dss'] = 'Digital Speech Standard zvočna datoteka';
$string['editfile'] = 'Uredi datoteko';
$string['editfolder'] = 'Uredi mapo';
$string['emptyfolder'] = 'Sprazni mapo';
$string['extractfilessuccess'] = 'Ustvarjenih %s map in %s datotek.';
$string['file'] = 'datoteka';
$string['filealreadyindestination'] = 'Datoteka, ki jo želite premakniti, je že v tej mapi';
$string['fileappearsinviews'] = 'Ta datoteka je del enega ali več vaših pogledov.';
$string['fileattached'] = 'Ta datoteka je pripeta k %s drugim item(s) v vašem listovniku.';
$string['fileexists'] = 'Datoteka že obstaja';
$string['fileexistsonserver'] = 'Datoteka z imenom %s že obstaja.';
$string['fileexistsoverwritecancel'] = 'Datoteka s tem imenom že obstaja. Poizkusite lahko z drugim imenom, ali prepišete obstoječo datoteko.';
$string['fileinstructions'] = 'Naložite slike, dokumente ali druge datoteke za vključitev v poglede. Če želite premakniti datoteko ali mapo, jo povlecite in spustite na drugo mapo.';
$string['filelistloaded'] = 'Seznam datotek naložen';
$string['filemoved'] = 'Datoteka uspešno premaknjena';
$string['filenamefieldisrequired'] = 'Polje datoteka je zahtevano';
$string['files'] = 'datoteke';
$string['filesextractedfromarchive'] = 'Datoteke, razširjene iz arhiva';
$string['filesextractedfromziparchive'] = 'Datoteke, razširjene iz zip arhiva';
$string['fileswillbeextractedintofolder'] = 'Datoteke bodo razširjene v %s';
$string['filethingdeleted'] = '%s izbrisano';
$string['fileuploadedas'] = '%s naloženo kot "%s"';
$string['fileuploadedtofolderas'] = '%s naloženo v %s kot "%s"';
$string['filewithnameexists'] = 'Datoteka ali mapa z imenom "%s" že obstaja.';
$string['flv'] = 'FLV Flash film';
$string['folder'] = 'Mapa';
$string['folderappearsinviews'] = 'Ta mapa se pojavlja v enem ali več vaših pogledov.';
$string['foldercreated'] = 'Mapa ustvarjena';
$string['foldernamerequired'] = 'Prosimo, vpišite ime nove mape.';
$string['foldernotempty'] = 'Mapa ni prazna.';
$string['gif'] = 'GIF slikovna datoteka';
$string['gotofolder'] = 'Pojdite v %s';
$string['groupfiles'] = 'Datoteke skupine';
$string['gz'] = 'Gzip stisnjena datoteka';
$string['home'] = 'Domov';
$string['html'] = 'HTML datoteka';
$string['htmlremovedmessage'] = 'Ogledujete si <strong>%s</strong> osebe <a href="%s">%s</a>. Datoteka, ki je prikazana spodaj, je bila filtrirana zaradi zlonamerne vsebine in je samo približna predstavitev izvirnika.';
$string['htmlremovedmessagenoowner'] = 'Ogledujete si <strong>%s</strong>. Datoteka, ki je prikazana spodaj, je bila filtrirana zaradi zlonamerne vsebine in je samo približna predstavitev izvirnika.';
$string['image'] = 'Slika';
$string['imagetitle'] = 'Naslov slike';
$string['insufficientquotaforunzip'] = 'Vaša preostala kvota je prenizka za razširjanje te datoteke.';
$string['invalidarchive'] = 'Napaka pri branju arhivske datoteke.';
$string['jpeg'] = 'JPEG slikovna datoteka';
$string['jpg'] = 'JPEG slikovna datoteka';
$string['js'] = 'JavaScript izvorna koda';
$string['lastmodified'] = 'Zadnja sprememba';
$string['latex'] = 'LaTeX dokument';
$string['m3u'] = 'M3U seznam predvajanja';
$string['maxuploadsize'] = 'Največja velikost nalaganja';
$string['mov'] = 'MOV Quicktime film';
$string['movefailed'] = 'Premikanje neuspešno';
$string['movefaileddestinationinartefact'] = 'Mape ne morete premakniti same vase.';
$string['movefaileddestinationnotfolder'] = 'V mapo lahko premaknete samo datoteke.';
$string['movefailednotfileartefact'] = 'Premikate lahko samo naslednje izdelke: datoteke, mape in slike.';
$string['movefailednotowner'] = 'Nimate dovoljenj za premikanje datoteke v to mapo';
$string['mp3'] = 'MP3 zvočna datoteka';
$string['mp4_audio'] = 'MP4 zvočna datoteka';
$string['mp4_video'] = 'MP4 video datoteka';
$string['mpeg'] = 'MPEG filmska datoteka';
$string['mpg'] = 'MPG filmska datoteka';
$string['myfiles'] = 'Moje datoteke';
$string['namefieldisrequired'] = 'Polje z imenom je zahtevano';
$string['nametoolong'] = 'Ime je predolgo. Prosimo, izberite krajše ime.';
$string['nofilesfound'] = 'Ne najdem datotek';
$string['noimagesfound'] = 'Ne najdem slik';
$string['notpublishable'] = 'Nimate dovoljenj za objavo te datoteke';
$string['odb'] = 'OpenDocument zbirka podatkov';
$string['odc'] = 'OpenDocument grafikon';
$string['odf'] = 'OpenDocument formula';
$string['odg'] = 'OpenDocument risba';
$string['odi'] = 'OpenDocument slika';
$string['odm'] = 'OpenDocument glavni dokument';
$string['odp'] = 'OpenDocument predstavitev';
$string['ods'] = 'OpenDocument preglednica';
$string['odt'] = 'OpenDocument besedilo';
$string['onlyfiveprofileicons'] = 'Naložite lahko samo pet ikon profila';
$string['or'] = 'ali';
$string['oth'] = 'Predloga dokumenta HTML';
$string['ott'] = 'Predloga dokumenta z besedilom';
$string['overwrite'] = 'Prepiši';
$string['parentfolder'] = 'Nadrejena mapa';
$string['pdf'] = 'PDF dokument';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Prosimo počakajte, dokler se datoteke razširjajo.';
$string['pluginname'] = 'Datoteke';
$string['png'] = 'PNG slikovna datoteka';
$string['ppt'] = 'Microsoft PowerPoint predstavitev';
$string['profileicon'] = 'Ikona profila';
$string['profileiconimagetoobig'] = 'Ikona, ki ste jo naložili je prevelika (%sx%s pikslov). Ikona profila ne sme biti večja od %sx%s pikslov.';
$string['profileicons'] = 'Ikone profila';
$string['profileiconsdefaultsetsuccessfully'] = 'Privzeta ikona profila nastavljena uspešno';
$string['profileiconsdeletedsuccessfully'] = 'Ikona/e profila uspešno izbrisana/e';
$string['profileiconsetdefaultnotvalid'] = 'Ne morem nastaviti privzete ikone profila, ker izbira ni veljavna';
$string['profileiconsiconsizenotice'] = 'Tukaj lahko naložite do <strong>pet</strong> ikon profila in izberete eno, ki bo vedno prikazana kot vaša privzeta ikona. Ikone morajo meriti med 16x16 in %sx%s pikslov.';
$string['profileiconsize'] = 'Velikost ikone profila';
$string['profileiconsnoneselected'] = 'Nobena ikona profila ni bila izbrana za brisanje';
$string['profileiconuploadexceedsquota'] = 'Nalaganje te ikone profila bi preseglo vašo kvoto. Poizkusite izbrisati nekaj naloženih datotek.';
$string['quicktime'] = 'Quicktime filmska datoteka';
$string['ra'] = 'Real zvočna datoteka';
$string['ram'] = 'RAM RealMedia meta-datoteka';
$string['requireagreement'] = 'Zahtevaj sporazum';
$string['rm'] = 'RM RealMedia pretočna predstavnost';
$string['rpm'] = 'RPM RealMedia vtičnik za predvajalnik';
$string['rtf'] = 'RTF dokument';
$string['savechanges'] = 'Shrani spremembe';
$string['selectafile'] = 'Izberite datoteko';
$string['setdefault'] = 'Nastavi privzeto ikono';
$string['sgi_movie'] = 'SGI filmska datoteka';
$string['sh'] = 'Shell skriptna datoteka';
$string['sitefilesloaded'] = 'Datoteke spletišča naložene';
$string['spacerequired'] = 'Zahtevani prostor';
$string['spaceused'] = 'Uporabljeni prostor';
$string['swf'] = 'SWF Flash animacija';
$string['tar'] = 'TAR arhivska datoteka';
$string['timeouterror'] = 'Nalaganje datoteke neuspešno: poizkusite ponovno naložiti datoteko';
$string['title'] = 'Naslov';
$string['titlefieldisrequired'] = 'Polje z imenom je zahtevano';
$string['txt'] = 'Besedilna datoteka';
$string['unlinkthisfilefromblogposts?'] = 'Datoteka je pripeta k eni ali več objavam bloga. Če to datoteko izbrišete, bo odstranjena iz teh objav.';
$string['unzipprogress'] = 'Ustvarjenih %s datotek/map.';
$string['upload'] = 'Naloži';
$string['uploadagreement'] = 'Sporazum za nalaganje';
$string['uploadagreementdescription'] = 'Vključite to možnost, če želite uporabnike prisiliti, da se morajo strinjati s spodnjim besedilom sporazuma preden lahko naložijo datoteko na spletišče.';
$string['uploadedprofileiconsuccessfully'] = 'Nalaganje nove ikone profila uspešno';
$string['uploadexceedsquota'] = 'Nalaganje te datoteke bi preseglo vašo kvoto. Poizkusite izbrisati nekaj naloženih datotek.';
$string['uploadfile'] = 'Naloži datoteko';
$string['uploadfileexistsoverwritecancel'] = 'Datoteka s tem imenom že obstaja. Datoteko, ki jo želite naložiti, lahko preimenujete ali prepišete obstoječo datoteko.';
$string['uploadingfile'] = 'nalagam datoteko...';
$string['uploadingfiletofolder'] = 'Nalagam %s v %s';
$string['uploadoffilecomplete'] = 'Nalaganje %s končano';
$string['uploadoffilefailed'] = 'Nalaganje %s neuspešno';
$string['uploadoffiletofoldercomplete'] = 'Nalaganje %s v %s končano';
$string['uploadoffiletofolderfailed'] = 'Nalaganje %s v %s neuspešno';
$string['uploadprofileicon'] = 'Naloži ikono profila';
$string['usecustomagreement'] = 'Uporabi lasten sporazum';
$string['usenodefault'] = 'Brez privzete ikone';
$string['usingnodefaultprofileicon'] = 'Sedaj ne uporabljate privzete ikone';
$string['wav'] = 'WAV zvočna datoteka';
$string['wmv'] = 'WMV video datoteka';
$string['wrongfiletypeforblock'] = 'Datoteka ki ste jo naložili, ni ustreznega tipa za ta blok.';
$string['xml'] = 'XML datoteka';
$string['youmustagreetothecopyrightnotice'] = 'Strinjati se morate z obvestilom o avtorskih pravicah';
$string['zip'] = 'ZIP arhivska datoteka';
?>
