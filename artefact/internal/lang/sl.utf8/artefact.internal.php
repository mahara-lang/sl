<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Created'] = 'Ustvarjeno';
$string['Description'] = 'Opis';
$string['Download'] = 'Prenos';
$string['Owner'] = 'Lastnik';
$string['Preview'] = 'Predogled';
$string['Size'] = 'Velikost';
$string['Title'] = 'Naslov';
$string['Type'] = 'Tip';
$string['aboutdescription'] = 'Tukaj vpišite svoje pravo ime in priimek. Če se želite ljudem v sistemu predstavljati z drugim imenom, ga vpišite kot svoje prikazno ime.';
$string['aboutme'] = 'O meni';
$string['address'] = 'Poštni naslov';
$string['aimscreenname'] = 'AIM zaslonsko ime';
$string['blogaddress'] = 'Naslov bloga';
$string['businessnumber'] = 'Službeni telefon';
$string['city'] = 'Mesto/Pokrajina';
$string['contact'] = 'Kontaktni podatki';
$string['contactdescription'] = 'Vsi ti podatki so zasebni, dokler jih ne vključite v pogled.';
$string['country'] = 'Država';
$string['editprofile'] = 'Uredi profil';
$string['email'] = 'Epoštni naslov';
$string['emailactivation'] = 'Aktivacija epošte';
$string['emailactivationfailed'] = 'Neuspešna aktivacija epošte';
$string['emailactivationsucceeded'] = 'Uspešna aktivacija epošte';
$string['emailaddress'] = 'Alternativna epošta';
$string['emailalreadyactivated'] = 'Epošta je že aktivirana';
$string['emailingfailed'] = 'Profil shranjen, vendar epošta ni bila poslana na: %s';
$string['emailvalidation_body'] = 'Pozdravljeni %s,

Epoštni naslov %s je bil dodan vašemu uporabniškemu računu v Mahari. Prosimo, obiščite spodnjo povezavo za aktivacijo tega naslova.

%s';
$string['emailvalidation_subject'] = 'Potrditev epošte';
$string['faxnumber'] = 'Faks';
$string['firstname'] = 'Ime';
$string['fullname'] = 'Polno ime';
$string['general'] = 'Splošno';
$string['homenumber'] = 'Domači telefon';
$string['icqnumber'] = 'ICQ številka';
$string['industry'] = 'Poklicno področje';
$string['institution'] = 'Ustanova';
$string['introduction'] = 'Predstavitev';
$string['invalidemailaddress'] = 'Neveljaven epoštni naslov';
$string['jabberusername'] = 'Jabber uporabniško ime';
$string['lastmodified'] = 'Zadnja sprememba';
$string['lastname'] = 'Priimek';
$string['loseyourchanges'] = 'Ne shranim sprememb?';
$string['mandatory'] = 'Obvezno';
$string['messaging'] = 'Sporočanje';
$string['messagingdescription'] = 'Podobno kot kontaktni podatki, so tudi ti podatki zasebni.';
$string['mobilenumber'] = 'Prenosni telefon';
$string['msnnumber'] = 'MSN Chat';
$string['myfiles'] = 'Moje datoteke';
$string['name'] = 'Ime';
$string['occupation'] = 'Zaposlitev';
$string['officialwebsite'] = 'Uradna spletna stran';
$string['personalwebsite'] = 'Osebna spletna stran';
$string['pluginname'] = 'Profil';
$string['preferredname'] = 'Prikazno ime';
$string['principalemailaddress'] = 'Osnovna epošta';
$string['profile'] = 'Profil';
$string['profilefailedsaved'] = 'Shranjevanje profila neuspešno';
$string['profileinformation'] = 'Podatki profila';
$string['profilepage'] = 'Stran s profilom';
$string['profilesaved'] = 'Profil uspešno shranjen';
$string['public'] = 'Javno';
$string['saveprofile'] = 'Shrani profil';
$string['skypeusername'] = 'Skype uporabniško ime';
$string['studentid'] = 'ID številka';
$string['town'] = 'Kraj';
$string['unvalidatedemailalreadytaken'] = 'Epoštni naslov, ki ga želite potrditi, je že zaseden';
$string['validationemailsent'] = 'potrditvena epošta je bila poslana';
$string['validationemailwillbesent'] = 'potrditvena epošta bo poslana, ko boste shranili vaš profil';
$string['verificationlinkexpired'] = 'Povezava za preverjanje je potekla';
$string['viewallprofileinformation'] = 'Ogled vseh podatkov profila';
$string['viewmyprofile'] = 'Ogled mojega profila';
$string['viewprofilepage'] = 'Ogled strani s profilom';
$string['yahoochat'] = 'Yahoo Chat';
?>
