<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['15,70,15'] = 'Precej širši srednji stolpec';
$string['20,30,30,20'] = 'Širša srednja stolpca';
$string['25,25,25,25'] = 'Stolpci enakih širin';
$string['25,50,25'] = 'Širši srednji stolpec';
$string['33,33,33'] = 'Stolpci enakih širin';
$string['33,67'] = 'Širši desni stolpec';
$string['50,50'] = 'Stolpca enakih širin';
$string['67,33'] = 'Širši levi stolpec';
$string['Added'] = 'Dodano';
$string['Browse'] = 'Brskaj';
$string['Configure'] = 'Nastavitev';
$string['Owner'] = 'Lastnik';
$string['Preview'] = 'Predogled';
$string['Search'] = 'Najdi';
$string['Template'] = 'Predloga';
$string['Untitled'] = 'Brez naslova';
$string['View'] = 'Pogled';
$string['Views'] = 'Pogledi';
$string['access'] = 'Dostop';
$string['accessbetweendates2'] = 'Nihče drug ne more videti tega pogleda pred %s ali po %s';
$string['accessfromdate2'] = 'Nihče drug ne more videti tega pogleda pred %s';
$string['accessuntildate2'] = 'Nihče drug ne more videti tega pogleda po %s';
$string['add'] = 'Dodaj';
$string['addcolumn'] = 'Dodaj stolpec';
$string['addedtowatchlist'] = 'Pogled je dodan na vaš nadzorni seznam';
$string['addfeedbackfailed'] = 'Dodajanje povratne informacije neuspešno';
$string['addnewblockhere'] = 'Tukaj dodaj nov blok';
$string['addtowatchlist'] = 'Dodaj pogled na nadzorni seznam';
$string['addtutors'] = 'Dodaj mentorje';
$string['addusertogroup'] = 'Dodaj uporabnika tej skupini';
$string['allowcopying'] = 'Dovoli kopiranje';
$string['allviews'] = 'Vsi pogledi';
$string['alreadyinwatchlist'] = 'Pogled je že na vašem nadzornem seznamu';
$string['artefacts'] = 'Izdelki';
$string['artefactsinthisview'] = 'Izdelki v tem polgedu';
$string['attachedfileaddedtofolder'] = 'Priložena datoteka %s je dodana v vašo mapo "%s".';
$string['attachfile'] = 'Pripni datoteko';
$string['attachment'] = 'Priponka';
$string['back'] = 'Nazaj';
$string['backtocreatemyview'] = 'Nazaj na ustvarjanje mojega pogleda';
$string['backtoyourview'] = 'Nazaj na moj pogled';
$string['blockcopypermission'] = 'Dovoljenje za kopiranje bloka';
$string['blockcopypermissiondesc'] = 'Če dovolite drugim uporabnikom, da kopirajo pogled, lahko izberete, kako bo kopiran ta blok.';
$string['blockinstanceconfiguredsuccessfully'] = 'Blok uspešno nastavljen';
$string['blocksinstructionajax'] = 'Povlecite bloke pod to črto, da jih dodate v postavitev pogleda. Bloke lahko v postavitvi pogleda povlečete na drugo mesto in tako spreminjate njihov položaj.';
$string['blocksintructionnoajax'] = 'Izberite blok, nato izberete položaj na pogledu, kamor bi ga dodali. Položaj bloka lahko spreminjate z uporabo smernih tipk nad njegovo naslovno vrstico.';
$string['blocktitle'] = 'Naslov bloka';
$string['blocktypecategory.feeds'] = 'Zunanji viri';
$string['blocktypecategory.fileimagevideo'] = 'Datoteke, slike in video';
$string['blocktypecategory.general'] = 'Splošno';
$string['by'] = 'osebe';
$string['cantdeleteview'] = 'Pogleda ne morete izbrisati';
$string['canteditdontown'] = 'Pogleda ne morete urejati, saj niste njegov lastnik';
$string['canteditdontownfeedback'] = 'Povratne informacije ne morete urejati, saj niste njen lastnik';
$string['canteditsubmitted'] = 'Pogleda ne morete urejati, saj je bil oddan za ocenitev skupini "s%". Počakati boste morali, da mentor oceni in nato sprosti vaš pogled.';
$string['cantsubmitviewtogroup'] = 'Pogleda ne morete oddati skupini za ocenitev';
$string['changemyviewlayout'] = 'Spremeni postavitev mojega pogleda';
$string['changeviewlayout'] = 'Spremeni postavitev pogleda';
$string['choosetemplategrouppagedescription'] = '<p>Izhodišče za nov pogled lahko poiščete med pogledi, ki jih lahko kopira ta skupina. Predogled vsakega pogleda si lahko ogledate tako, da kliknete na njegovo ime. Ko najdete pogled, ki ga želite kopirati, kliknite ustrezni gumb "Kopiraj pogled", da ustvarite kopijo in jo nato preoblikujte.</p><p><strong>Opomba:</strong> Skupine trenutno ne morejo ustvariti kopij blogov ali objav v blogih.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Izhodišče za nov pogled lahko poiščete med pogledi, ki jih lahko kopira ta ustanova. Predogled vsakega pogleda si lahko ogledate tako, da kliknete na njegovo ime. Ko najdete pogled, ki ga želite kopirati, kliknite ustrezni gumb "Kopiraj pogled", da ustvarite kopijo in jo nato preoblikujte.</p><p><strong>Opomba:</strong> Ustanove trenutno ne morejo ustvariti kopij blogov ali objav v blogih.</p>';
$string['choosetemplatepagedescription'] = '<p>Izhodišče za nov pogled lahko poiščete med pogledi, ki jih lahko kopirate. Predogled vsakega pogleda si lahko ogledate tako, da kliknete na njegovo ime. Ko najdete pogled, ki ga želite kopirati, kliknite ustrezni gumb "Kopiraj pogled", da ustvarite kopijo in jo nato preoblikujte.</p>';
$string['clickformoreinformation'] = 'Kliknite za več informacij in za dajanje povratnih informacij';
$string['close'] = 'Zapri';
$string['comment'] = 'komentar';
$string['comments'] = 'komentarji';
$string['complaint'] = 'Pritožba';
$string['configureblock'] = 'Nastavi blok %s';
$string['configurethisblock'] = 'Nastavi blok';
$string['confirmcancelcreatingview'] = 'Pogled ni dokončan. Ali zares želite preklicati?';
$string['confirmdeleteblockinstance'] = 'Ali zares želite izbrisati ta blok?';
$string['copiedblocksandartefactsfromtemplate'] = 'Kopiranih %d blokov in %d izdelkov iz %s';
$string['copyaview'] = 'Kopiraj pogled';
$string['copyfornewgroups'] = 'Kopiraj za nove skupine';
$string['copyfornewgroupsdescription'] = 'Ustvari kopijo pogleda v vseh novih skupinah tipa:';
$string['copyfornewmembers'] = 'Kopiraj za nove člane ustanove';
$string['copyfornewmembersdescription'] = 'Samodejno ustvari osebni izvod pogleda za vse nove člane ustanove %s.';
$string['copyfornewusers'] = 'Kopiraj za nove uporabnike';
$string['copyfornewusersdescription'] = 'Kadarkoli je ustvarjen nov uporabnik, samodejno ustvari osebni izvod pogleda v uporabnikovem listovniku.';
$string['copynewusergroupneedsloggedinaccess'] = 'Kopirani pogledi za nove uporabnike ali skupine morajo omogočati dostop prijavljenim uporabnikom.';
$string['copythisview'] = 'Kopiraj ta pogled';
$string['copyview'] = 'Kopiraj pogled';
$string['createemptyview'] = 'Ustvari prazen pogled';
$string['createtemplate'] = 'Ustvari predlogo pogleda';
$string['createview'] = 'Ustvari pogled';
$string['createviewstepone'] = 'Ustvari pogled - korak ena: Postavitev';
$string['createviewstepthree'] = 'Ustvari pogled - korak tri: Dostop';
$string['createviewsteptwo'] = 'Ustvari pogled - korak dve: Podrobnosti';
$string['date'] = 'Datum';
$string['deletespecifiedview'] = 'Izbriši pogled \'%s\'';
$string['deletethisview'] = 'Izbriši ta pogled';
$string['deleteviewconfirm'] = 'Ali zares želite izbrisati pogled? Brisanja ne bo mogoče razveljaviti.';
$string['description'] = 'Prikaži opis';
$string['displaymyview'] = 'Prikaži moj pogled';
$string['editaccessforview'] = 'Uredi dostop za pogled "%s"';
$string['editaccesspagedescription2'] = '<p>Privzeto, lahko samo vi vidite vaš pogled. Tukaj lahko izberete, kdo drug bo tudi lahko videl informacije v vašem pogledu. Kliknite gumb "Dodaj" ter odobrite dostop javnosti, prijavljenim uporabnikom ali prijateljem. Uporabite polje za iskanje, če želite dodati posamezne uporabnike ali skupine. Vsi, ki jih boste dodali, bodo prikazani v desnem seznamu pod Dodano.</p> 
<p>Drugim uporabnikom lahko dovolite tudi kopiranje vaših pogledov v njihove listovnike. Ko uporabniki kopirajo pogled, samodejno dobijo tudi svoje izvode vseh datotek in map, ki jih pogled vsebuje.</p> 
<p>Ko končate, se pomaknite navzdol in za nadaljevanje kliknite gumb "Shrani".</p>';
$string['editblocksforview'] = 'Uredi pogled "%s"';
$string['editblockspagedescription'] = '<p>Izberite enega izmed spodnjih zavihkov ter si oglejte, katere bloke lahko prikažete v vašem pogledu. Bloke lahko povlečete in spustite v postavitev pogleda. Izberite ikono ? za več informacij.</p>';
$string['editmyview'] = 'Uredi moj pogled';
$string['editprofileview'] = 'Uredi pogled profila';
$string['editthisview'] = 'Uredi ta pogled';
$string['editviewaccess'] = 'Uredi dostop do pogleda';
$string['editviewdetails'] = 'Uredi podrobnosti za pogled "%s"';
$string['editviewnameanddescription'] = 'Uredi podrobnosti pogleda';
$string['empty_block'] = 'Izberite izdelek v drevesu na levi in ga postavite sem';
$string['emptylabel'] = 'Kliknite tukaj, za vnos besedila za to oznako';
$string['err.addblocktype'] = 'Ne morem dodati bloka k pogledu';
$string['err.addcolumn'] = 'Ne morem dodati novega stolpca';
$string['err.moveblockinstance'] = 'Ne morem premakniti bloka na določen položaj';
$string['err.removeblockinstance'] = 'Ne morem izbrisati bloka';
$string['err.removecolumn'] = 'Ne morem izbrisati stolpca';
$string['everyoneingroup'] = 'Kdorkoli v skupini';
$string['feedback'] = 'Povratna informacija';
$string['feedbackattachdirdesc'] = 'Datoteke, pripete k pogledu za ocenitev';
$string['feedbackattachdirname'] = 'assessmentfiles';
$string['feedbackattachmessage'] = 'Pripeta datoteka je dodana v mapo %s';
$string['feedbackchangedtoprivate'] = 'Povratna informacija spremenjena v zasebno';
$string['feedbackonthisartefactwillbeprivate'] = 'Povratno informacijo o tem izdelku bo videl samo lastnik.';
$string['feedbackonviewbytutorofgroup'] = 'Povratna informacija na %s mentorja %s za %s';
$string['feedbacksubmitted'] = 'Povratna informacija oddana';
$string['filescopiedfromviewtemplate'] = 'Datoteke kopirane iz %s';
$string['forassessment'] = 'za ocenitev';
$string['friend'] = 'Prijatelj';
$string['friends'] = 'Prijatelji';
$string['friendslower'] = 'prijatelji';
$string['grouplower'] = 'skupina';
$string['groups'] = 'Skupine';
$string['groupviews'] = 'Pogledi skupine';
$string['in'] = 'v';
$string['institutionviews'] = 'Pogledi ustanove';
$string['invalidcolumn'] = 'Stolpec %s izven obsega';
$string['inviteusertojoingroup'] = 'Povabi uporabnika, da se pridruži skupini';
$string['listviews'] = 'Seznam pogledov';
$string['loggedin'] = 'Prijavljeni uporabniki';
$string['loggedinlower'] = 'prijavljeni uporabniki';
$string['makeprivate'] = 'Spremeni v zasebno';
$string['makepublic'] = 'Naredi javno';
$string['moveblockdown'] = 'Premakni ta blok dol';
$string['moveblockleft'] = 'Premakni ta blok levo';
$string['moveblockright'] = 'Premakni ta blok desno';
$string['moveblockup'] = 'Premakni ta blok gor';
$string['movethisblockdown'] = 'Premakni blok dol';
$string['movethisblockleft'] = 'Premakni blok levo';
$string['movethisblockright'] = 'Premakni blok desno';
$string['movethisblockup'] = 'Premakni blok gor';
$string['myviews'] = 'Moji pogledi';
$string['next'] = 'Naprej';
$string['noaccesstoview'] = 'Nimate dovoljenj za dostop do tega pogleda';
$string['noartefactstochoosefrom'] = 'Oprostite, ni izdelkov za izbiranje';
$string['noblocks'] = 'Oprostite, v tej kategoriji ni blokov :(';
$string['nobodycanseethisview2'] = 'Samo vi lahko vidite ta pogled';
$string['nocopyableviewsfound'] = 'Brez pogledov, ki bi jih lahko kopirali';
$string['noownersfound'] = 'Ne najdem lastnika';
$string['nopublicfeedback'] = 'Ni javne povratne informacije';
$string['notifysiteadministrator'] = 'Obvesti skrbnika spletišča';
$string['notitle'] = 'Brez naslova';
$string['noviewlayouts'] = 'Za pogled s %s stolpci ne obstaja nobena postavitev';
$string['noviews'] = 'Brez pogledov.';
$string['numberofcolumns'] = 'Število stolpcev';
$string['overridingstartstopdate'] = 'Preglasitev datuma začetka/konca';
$string['overridingstartstopdatesdescription'] = 'Če želite, lahko nastavite preglasitev datuma začetka in/ali konca. Drugi ljudje ne bodo mogli videti vašega pogleda pred datumom začetka in po datumu konca, ne glede na druga dodeljena dovoljenja dostopa.';
$string['owner'] = 'lastnik';
$string['ownerformat'] = 'Oblika prikaza imena';
$string['ownerformatdescription'] = 'Kako naj vaše ime vidijo ljudje, ki si ogledujejo vaš pogled?';
$string['owners'] = 'lastniki';
$string['placefeedback'] = 'Oddaj povratno informacijo';
$string['placefeedbacknotallowed'] = 'Nimate dovoljenj za dajanje povratnih informacij na tem pogledu';
$string['print'] = 'Natisni';
$string['profileicon'] = 'Ikona profila';
$string['profileviewtitle'] = 'Pogled profila';
$string['public'] = 'Javno';
$string['publiclower'] = 'javno';
$string['reallyaddaccesstoemptyview'] = 'Vaš pogled ne vsebuje blokov. Ali zares želite tem uporabnikom dovoliti dostop do pogleda?';
$string['remove'] = 'Odstrani';
$string['removeblock'] = 'Odstrani blok %s';
$string['removecolumn'] = 'Odstrani stolpec';
$string['removedfromwatchlist'] = 'Pogled je odstranjen iz vašega nadzornega seznama';
$string['removefromwatchlist'] = 'Odstrani pogled iz nadzornega seznama';
$string['removethisblock'] = 'Odstrani blok';
$string['reportobjectionablematerial'] = 'Prijavi neprimerno vsebino';
$string['reportsent'] = 'Vaše poročilo je poslano';
$string['searchowners'] = 'Iskanje lastnikov';
$string['searchviews'] = 'Iskanje pogledov';
$string['searchviewsbyowner'] = 'Iskanje pogledov lastnika:';
$string['selectaviewtocopy'] = 'Izberite pogled, ki ga želite kopirati:';
$string['show'] = 'Pokaži';
$string['startdate'] = 'Datum/čas začetka dostopa';
$string['startdatemustbebeforestopdate'] = 'Datum začetka mora biti pred datumom zaključka';
$string['stopdate'] = 'Datum/čas konca dostopa';
$string['submitthisviewto'] = 'Oddaj ta pogled osebi';
$string['submitviewconfirm'] = 'Če oddate \'%s\' mentorju \'%s\' za ocenitev, ne boste mogli urejati pogleda, dokler ga mentor ne oceni. Ali sedaj zares želite oddati pogled?';
$string['submitviewtogroup'] = 'Oddaj \'%s\' mentorju \'%s\' za ocenitev';
$string['success.addblocktype'] = 'Dodajanje bloka uspešno';
$string['success.addcolumn'] = 'Dodajanje stolpca uspešno';
$string['success.moveblockinstance'] = 'Premikanje bloka uspešno';
$string['success.removeblockinstance'] = 'Brisanje bloka uspešno';
$string['success.removecolumn'] = 'Brisanje stolpca uspešno';
$string['templatedescription'] = 'Obkljukajte to polje, če želite, da si lahko ljudje, ki vidijo vaš pogled, ustvarijo lastno kopijo tega pogleda.';
$string['thisfeedbackisprivate'] = 'Povratna informacija je zasebna';
$string['thisfeedbackispublic'] = 'Povratna informacija je javna';
$string['thisviewmaybecopied'] = 'Kopiranje je dovoljeno';
$string['title'] = 'Prikaži naslov';
$string['token'] = 'Skrivni URL';
$string['tutors'] = 'mentorji';
$string['unrecogniseddateformat'] = 'Neprepoznavna oblika datuma';
$string['updatewatchlistfailed'] = 'Neuspešno posodabljanje opazovanega seznama';
$string['users'] = 'Uporabniki';
$string['view'] = 'pogled';
$string['viewaccesseditedsuccessfully'] = 'Dostop do pogleda uspešno shranjen';
$string['viewcolumnspagedescription'] = 'Najprej izberite število stolpcev v pogledu. V naslednjem koraku boste lahko spremenili širino stolpcev.';
$string['viewcopywouldexceedquota'] = 'Kopiranje tega pogleda bi preseglo vašo dovoljeno kvoto.';
$string['viewcreatedsuccessfully'] = 'Pogled uspešno ustvarjen';
$string['viewdeleted'] = 'Pogled izbrisan';
$string['viewfilesdirdesc'] = 'Datoteke iz kopiranih pogledov';
$string['viewfilesdirname'] = 'viewfiles';
$string['viewinformationsaved'] = 'Informacije o pogledu uspešno shranjene';
$string['viewlayoutchanged'] = 'Postavitev pogleda spremenjena';
$string['viewlayoutpagedescription'] = 'Izberite, kako naj bodo postavljeni stolpci v vašem pogledu.';
$string['views'] = 'pogledi';
$string['viewsavedsuccessfully'] = 'Pogled uspešno shranjen';
$string['viewsby'] = 'Pogledi osebe %s';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Omogočiti morate kopiranje, preden lahko nastavite kopiranje pogleda za nove skupine.';
$string['viewscopiedfornewusersmustbecopyable'] = 'Omogočiti morate kopiranje, preden lahko nastavite kopiranje pogleda za nove uporabnike.';
$string['viewsownedbygroup'] = 'Pogledi, katerih lastnik je ta skupina';
$string['viewssharedtogroup'] = 'Pogledi, ki so deljeni s to skupino';
$string['viewssharedtogroupbyothers'] = 'Pogledi, ki jih drugi delijo s to skupino';
$string['viewssubmittedtogroup'] = 'Pogledi, oddani tej skupini';
$string['viewsubmitted'] = 'Pogled oddan';
$string['viewsubmittedtogroup'] = 'Pogled je bil oddan <a href="%sgroup/view.php?id=%s">%s</a>';
$string['watchlistupdated'] = 'Opazovani seznam je posodobljen';
$string['whocanseethisview'] = 'Kdo lahko vidi ta pogled';
$string['youhavenoviews'] = 'Nimate nobenega pogleda.';
$string['youhaveoneview'] = 'Imate 1 pogled.';
$string['youhaveviews'] = 'Imate %s pogleda/e/ov.';
?>
