<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['aboutdefaultcontent'] = '<h2>O Mahari</h2>

<p>Ustanovljena leta 2006, je Mahara rezultat skupnega dela, ki ga financira E-learning Collaborative Development Fund (eCDF) Komisije za terciarno izobraževanje iz Nove Zelandije in ki vključuje Univerzo Massey, Tehnološko univerzo Auckland, Open Polytechnic iz Nove Zelandije in Univerzo Victoria iz Wellingtona.</p>

<p>Mahara je v celoti opremljen elektronski listovnik oz. portfelj, spletni dnevnik, graditelj življenjepisa in sistem socialne mreže, ki služi za povezovanje uporabnikov in ustvarjanje spletnih skupnosti. Mahara je zgrajena tako, da zagotavlja uporabnikom orodja za oblikovanje razvojnega okolja za osebno in poklicno izobraževanje.</p>

<p>Ime, ki v jeziku Te Reo Maori pomeni "misliti" ali "pomisliti", odraža predanost sodelavcev projekta, da ustvarijo program, ki bo osredotočen na uporabnikovo vseživljenjsko učenje in razvoj, kot tudi prepričanje, da tehnoloških rešitev ne moremo razviti brez upoštevanja pedagogike in pravil.</p>

<p>Mahara je prosto dostopna kot odprto-kodna programska oprema (v skladu s pogoji Splošne javne licence GNU). Na kratko to pomeni, da imate dovoljenje za kopiranje, uporabo in spreminjanje Mahare če se strinjate, da:
</p>

<ul>
    <li>boste zagotavljali izvorno kodo drugim;</li>
    <li>ne boste spremenili ali odstranili izvirne licence in avtorskih pravic, ter</li>
    <li>da boste uporabljali isto licenco za vsako delo, ki izvira iz tega dela.</li>
</ul>

<p>Če imate kakršnakoli vprašanja v zvezi Maharo, se lahko <a href="contact.php">obrnete na nas</a>.</p>

<p><a href="http://mahara.org">http://mahara.org</a></p>';
$string['homedefaultcontent'] = '<h2>E-listovnik Mahara</h2>

<p>Mahara je v celoti opremljen elektronski listovnik oz. portfelj, spletni dnevnik, graditelj življenjepisa in sistem socialne mreže, ki služi za povezovanje uporabnikov in ustvarjanje spletnih skupnosti. Mahara je zgrajena tako, da zagotavlja uporabnikom orodja za oblikovanje razvojnega okolja za osebno in poklicno izobraževanje.</p>

<p>Za več informacij lahko preberete stran <a href="about.php">o Mahari</a> ali pa se <a href="contact.php">obrnete na nas</a>.</p>';
$string['loggedouthomedefaultcontent'] = '<h2>E-listovnik Mahara</h2>

<p>Mahara je v celoti opremljen elektronski listovnik oz. portfelj, spletni dnevnik, graditelj življenjepisa in sistem socialne mreže, ki služi za povezovanje uporabnikov in ustvarjanje spletnih skupnosti. Mahara je zgrajena tako, da zagotavlja uporabnikom orodja za oblikovanje razvojnega okolja za osebno in poklicno izobraževanje.</p>

<p>Za več informacij lahko preberete stran <a href="about.php">o Mahari</a> ali pa se <a href="contact.php">obrnete na nas</a>.</p>';
$string['privacydefaultcontent'] = '<h2>Izjava o zasebnosti</h2>
    
<h3>Uvod</h3>

<p>Zavezani smo k varovanju vaše zasebnosti in uporabnikom varno in funkcionalno okolje za osebno učenje in razvoj. Ta izjava o zasebnosti velja za spletišče Mahara in ureja zbiranje in uporabo podatkov.</p>

<h3>Zbiranje osebnih podatkov</h3>

<p>Pri registraciji na spletišču Mahara nam boste morali posredovati določeno količino osebnih podatkov. Vaših osebnih podatkov ne bomo, brez vašega pisnega soglasja, razkrili nobeni drugi organizaciji ali osebi, razen če to dovoli ali zahteva zakon.</p>

<h3>Piškotki</h3>

<p>Da bi lahko uporabljali spletišče Mahara, morate imeti v vašem brskalniku omogočene piškotke. Prosimo upoštevajte, da se osebni podatki ne shranjujejo v piškotke, ki jih uporablja spletišče Mahara.</p>

<p>Piškotek je podatkovna datoteka, ki se namesti na vaš računalnik s spletne strani strežnika. Piškotki niso programi, vohunska programska oprema (spyware) ali virusi in sami po sebi ne morejo opravljati nobene dejavnosti.</p>

<h3>Kako lahko uporabimo vaše osebne podatke</h3>

<p>Vaše osebne podatke bomo uporabili samo za namen, ki ste nam ga navedli.</p>

<p>Kot uporabniki spletišča Mahara lahko ugotovite, katere osebne podatke dajete na razpolago drugim. Privzeto lahko drugi uporabniki vidijo le vaše prikazno ime, razen če imajo pravice dostopa kot Skrbnik, Ustvarjalec predmetov ali Mentor. To vključuje tudi navedbe v Poročilu o obvestilih ali Dnevniku obiskovalcev.</p>

<p>Zbiramo lahko tudi podatke o uporabi sistema za statistične namene, vendar ti podatki ne bodo identificirali nobenega posameznika.</p>

<h3>Shranjevanje in varnost osebnih podatkov</h3>

<p>Sprejeli bomo vse razumne ukrepe, da preprečimo izgubo, zlorabo, razkritje, spremembo ali nepooblaščen dostop
do katerih koli osebnih podatkov.</p>

<p>Da bi pripomogli k varovanju vaših osebnih podatkov, vas prosimo, da ne razkrijete vašega uporabniškega imena ali gesla nikomur, razen morda skrbniku spletišča.</p>

<h3>Spremembe te izjave zasebnosti</h3>

<p>Izjavo o zasebnosti bomo morda občasno prilagodili, da bo odražala spremembe v sistemu in kot odgovor na povratne informacije uporabnikov. Zato vam predlagamo, da preverite izjavo o zasebnosti vsakokrat, ko boste obiskali to spletišče.</p>

<h3>Kontakt</h3>

<p>Če imate kakršna koli vprašanja v zvezi s to izjavo oziroma menite, da ne izpolnjujemo navedenih meril, se nemudoma <a href="contact.php">obrnite na nas</a>. Uporabili bomo vse razumne napore za rešitev vašega problema.</p>';
$string['termsandconditionsdefaultcontent'] = '<h2>Pravila in pogoji</h2>

<p>Z uporabo [Mahare] se strinjate s pravili in pogoji, navedenimi spodaj.</p>

<h3>Naše obveznosti</h3>

<p>Skrbniki spletišča [Mahara] bodo opravili vse razumne ukrepe, da zagotovijo vsem uporabnikom varen, zanesljiv in delujoč sistem elektronskega listovnika oz. portfelja. Če kadarkoli mislite, da vašim uporabniškim pravicam ni bilo ugodeno, ali imate kakršno koli vprašanje v zvezi s spodnjimi določili, vas prosimo, da se nemudoma <a href="contact.php">obrnete na nas</a>.</p>

<p>[Mahara] občasno ne bo dostopna za krajša časovna obdobja, zaradi nadgradenje delovanja sistema. Prizadevali si bomo, da vas o vseh načrtovanih izpadih obvestimo najmanj 3 delovne dni vnaprej.</p>

<p>Spodbujamo vas, da nemudoma prijavite neprimerno vsebino ali neprimerno obnašanje <a href="contact.php">skrbniku sistema</a>. Zagotovili bomo, da bo zadeva v doglednem času raziskana.</p>

<p>Skrbniki spletišča lahko kadarkoli dostopajo do vašega listovnika oz. portfelja in njegove vsebine, vendar se bodo temu izogibali, razen če bo to izrecno potrebno za podporo vaše uporabe [Mahare], ali glede na ta pravila in pogoje.</p>

<h3>Vaše obveznosti</h3>

<p><a href="privacy.php">Izjava o zasebnosti</a> je potrebno obravnavati kot razširitev teh pravil in pogojev in jo morajo prebrati vsi uporabniki.</p>

<p>Vaš [Mahara] račun bo potekel po določenem času ali po določenem obdobju nedejavnosti, kakor je določeno s strani skrbnikov sistema. Prejeli boste epoštno opozorilo, tik preden bo vaš račun potekel in v katerem vam bomo predlagali, da shranite svoj listovnik oz. portfelj na vaš računalnik, da ga boste lahko kasneje ponovno uporabili.</p>

<p>Za vse datoteke in vsebino, ki jo prenesete na spletišče [Mahara], veljajo zakoni o avtorskih in sorodnih pravicah. Odgovorni ste za zagotovitev, da imate ustrezno dovoljenje za ponatis in objavo vsakega dela, ki ni vaše lastno. Ponarejanje bo obravnavno v skladu s pravili vaše izobraževalne ustanove.</p>

<p>Svojega listovnika oz. portfelja ne smete uporabljati za shranjevanje ali prikaz žaljivih gradiv. Če skrbnik spletišča prejme poročilo o neprimerni vsebini, objavljeni v vašem listovniku oz. portfelju, bo vaš račun začasno blokiran in dostop do [Mahare] zamrznjen do preiskave v skladu s Hišnim redom vaše izobraževalne institucije. Če Hišni red oz. podoben pravilnik ne obstaja, bo zadevo obravnaval ustrezni uslužbenec vaše ustanove ali organizacije.</p>

<p>Če skrbnik spletišča prejme poročilo o vašem neprimernem vedenju v povezavi s sistemom [Mahara], bo vaš račun začasno blokiran in dostop do [Mahare] zamrznjen do preiskave v skladu s Hišnim redom vaše izobraževalne institucije. Če Hišni red oz. podoben pravilnik ne obstaja, bo zadevo obravnaval ustrezni uslužbenec vaše ustanove ali organizacije.</p>

<p>Neprimerno vedenje vključuje zlorabo sistema poročanja o neprimernih vsebinah, poizkuse namernega nalaganja datotek z virusi, dajanje neprimernih ali pretiranih/preobsežnih povratnih informacij ali pripomb na listovniku katerega koli drugega uporabnika in vsa druga ravnanja, ki jih skrbnik šteje za neprijetna ali žaljiva.</p>

<p>Vsi nezahtevani kontakti, ki jih prejmete kot rezultat javno objavljenih osebnih podatkov, sporočenih preko vašega listovnika oz. portfelja, so vaša odgovornost. Kakršno koli kršitev obnašanja uporabnikov sistema, je treba nemudoma sporočiti <a href="contact.php">skrbniku sistema</a>. Pravila in pogoje bomo morda občasno prilagodili, da bodo odražala spremembe v sistemu in kot odgovor na povratne informacije uporabnikov. Zato vam predlagamo, da preverite pravila in pogoje vsakokrat, ko boste obiskali to spletišče. O vseh večjih spremembah pravil in pogojev vas bomo obvestili preko [Maharine] domače strani.</p>';

$string['uploadcopyrightdefaultcontent'] = 'Da: Datoteka, ki jo nameravam naložiti, je moja oz. imam pisno dovoljenje za njeno reprodukcijo in/ali distribucijo. Moja uporaba te datoteke ne krši nobenega izmed lokalnih zakonov o avtorskih in sorodnih pravicah. Ta datoteka se drži tudi Pravil in pogojev tega spletišča.';

?>
