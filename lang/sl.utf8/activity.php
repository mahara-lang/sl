<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addtowatchlist'] = 'Dodaj na nadzorni seznam';
$string['alltypes'] = 'Vsi tipi';
$string['artefacts'] = 'Izdelki';
$string['attime'] = 'ob';
$string['date'] = 'Datum';
$string['deleteallnotifications'] = 'Izbriši vsa obvestila';
$string['deletednotifications'] = 'Izbrisanih obvestil: %s';
$string['failedtodeletenotifications'] = 'Brisanje obvestil neuspešno';
$string['failedtomarkasread'] = 'Označevanje obvestila kot prebrana neuspešno';
$string['groups'] = 'Skupine';
$string['institutioninvitemessage'] = 'Svoje članstvo v tej ustanovi lahko potrdite na strani Nastavitve ustanove:';
$string['institutioninvitesubject'] = 'Povabljeni ste bili, da se pridružite ustanovi %s.';
$string['institutionrequestmessage'] = 'Uporabnike lahko dodate k ustanovi na strani Člani ustanove:';
$string['institutionrequestsubject'] = '%s je prosil/a za članstvo v %s.';
$string['markasread'] = 'Označi kot prebrano';
$string['markedasread'] = 'Označi obvestila kot prebrana';
$string['missingparam'] = 'Zahtevani parameter %s je bil prazen za tip dejavnosti %s';
$string['monitored'] = 'Opazovano';
$string['newcontactus'] = 'Nov kontakt';
$string['newcontactusfrom'] = 'Nov obrazec za kontakt';
$string['newfeedbackonartefact'] = 'Nove povratne informacije o izdelku';
$string['newfeedbackonview'] = 'Nove povratne informacije o pogledu';
$string['newgroupmembersubj'] = '%s je zdaj član skupine!';
$string['newviewaccessmessage'] = 'Dobili ste dostop do pogleda imenovanega "%s" osebe %s';
$string['newviewaccessmessagenoowner'] = 'Dobili ste dostop do pogleda imenovanega "%s"';
$string['newviewaccesssubject'] = 'Nov dostop do pogleda';
$string['newviewmessage'] = '%s je ustvaril/a nov pogled "%s"';
$string['newviewsubject'] = 'Ustvarjen nov pogled';
$string['newwatchlistmessage'] = 'Nova dejavnost na opazovanem seznamu';
$string['newwatchlistmessageview'] = '%s je spremenil/a pogled "%s"';
$string['objectionablecontentartefact'] = 'Sporna vsebina v izdelku "%s", ki jo je prijavil/a %s';
$string['objectionablecontentview'] = 'Sporna vsebina v pogledu "%s", ki jo je prijavil/a %s';
$string['ongroup'] = 'v skupini';
$string['ownedby'] = 'katere lastnik je';
$string['prefsdescr'] = 'Če izberete katerokoli izmed epoštnih možnosti, bodo obvestila kljub temu vpisana v Dnevnik dejavnosti, vendar bodo samodejno označena kot prebrana.';
$string['read'] = 'Prebrano';
$string['reallydeleteallnotifications'] = 'Ali zares želite izbrisati vsa vaša obvestila?';
$string['recurseall'] = 'Vključi vse';
$string['removedgroupmembersubj'] = '%s ni več član skupine';
$string['removefromwatchlist'] = 'Odstrani iz nadzornega seznama';
$string['selectall'] = 'Izberi vse';
$string['stopmonitoring'] = 'Nehaj opazovati';
$string['stopmonitoringfailed'] = 'Ne morem ustaviti opazovanja';
$string['stopmonitoringsuccess'] = 'Opazovanje uspešno ustavljeno';
$string['subject'] = 'Predmet';
$string['type'] = 'Tip dejavnosti';
$string['typeadminmessages'] = 'Sporočila skrbnikov';
$string['typecontactus'] = 'Kontakt';
$string['typefeedback'] = 'Povratne informacije';
$string['typegroupmessage'] = 'Sporočilo skupine';
$string['typeinstitutionmessage'] = 'Sporočilo ustanove';
$string['typemaharamessage'] = 'Sistemsko sporočilo';
$string['typeobjectionable'] = 'Sporna vsebina';
$string['typeusermessage'] = 'Sporočilo drugih uporabnikov';
$string['typeviewaccess'] = 'Nov dostop do pogleda';
$string['typevirusrelease'] = 'Sprostitev oznake virusa';
$string['typevirusrepeat'] = 'Ponovno nalaganje virusa';
$string['typewatchlist'] = 'Nadzorni seznam';
$string['unread'] = 'Neprebrano';
$string['viewmodified'] = 'je spremenil/a svoj pogled';
$string['views'] = 'Pogledi';
$string['viewsandartefacts'] = 'Pogledi in izdelki';
$string['viewsubmittedmessage'] = '%s je oddal/a svoj pogled "%s" osebi %s';
$string['viewsubmittedsubject'] = 'Pogled oddan osebi %s';
?>
