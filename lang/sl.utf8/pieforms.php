<?php

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'B';
$string['element.bytes.kilobytes'] = 'kB';
$string['element.bytes.megabytes'] = 'MB';
$string['element.bytes.gigabytes'] = 'GB';
$string['element.bytes.invalidvalue'] = 'Vrednost mora biti število';

$string['element.calendar.invalidvalue'] = 'Datum/čas ni določen pravilno';

$string['element.date.or'] = 'ali';
$string['element.date.monthnames'] = 'januar,febuar,marec,april,maj,junij,julij,avgust,september,oktober,november,december';
$string['element.date.monthnames'] = 'prosinec,svečan,sušec,mali traven,veliki traven,rožnik,mali srpan,veliki srpan,kimovec,vinotok,listopad,gruden';
$string['element.date.notspecified'] = 'Ni določeno';

$string['element.expiry.days'] = 'dni';
$string['element.expiry.weeks'] = 'tednov';
$string['element.expiry.months'] = 'mesecev';
$string['element.expiry.years'] = 'let';
$string['element.expiry.noenddate'] = 'ni zaključnega datuma';

$string['rule.before.before'] = 'To ne sme biti za poljem "%s"';

$string['rule.email.email'] = 'Epoštni naslov je neveljaven';

$string['rule.integer.integer'] = 'Vrednost polja mora biti število';

$string['rule.maxlength.maxlength'] = 'To polje mora biti dolgo največ %d znakov';

$string['rule.minlength.minlength'] = 'To polje mora biti dolgo vsaj %d znakov';

$string['rule.minvalue.minvalue'] = 'Ta vrednost ne sme biti manjša od %d';

$string['rule.regex.regex'] = 'To polje ni v veljavni obliki';

$string['rule.required.required'] = 'To polje je zahtevano';

$string['rule.validateoptions.validateoptions'] = 'Možnost "%s" je neveljavna';

$string['rule.maxvalue.maxvalue'] = 'Ta vrednost ne sme biti večja od %d';
