<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addauthority'] = 'Dodaj avtoriteto';
$string['application'] = 'Program';
$string['authloginmsg'] = 'Vpišite sporočilo, ki bo prikazano, ko se bo uporabnik želel prijaviti preko Maharinega prijavnega obrazca';
$string['authname'] = 'Naziv avtoritete';
$string['cannotremove'] = 'Ne morem odstraniti tega vtičnika za overjanje, saj je 
edini vtičnik, ki obstaja za to ustanovo.';
$string['cannotremoveinuse'] = 'Ne morem odstraniti tega vtičnika za overjanje, saj ga uporabljajo nekateri uporabniki.

Preden lahko odstranite ta vtičnik, morate posodobiti njihove zapise.';
$string['cantretrievekey'] = 'Prišlo je do napake pri prenosu javnega ključa iz oddaljenega strežnika.<br>Prosimo, zagotovite da sta polji prijave in WWW korenske mape pravilni, ter da je omogočeno povezovanje v mreže na oddaljenem gostitelju.';
$string['changepasswordurl'] = 'URL za spremembo gesla';
$string['editauthority'] = 'Uredi avtoriteto';
$string['errnoauthinstances'] = 'Za strežnik na %s ni nastavljen noben vtičnik overjanja';
$string['errnoxmlrpcinstances'] = 'Za strežnik na %s ni nastavljen noben XMLRPC vtičnik overjanja';
$string['errnoxmlrpcuser'] = 'Trenutno vas ne moremo overiti. Možna razloga sta:

    * Vaša SSO seja je morda potekla. Pojdite nazaj v drug progrma in ponovno kliknite povezavo za prijavo v Maharo.
    * Morda nimate dovoljenj za SSO v Maharo. Preverite pri skrbniku, če mislite, da bi ta dovoljenja morali imeti.';
$string['errnoxmlrpcwwwroot'] = 'Za strežnik na %s ni nobenega zapisa';
$string['errorcertificateinvalidwwwroot'] = 'To potrdilo trdi, da je za %s, vendar ga poskušate uporabiti za %s.';
$string['errorcouldnotgeneratenewsslkey'] = 'Ne morem ustvariti novega SSl ključa. Ali ste prepričani, da sta na tem strežniku nameščena openssl in PHP modul za openssl?';
$string['errornotvalidsslcertificate'] = 'To ni veljaven SSL certifikat';
$string['host'] = 'Ime ali naslov gostitelja';
$string['hostwwwrootinuse'] = 'WWW korensko mapo že uporablja druga ustanova (%s)';
$string['ipaddress'] = 'IP naslov';
$string['name'] = 'Ime spletišča';
$string['noauthpluginconfigoptions'] = 'Ni možnosti nastavitev, povezanih s tem vtičnikom';
$string['nodataforinstance'] = 'Ne najdem podatkov za primer overjanja';
$string['parent'] = 'Nadrejena avtoriteta';
$string['port'] = 'Številka vrat';
$string['protocol'] = 'Protokol';
$string['requiredfields'] = 'Zahtevana polja profila';
$string['requiredfieldsset'] = 'Zahtevana polja profila so nastavljena';
$string['saveinstitutiondetailsfirst'] = 'Prosimo, shranite podrobnosti o ustanovi pred nastavitvijo vtičnikov za overjanje.';
$string['shortname'] = 'Kratko ime spletišča';
$string['ssodirection'] = 'SSO smer';
$string['theyautocreateusers'] = 'Oni samodejno ustvarijo uporabnike';
$string['theyssoin'] = 'Oni se SSO prijavijo notri';
$string['unabletosigninviasso'] = 'Prijava preko SSO ni mogoča';
$string['updateuserinfoonlogin'] = 'Posodobi uporabniške podatke ob prijavi';
$string['updateuserinfoonlogindescription'] = 'Naloži uporabniške podatke iz oddaljenega strežnika in posodobi zapis vsakokrat, ko se uporabnik prijavi.';
$string['weautocreateusers'] = 'Mi samodejno ustvarimo uporabnike';
$string['weimportcontent'] = 'Mi uvozimo vsebino';
$string['weimportcontentdescription'] = '(samo nekateri programi)';
$string['wessoout'] = 'Mi se SSO prijavimo ven';
$string['wwwroot'] = 'WWW korenska mapa';
$string['xmlrpccouldnotlogyouin'] = 'Oprostite, ne moremo vas prijaviti :(';
$string['xmlrpccouldnotlogyouindetail'] = 'Oprostite, trenutno vas ne moremo prijaviti v Maharo. Prosimo, kmalu poizkusite ponovno. Če se težava ponavlja, obvestite skrbnika.';
$string['xmlrpcserverurl'] = 'URL XML-RPC strežnika';
?>
