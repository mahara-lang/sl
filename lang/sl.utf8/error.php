<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['accessdenied'] = 'Zavrnjen dostop';
$string['accessdeniedexception'] = 'Nimate dovoljenj za ogled te strani';
$string['artefactnotfound'] = 'Ne najdem izdelka z id številko %s';
$string['artefactnotfoundmaybedeleted'] = 'Ne najdem izdelka z id številko %s (Morda ste ga že izbrisali?)';
$string['artefactpluginmethodmissing'] = 'Vtičnik izdelka %s mora izvajati %s, ki ga ne izvaja';
$string['artefacttypeclassmissing'] = 'Vsi tipi izdelkov se morajo izvajati kot razredi. Manjka %s';
$string['artefacttypemismatch'] = 'Neusklajen tip izdelka, uporabiti želite %s kot %s';
$string['artefacttypenametaken'] = 'Tip izdelka %s že obstaja v drugem vtičniku (%s)';
$string['blockconfigdatacalledfromset'] = 'Nastavitveni podatki ne smejo biti nastavljeni neposredno, raje uporabite PluginBlocktype::instance_config_save';
$string['blockinstancednotfound'] = 'Ne najdem primerka bloka z id številko %s';
$string['blocktypelibmissing'] = 'Manjkajoča datoteka lib.php bloka %s v vtičniku izdelkov %s';
$string['blocktypemissingconfigform'] = 'Tip bloka %s mora izvajati instance_config_form';
$string['blocktypenametaken'] = 'Tip bloka %s že obstaja v drugem vtičniku (%s)';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'To bo nameščeno kot del namestitve vtičnika izdelkov %s';
$string['classmissing'] = 'manjkajoči razred %s za tip %s v vtičniku %s';
$string['couldnotmakedatadirectories'] = 'Zaradi neznanega razloga nekaj temeljnih podatkovnih map ni mogoče ustvariti. To se ne bi smelo zgoditi, saj je Mahara prej odkrila, da je korenska podatkovna mapa zapisljiva. Prosimo, preverite dovoljenja korenske podatkovne mape.';
$string['curllibrarynotinstalled'] = 'Nastavitev strežnika ne vključuje razširitve curl. Mahara potrebuje to razširitev za integracijo z Moodle-om in za nalaganje zunanjih virov. Prosimo, prepričajte se, da je razširitev curl naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['datarootinsidedocroot'] = 'Korensko podatkovno mapo ste nastavili znotraj korenske mape dokumentov. To je velik varnostni problem, saj lahko tako kdorkoli neposredno zahteva podatke sej (da bi ugrabil seje drugih uporabnikov), ali naložene datoteke drugih uporabnikov, do katerih ni dovoljen dostop. Prosimo, nastavite korensko podatkovno mapo izven korenske mape dokumentov.';
$string['datarootnotwritable'] = 'Korenska podatkovna mapa, %s, ni zapisljiva. To pomeni, da seje, uporabniške datoteke ali karkoli drugega, kar mora biti naloženo na strežnik, ne more biti shranjeno. Prosimo, ustvarite mapo, če ta ne obstaja, ali pa nastavite spletni strežnik za lastnika te mape.';
$string['dbconnfailed'] = 'Mahara se ne more povezati s podatkovno bazo.

 * Če ste uporabnik Mahare, prosimo, počakajte nekaj trenutkov, nato poizkusite ponovno.
 * Če ste skrbnik sistema, prosimo, preverite nastavitve podatkovne baze ter se prepričajte, da je podatkovna baza na voljo.

Javljena je bila napaka:';
$string['dbnotutf8'] = 'Ne uporabljate UTF-8 podatkovne baze. Mahara shranjuje vse podatke kot UTF-8. Prosimo, izbrište ter ponovno ustvarite podatkovno bazo z UTF-8 kodiranjem.';
$string['dbversioncheckfailed'] = 'Različica strežnika podatkovne baze je prestara za uspešno izvajanje Mahare. Strežnik je %s %s, toda Mahara zahteva vsaj različico %s.';
$string['domextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve dom. Mahara potrebuje to razširitev za razčlenitev XML podatkov iz različnih virov.';
$string['gdextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve gd. Mahara potrebuje to razširitev za povečevanje/zmanjševanje in druga opravila na naloženih slikah. Prosimo, prepričajte se, da je razširitev naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['gdfreetypenotloaded'] = 'Nastavitev strežnika za razširitev gd ne vključuje podpore za Freetype. Mahara potrebuje to podporo za izdelavo CAPTCHA slik. Prosimo, prepričajte se, da gd vključuje podporo za Freetype.';
$string['interactioninstancenotfound'] = 'Ne najdem primerka dejavnosti z id številko %s';
$string['invaliddirection'] = 'Neveljavna smer %s';
$string['invalidviewaction'] = 'Neveljavno dejanje kontrole pogledov: %s';
$string['jsonextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve JSON. Mahara potrebuje to razširitev za pošiljena v in iz brskalnika. Prosimo, prepričajte se, da je razširitev naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['magicquotesgpc'] = 'Nevarna PHP nastavitev, magic_quotes_gpc je vključena. Mahara bo verjetno delovala, vendar bi to težavo morali odpraviti.';
$string['magicquotesruntime'] = 'Nevarna PHP nastavitev, magic_quotes_runtime je vključena. Mahara bo verjetno delovala, vendar bi to težavo morali odpraviti.';
$string['magicquotessybase'] = 'Nevarna PHP nastavitev, magic_quotes_sybase je vključena. Mahara bo verjetno delovala, vendar bi to težavo morali odpraviti.';
$string['missingparamblocktype'] = 'Try selecting a block type to add first';
$string['missingparamcolumn'] = 'Manjkajoči predpis stolpca';
$string['missingparamid'] = 'Manjkajoča id številka';
$string['missingparamorder'] = 'Manjkajoči predpis vrstnega reda';
$string['mysqldbextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve mysql. Mahara potrebuje to razširitev za shranjevanje podatkov v podatkovni bazi. Prosimo, prepričajte se, da je razširitev naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['notartefactowner'] = 'Niste lastni tega izdelka';
$string['notfound'] = 'Ne najdem';
$string['notfoundexception'] = 'Ne najdem strani, ki jo iščete';
$string['onlyoneblocktypeperview'] = 'V pogled lahko vključite samo en blok %s';
$string['onlyoneprofileviewallowed'] = 'Dovoljen vam je le en pogled profila';
$string['parameterexception'] = 'Obvezni parameter manjka';
$string['pgsqldbextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve pgsql. Mahara potrebuje to razširitev za shranjevanje podatkov v podatkovni bazi. Prosimo, prepričajte se, da je razširitev naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['phpversion'] = 'Mahara za delovanje potrebuje PHP različice 5.1.3 ali novejše. Prosimo, nadgradite različico PHP, ali preselite Maharo na drug strežnik.';
$string['registerglobals'] = 'Nevarna PHP nastavitev, register_globals je vključena. Mahara bo verjetno delovala, vendar bi to težavo morali odpraviti.';
$string['safemodeon'] = 'Zdi se, da strežnik teče v varnem načinu. Mahara ne porpira izvajanja v varnem načinu. To nastavitev morate izključiti v datoteki php.ini, ali v apache nastavitvah za spletišče.

Če ste na deljenem gostovanju, potem za izključitev nastavitve safe_mode ne morete naredi ničesar drugega, razen da prosite ponudnika. Morda bi morali premisliti o selitvi na drug strežnik.';
$string['sessionextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve session. Mahara potrebuje to razširitev za podporo prijavljanju uporabnikov v sistem. Prosimo, prepričajte se, da je razširitev naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['unknowndbtype'] = 'Nastavitev strežnika se sklicuje na neznani tip podatkovne baze. Veljavni vrednosti sta "postgres8" in "mysql5". Prosimo, spremenite nastavitev tipa podatkovne baze v config.php.';
$string['unrecoverableerror'] = 'Zgodila se je napaka, ki je ne morem popraviti. To verjetno pomeni, da ste naleteli na napako v sistemu.';
$string['unrecoverableerrortitle'] = '%s - spletišče nedostopno';
$string['versionphpmissing'] = 'Vtičniku %s %s manjka datoteka version.php!';
$string['viewnotfound'] = 'Ne najdem pogleda z id številko %s';
$string['viewnotfoundexceptionmessage'] = 'Pogled, ki si ga želite ogledati, ne obstaja!';
$string['viewnotfoundexceptiontitle'] = 'Ne najdem pogleda';
$string['xmlextensionnotloaded'] = 'Nastavitev strežnika ne vključuje razširitve %s. Mahara potrebuje to razširitev za razčlenitev XML podatkov iz različnih virov. Prosimo, prepričajte se, da je razširitev naložena v php.ini, ali jo namestite, če še ni nameščena.';
$string['youcannotviewthisusersprofile'] = 'Ne morete videti uporabniškega profila tega uporabnika';
?>
