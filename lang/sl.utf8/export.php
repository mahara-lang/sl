<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Done'] = 'Končano';
$string['Export'] = 'Izvoz';
$string['Starting'] = 'Začenjam';
$string['allmydata'] = 'Vse moje podatke';
$string['chooseanexportformat'] = 'Izberite obliko izvoza';
$string['clicktopreview'] = 'Kliknite za predogled';
$string['creatingzipfile'] = 'Ustvarjam stisnjeno datoteko';
$string['exportgeneratedsuccessfully'] = 'Izvoz je uspešno ustvarjen. %sZa prenos kliknite tukaj%s';
$string['exportgeneratedsuccessfullyjs'] = 'Izvoz je uspešno ustvarjen. %sNadaljuj%s';
$string['exportingartefactplugindata'] = 'Izvažanje podatkov vtičnika izdelkov';
$string['exportingartefacts'] = 'Izvažanje izdelkov';
$string['exportingartefactsprogress'] = 'Izvažanje izdelkov: %s/%s';
$string['exportingfooter'] = 'Izvažanje noge';
$string['exportingviews'] = 'Izvažanje pogledov';
$string['exportingviewsprogress'] = 'Izvažanje pogledov: %s/%s';
$string['exportpagedescription'] = 'Tukaj lahko izvozite vaš listovnik. To orodje izvozi vse podatke in poglede vašega listovnika, ne izvozi pa vaših nastavitev.';
$string['exportyourportfolio'] = 'Izvozite vaš listovnik';
$string['generateexport'] = 'Ustvari izvoz';
$string['justsomeviews'] = 'Samo nekatere izmed mojih pogledov';
$string['noexportpluginsenabled'] = 'Skrbnik spletišča ni omogočil nobenega vtičnika za izvoz, zato te možnosti ne morete uporabiti';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Prosimo počakajte, dokler se ustvarja vaš izvoz...';
$string['setupcomplete'] = 'Namestitev končana';
$string['unabletoexportportfoliousingoptions'] = 'Ne morem izvoziti listovnika z izbranimi možnostmi';
$string['unabletogenerateexport'] = 'Ne morem ustvariti izvoza';
$string['viewstoexport'] = 'Pogledi za izvoz';
$string['whatdoyouwanttoexport'] = 'Kaj želite izvoziti?';
$string['writingfiles'] = 'Zapisovanje datotek';
$string['youarehere'] = 'Tukaj ste';
$string['youmustselectatleastoneviewtoexport'] = 'Za izvoz morate izbrati vsaj en pogled';
$string['zipnotinstalled'] = 'Vaš sistem nima ukaza zip. Prosimo namestite ukaz zip, za omogočanje te možnosti';
?>
