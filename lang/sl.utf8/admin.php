<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Add'] = 'Dodaj';
$string['Admins'] = 'Skrbniki';
$string['Close'] = 'Zapri';
$string['Everyone'] = 'Kdorkoli';
$string['Field'] = 'Polje';
$string['Institution'] = 'Ustanova';
$string['Lockedfields'] = 'Zaklenjena polja';
$string['Maximum'] = 'Največ';
$string['Members'] = 'Članov';
$string['Non-members'] = 'Ne-člani';
$string['Open'] = 'Odpri';
$string['Or...'] = 'Ali...';
$string['Plugin'] = 'Vtičnik';
$string['Query'] = 'Poizvedba';
$string['Register'] = 'Registracija';
$string['Staff'] = 'Osebje';
$string['Value'] = 'Vrednost';
$string['about'] = 'O Mahari';
$string['accountexpiry'] = 'Račun poteče';
$string['accountexpirydescription'] = 'Datum, ko bo uporabnikova prijava samodejno onemogočena.';
$string['accountsettings'] = 'Nastavitve računa';
$string['addinstitution'] = 'Dodaj ustanovo';
$string['addmembers'] = 'Dodaj člane';
$string['addnewmembers'] = 'Dodaj nove člane';
$string['adduser'] = 'Dodaj uporabnika';
$string['adduserdescription'] = 'Ustvarjanje novega uporabnika';
$string['adduserpagedescription'] = '<p>Tukaj lahko dodate novega uporabnika. Ko bo dodan, bo dobil epoštno sporočilo s podatki o računu, vključno z uporabniškim imenom in geslom. Geslo bo moral spremeniti ob prvi prijavi.</p>';
$string['addusertoinstitution'] = 'Dodaj uporabnika k ustanovi';
$string['adminauthorities'] = 'Skrbništvo avtoritet';
$string['adminfilespagedescription'] = 'Tukaj lahko naložite datoteke, ki jih lahko vključite v %sMeni povezav in virov%s. Datoteke v domači mapi lahko dodate v meni prijavljenih uporabnikov, medtem ko lahko datoteke v javni (public) mapi dodate v javno dostopni meni.';
$string['adminhome'] = 'Skrbništvo';
$string['admininstitutions'] = 'Skrbništvo ustanov';
$string['administration'] = 'Skrbništvo';
$string['adminnoauthpluginforinstitution'] = 'Prosim, nastavi vtičnik za overjanje za to ustanovo.';
$string['adminnotifications'] = 'Skrbniška obvestila';
$string['adminnotificationsdescription'] = 'Pregled nad tem, kako skrbniki dobijo sistemska obvestila';
$string['adminpublicdirdescription'] = 'Datoteke, ki so dostopne neprijavljenim uporabnikom';
$string['adminpublicdirname'] = 'public';
$string['adminsandstaffonly'] = 'Samo skrbniki in osebje';
$string['adminsonly'] = 'Samo skrbniki';
$string['adminusers'] = 'Skrbniki';
$string['adminusersdescription'] = 'Določanje uporabnikov z dostopom za skrbnike';
$string['adminuserspagedescription'] = '<p>Tukaj lahko izberete, kateri uporabniki so skrbniki spletišča. Trenutni skrbniki so na desni, možni skrbniki pa na levi.</p><p>Sistem mora imeti vsaj enega skrbnika.</p>';
$string['adminusersupdated'] = 'Skrbniki posodobljeni';
$string['agreelicense'] = 'Strinjam se';
$string['allowpublicprofiles'] = 'Dovoli javne profile';
$string['allowpublicprofilesdescription'] = 'Če je nastavljeno na Da, lahko uporabniki nastavijo poglede profila, ki so dostopni javnosti in ne le prijavljenim uporabnikom';
$string['allowpublicviews'] = 'Dovoli javne poglede';
$string['allowpublicviewsdescription'] = 'Če je nastavljeno na Da, lahko uporabniki ustvarijo poglede listovnika, ki so dostopni javnosti in ne le prijavljenim uporabnikom';
$string['authenticatedby'] = 'Metoda overjanja';
$string['authenticatedbydescription'] = 'Kako Mahara overi pristnost uporabnika';
$string['authplugin'] = 'Vtičnik za overjanje';
$string['badmenuitemtype'] = 'Neznan tip elementa';
$string['basicdetails'] = '2 - Osnovni podatki';
$string['basicinformationforthisuser'] = 'Osnovni podatki uporabnika.';
$string['becomeadminagain'] = 'Postanite ponovno %s';
$string['captchaoncontactform'] = 'Captcha je potrebna za kontakt';
$string['captchaoncontactformdescription'] = 'Neprijavljeni uporabniki morajo pri pošiljanju vsebine kontaktnega obrazca pretipkati znake iz captcha slike';
$string['captchaonregisterform'] = 'Captcha je potrebna za registracijo';
$string['captchaonregisterformdescription'] = 'Uporabniki morajo pri pošiljanju obrazca za registracijo pretipkati znake iz captcha slike';
$string['changeinstitution'] = 'Zamenjaj ustanovo';
$string['clickthebuttontocreatetheuser'] = 'Kliknite gumb za ustvarjanje uporabnika.';
$string['closesite'] = 'Zaprite spletišče';
$string['closesitedetail'] = 'Dostop do spletišča lahko omejite vsem, razen skrbnikom. To je priročno, kadar se pripravljate na posodobitev podatkovne baze. Dokler ne boste ponovno odprli spletišča ali posodobitev ne bo uspešno zaključena, se bodo v spletišče lahko prijavili samo skrbniki.';
$string['component'] = 'Komponenta ali vtičnik';
$string['configextensions'] = 'Razširitve';
$string['configsite'] = 'Nastavitve';
$string['configureauthplugin'] = 'Nastaviti morate vtičnik za preverjanje pristnosti, preden lahko dodate uporabnike';
$string['configusers'] = 'Uporabniki';
$string['confirm'] = 'potrdi';
$string['confirmdeletemenuitem'] = 'Ali zares želite izbrisati ta element?';
$string['confirmdeleteuser'] = 'Ali zares želite izbrisati tega uporabnika?';
$string['confirmdeleteusers'] = 'Ali zares želite izbrisati izbrane uporabnike?';
$string['confirmremoveuserfrominstitution'] = 'Ali zares želite odstraniti uporabnika iz te ustanove?';
$string['continue'] = 'Nadaljuj';
$string['copyright'] = 'Copyright &copy; od 2006 dalje, Catalyst IT Ltd.';
$string['coredata'] = 'podatki jedra';
$string['coredatasuccess'] = 'Podatki jedra uspešno nameščeni';
$string['create'] = '3 - Ustvari';
$string['createnewuserfromscratch'] = 'Ustvari novega uporabnika iz nič';
$string['createuser'] = 'Ustvari uporabnika';
$string['csverroremptyfile'] = 'CSV datoteka je prazna.';
$string['csvfile'] = 'CSV datoteka';
$string['csvfiledescription'] = 'Datoteka, ki vsebuje uporabnike, ki naj bodo dodani';
$string['currentadmins'] = 'Trenutni skrbniki';
$string['currentmembers'] = 'Trenutni člani';
$string['currentstaff'] = 'Trenutno osebje';
$string['databasenotutf8'] = 'Ne uporabljate UTF-8 podatkovne baze. UTF-8 je priporočeno kodiranje znakov za Maharo.';
$string['datathatwillbesent'] = 'Podatki, ki bodo poslani';
$string['dbcollationmismatch'] = 'Stolpec podatkovne baze uporablja razvrščanje (collation), ki ni isto kot privzeto razvrščanje podatkovne baze. Prosimo vas, naj vsi stolpci uporabljajo isto razvrščanje kot podatkovna baza.';
$string['dbnotutf8warning'] = 'Ne uporabljate UTF-8 podatkovne baze. Mahara shranjuje vse podatke kot UTF-8. Še vedno lahko poizkusite izvesti nadgradnjo, vendar vam priporočamo, da pretvorite podatkovno bazo v UTF-8.';
$string['declinerequests'] = 'Zavrni prošnje';
$string['defaultaccountinactiveexpire'] = 'Privzeti čas nedejavnosti računa';
$string['defaultaccountinactiveexpiredescription'] = 'Kako dolgo bo ostal uporabniški račun aktiven, ne da bi se uporabnik prijavil';
$string['defaultaccountinactivewarn'] = 'Čas opozorila pred nedejavnostjo/iztekom';
$string['defaultaccountinactivewarndescription'] = 'Čas, ko naj bo poslano opozorilo uporabnikom, da bodo njihovi računi potekli ali postali neaktivni';
$string['defaultaccountlifetime'] = 'Privzeta življenjska doba računa';
$string['defaultaccountlifetimedescription'] = 'Če je nastavljeno, bodo po tem obdobju uporabniški računi potekli (merjeno od časa njihovega nastanka)';
$string['defaultmembershipperiod'] = 'Privzeto trajanje članstva';
$string['defaultmembershipperioddescription'] = 'Kako dolgo ostanejo novi uporabniki člani ustanove';
$string['deletefailed'] = 'Brisanje elementa neuspešno';
$string['deleteinstitution'] = 'Izbriši ustanovo';
$string['deleteinstitutionconfirm'] = 'Ali zares želite izbrisati to ustanovo?';
$string['deleteuser'] = 'Izbriši uporabnika';
$string['deleteusernote'] = 'Prosimo, zapomnite si, da te operacije <strong>ne morete preklicati.</strong>';
$string['deleteusers'] = 'Izbriši uporabnike';
$string['deletingmenuitem'] = 'Brisanje elementa';
$string['discardpageedits'] = 'Ali zares želite zavreči spremembe te strani?';
$string['editadmins'] = 'Uredi skrbnike';
$string['editmembers'] = 'Uredi člane';
$string['editmenus'] = 'Uredi povezave in vire';
$string['editsitepages'] = 'Uredi strani spletišča';
$string['editsitepagesdescription'] = 'Urejanje vsebine različnih strani spletišča';
$string['editsitepagespagedescription'] = 'Tukaj lahko urejate vsebino nekaterih strani spletišča, kot npr. začetna stran (ločeno za prijavljene in neprijavljene uporabnike), ter strani, povezane v nogo spletišča.';
$string['editstaff'] = 'Uredi osebje';
$string['emailusersaboutnewaccount'] = 'Pošljem epošto uporabnikom o njihovih računih?';
$string['emailusersaboutnewaccountdescription'] = 'Ali naj bo uporabnikom poslana epošta za obveščanje o podrobnostih njihovega novega računa';
$string['embeddedcontent'] = 'Vgrajena vsebina';
$string['embeddedcontentdescription'] = 'Če želite, da bodo uporabniki v svoje listovnike lahko vgradili videoposnetke in druge zunanje vsebine, lahko spodaj izberete, katerim stranem zaupate.';
$string['enablenetworking'] = 'Omogoči mreženje';
$string['enablenetworkingdescription'] = 'Dovoli Mahara strežniku da komunicira s strežniki, na katerih teče Moodle in druge aplikacije';
$string['errors'] = 'Napake';
$string['errorwhilesuspending'] = 'Prišlo blokiranju je prišlo do napake';
$string['errorwhileunsuspending'] = 'Prišlo aktiviranju je prišlo do napake';
$string['exportingnotsupportedyet'] = 'Izvažanje uporabniških profilov še ni podprto';
$string['exportuserprofiles'] = 'Izvozi uporabniške profile';
$string['externallink'] = 'Zunanja povezava';
$string['failedtoobtainuploadedleapfile'] = 'Ne morem pridobiti naložene LEAP2A datoteke';
$string['failedtounzipleap2afile'] = 'Ne morem razširiti LEAP2A datoteke. Za več informacij preverite dnevnik napak.';
$string['fileisnotaziporxmlfile'] = 'Ta datoteka ni stisnjena zip datoteka ali XML datoteka';
$string['filequota'] = 'Kvota za datoteke (MB)';
$string['filequotadescription'] = 'Količina prostora, ki je namenjena uporabniku za shranjevanje datotek.';
$string['filtersinstalled'] = 'Filtri nameščeni.';
$string['forcepasswordchange'] = 'Vsili spremembo gesla ob naslednji prijavi';
$string['forcepasswordchangedescription'] = 'Uporabnik bo usmerjen na stran za spremembo gesla ob naslednji prijavi.';
$string['forceuserstochangepassword'] = 'Vsilim spremembo gesla?';
$string['forceuserstochangepassworddescription'] = 'Ali naj bodo uporabniki prisiljeni spremeniti svoje geslo, ko se prvič prijavijo';
$string['fromversion'] = 'Iz različice';
$string['home'] = 'Začetna stran';
$string['howdoyouwanttocreatethisuser'] = 'Kako želite ustvariti uporabnika?';
$string['htmlfilters'] = 'HTML Filtri';
$string['htmlfiltersdescription'] = 'Omogočanje novih filtrov za HTML Purifier';
$string['information'] = 'Informacije';
$string['install'] = 'Namesti';
$string['installation'] = 'Namestitev';
$string['installed'] = 'Nameščeno';
$string['installsuccess'] = 'Uspešno nameščena različica';
$string['institutionaddedsuccessfully2'] = 'Ustanova uspešno dodana';
$string['institutionadmin'] = 'Skrbništvo ustanove';
$string['institutionadmindescription'] = 'Če je obkljukano, lahko uporabnik skrbi za vse uporabnike v tej ustanovi.';
$string['institutionadministration'] = 'Skrbništvo ustanove';
$string['institutionadministrator'] = 'Skrbnik ustanove';
$string['institutionadmins'] = 'Skrbniki ustanove';
$string['institutionadminsdescription'] = 'Določanje uporabnikov z dostopom za skrbnike ustanove';
$string['institutionadminuserspagedescription'] = 'Tukaj lahko izberete, kateri uporabniki so skrbniki ustanove. Trenutni skrbniki so na desni, možni skrbniki pa na levi.';
$string['institutionauth'] = 'Avtoritete ustanove';
$string['institutionautosuspend'] = 'Samodejno blokiraj pretečene ustanove';
$string['institutionautosuspenddescription'] = 'Če je obkljukano, bodo pretečene ustanove samodejno blokirane.';
$string['institutiondeletedsuccessfully'] = 'Ustanova uspešno izbrisana';
$string['institutiondetails'] = 'Podrobnosti ustanove';
$string['institutiondisplayname'] = 'Prikazno ime ustanove';
$string['institutionexpiry'] = 'Datum izteka ustanove';
$string['institutionexpirydescription'] = 'Datum, ko bo članstvo v ustanovi %s blokirano.';
$string['institutionexpirynotification'] = 'Čas opozorila pred iztekom ustanove';
$string['institutionexpirynotificationdescription'] = 'Preden se bo čas ustanove iztekel, bo skrbnikom spletišča in skrbnikom ustanove poslano obvestilo';
$string['institutionfiles'] = 'Datoteke ustanove';
$string['institutionfilesdescription'] = 'Nalaganje in upravljanje datotek za uporabo v Pogledih ustanove';
$string['institutionmaxusersexceeded'] = 'Ta ustanova je polna. Preden boste lahko dodali nove uporabnike, boste morali povečati dovoljeno število uporabnikov v ustanovi.';
$string['institutionmembers'] = 'Člani ustanove';
$string['institutionmembersdescription'] = 'Povezovanje uporabnikov z ustanovami';
$string['institutionmemberspagedescription'] = 'Na tej strani si lahko ogledate uporabnike, ki so zaprosili za članstvo v ustanovi in jih lahko dodate kot člane. Lahko tudi odstranite uporabnike iz ustanove ali povabite uporabnike, da se pridružijo.';
$string['institutionname'] = 'Ime ustanove';
$string['institutionnamealreadytaken'] = 'To ime ustanove je že zasedeno';
$string['institutions'] = 'Ustanove';
$string['institutionsdescription'] = 'Namestitev in upravljanje nameščenih ustanov';
$string['institutionsettings'] = 'Nastavitve ustanove';
$string['institutionsettingsdescription'] = 'Tukaj lahko spremenite nastavitve glede članstva tega uporabnika v ustanovah, ki so v sistemu.';
$string['institutionstaff'] = 'Osebje ustanove';
$string['institutionstaffdescription'] = 'Določanje uporabnikov z dostopom za osebje';
$string['institutionstaffuserspagedescription'] = 'Tukaj lahko izberete, kateri uporabniki so osebje ustanove. Trenutno osebje je na desni, možno osebje pa na levi.';
$string['institutionstudentiddescription'] = 'Neobvezna oznaka, ki je specifična za ustanovo. Tega polja uporabnik ne more urejati.';
$string['institutionsuspended'] = 'Ustanova blokirana';
$string['institutionunsuspended'] = 'Ustanova ponovno aktivirana';
$string['institutionupdatedsuccessfully'] = 'Ustanova uspešno posodobljena';
$string['institutionuserserrortoomanyinvites'] = 'Vaša vabila niso poslana. Število obstoječih članov in število izstavljenih vabil, ne sme presegati največjega dovoljenega števila uporabnikov za ustanovo. Lahko povabite manj uporabnikov, odstranite nekaj uporabnikov iz ustanove, ali prosite skrbnika spletišča, da poveča največje dovoljeno število uporabnikov.';
$string['institutionuserserrortoomanyusers'] = 'Uporabniki niso dodani. Število članov ne sme presegati največjega dovoljenega števila za ustanovo. Lahko dodate manj uporabnikov, odstranite nekaj uporabnikov iz ustanove, ali prosite skrbnika spletišča, da poveča največje dovoljeno število uporabnikov.';
$string['institutionusersinstructionsmembers'] = 'Seznam uporabnikov na levi strani prikazuje vse uporabnike, ki so člani ustanove. Lahko uporabite polje za iskanje, da zmanjšate število prikazanih uporabnikov. Če želite odstraniti uporabnike iz ustanove, morate najprej premakniti nekaj uporabnikov na desno stran. To storite tako, da izberete enega ali več uporabnikov in nato kliknete na desno puščico. Gumb "Odstrani uporabnike" bo iz ustanove odstranil vse uporabnike na desni strani. Uporabniki na levi strani bodo ostali člani ustanove.';
$string['institutionusersinstructionsnonmembers'] = 'Seznam uporabnikov na levi strani prikazuje vse uporabnike, ki še niso člani ustanove. Lahko uporabite polje za iskanje, da zmanjšate število prikazanih uporabnikov. Če želite povabiti uporabnike, naj se pridružijo ustanovi, morate najprej premakniti nekaj uporabnikov na desno stran. To storite tako, da izberete enega ali več uporabnikov in nato kliknete na desno puščico. Z gumbom "Povabi uporabnike" boste poslali povabila vsem uporabnikom na desni strani. Ti uporabniki ne bodo pridruženi ustanovi, dokler ne bodo sprejeli povabila.';
$string['institutionusersinstructionsrequesters'] = 'Seznam uporabnikov na levi strani prikazuje vse uporabnike, ki so zaprosili za članstvo v ustanovi. Lahko uporabite polje za iskanje, da zmanjšate število prikazanih uporabnikov. Če želite dodati uporabnike ustanovi ali zavrniti njihove prošnje za članstvo, morate najprej premakniti nekaj uporabnikov na desno stran. To storite tako, da izberete enega ali več uporabnikov in nato kliknete na desno puščico. Gumb "Dodaj člane" bo pridružil ustanovi vse uporabnike na desni strani. Gumb "Zavrni prošnje" bo odstranil prošnje za članstvo vseh uporabnikov na desni strani.';
$string['institutionusersmembers'] = 'Ljudje, ki so že člani ustanove';
$string['institutionusersnonmembers'] = 'Ljudje, ki še niso zaprosili za članstvo';
$string['institutionusersrequesters'] = 'Ljudje, ki so zaprosili za članstvo v ustanovi';
$string['institutionusersupdated_addUserAsMember'] = 'Uporabniki dodani';
$string['institutionusersupdated_declineRequestFromUser'] = 'Prošnje zavrnjene';
$string['institutionusersupdated_inviteUser'] = 'Povabila poslana';
$string['institutionusersupdated_removeMembers'] = 'Uporabniki odstranjeni';
$string['institutionviews'] = 'Pogledi ustanove';
$string['institutionviewsdescription'] = 'Ustvarjanje in upravljanje Pogledov in Predlog pogledov za ustanovo';
$string['invalidfilename'] = 'Datoteka "%s" ne obstaja';
$string['invitationsent'] = 'Povabilo poslano';
$string['invitedby'] = 'Povabil/a';
$string['inviteusers'] = 'Povabi uporabnike';
$string['inviteuserstojoin'] = 'Povabi uporabnike, da se pridružijo ustanovi';
$string['jsrequiredforupgrade'] = 'Za namestitev ali nadgradnjo morate vključiti JavaScript.';
$string['langchoosereport'] = 'Izberite poročilo za prikaz:';
$string['langmissingstrings'] = 'Manjkajoči nizi';
$string['langreports'] = 'Jezikovno poročilo';
$string['language'] = 'Jezik';
$string['languagetranslation'] = 'Prevod jezika';
$string['leap2aimportfailed'] = '<p><strong>Oprostite - Neuspešno uvažanje LEAP2A datoteke.</strong></p><p>To se je verjetno zgodilo ker za nalaganje niste izbrali veljavne LEAP2A datoteke. Ali pa Mahara vsebuje napako, ki povzroča neuspešno uvažanje vaše datoteke, čeprav je datoteka veljavna.</p><p>Prosimo, <a href="add.php">pojdite nazaj in poizkusite ponovno</a>. Če težava ne izgine, potem lahko v <a href="http://mahara.org/forums/">Maharinih forumih</a> vprašate za pomoč. Bodite pripravljeni, da vas bodo prosili za kopijo vaše datoteke!</p>';
$string['linkedto'] = 'Povezano';
$string['linksandresourcesmenu'] = 'Meni povezav in virov';
$string['linksandresourcesmenudescription'] = 'Upravljanje povezav in datotek znotraj Menija povezav in virov';
$string['linksandresourcesmenupagedescription'] = 'Meni povezav in virov se pri vseh uporabnikih pojavlja na večini strani. Sem lahko dodate povezave do drugih spletnih strani in do datotek, ki so naložene v razdelku %sDatoteke spletišča%s.';
$string['loadingmenuitems'] = 'Nalaganje elementov';
$string['loadmenuitemsfailed'] = 'Nalaganje elementov neuspešno';
$string['loadsitepagefailed'] = 'Nalaganje strani spletišča neuspešno';
$string['localdatasuccess'] = 'Uspešno nameščene lokalne prilagoditve';
$string['loggedinmenu'] = 'Prijavljenim dostopne povezave in viri';
$string['loggedouthome'] = 'Začetna stran za neprijavljene';
$string['loggedoutmenu'] = 'Javne povezave in viri';
$string['loginasdenied'] = 'Poskus, da se brez dovoljenja prijavite kot drug uporabnik';
$string['loginasoverridepasswordchange'] = 'Ker se izdajate za drugega uporabnika, se lahko vseeno %sprijavite%s, ne glede na stran za spremembo geslo.';
$string['loginasrestorenodata'] = 'Ni uporabniških podatkov za obnovitev';
$string['loginastwice'] = 'Poskus, da se prijavite kot drug uporabnik, čeprav ste že prijavljeni kot drug uporabnik';
$string['loginasuser'] = 'Prijavite se kot %s';
$string['manageinstitutions'] = 'Ustanove';
$string['maxuseraccounts'] = 'Največ dovoljenih uporabniških računov';
$string['maxuseraccountsdescription'] = 'Največje število uporabniških računov, ki so povezani z ustanovo. Če ni omejitev, pustite to polje prazno.';
$string['membershipexpiry'] = 'Članstvo poteče';
$string['membershipexpirydescription'] = 'Datum, na katerega bo uporabnik samodejno odstranjen iz ustanove.';
$string['menuitemdeleted'] = 'Element izbrisan';
$string['menuitemsaved'] = 'Element shranjen';
$string['menuitemsloaded'] = 'Elementi naloženi';
$string['name'] = 'Ime';
$string['networking'] = 'Mreženje';
$string['networkingdescription'] = 'Nastavitev Mahara omrežja';
$string['networkingdisabled'] = 'Mreženje je onemogočeno.';
$string['networkingenabled'] = 'Mreženje je omogočeno.';
$string['networkingextensionsmissing'] = 'Oprostite, ne morete nastaviti Mahara mreženja, saj PHP namestitvi manjka ena ali več zahtevanih razširitev:';
$string['networkingpagedescription'] = 'Maharine lastnosti mreženja ji omogočajo, da komunicira z Mahara ali Moodle spletišči, ki tečejo na istem ali drugih strežnikih. Če je mreženje omogočeno, ga lahko uporabite za nastavitev enkratne-prijave (SSO) za uporabnike, ki se lahko prijavijo bodisi v Moodle ali v Maharo.';
$string['networkingunchanged'] = 'Nastavitve mreženja niso spremenjene';
$string['newfiltersdescription'] = 'Če ste naložili nov sklop HTML filtrov, jih lahko namestite tako, da datoteko razširite v mapo %s in nato kliknete na spodnji gumb';
$string['newusercreated'] = 'Uspešno ustvarjen nov uporabniški račun';
$string['newuseremailnotsent'] = 'Pošiljanje epošte z dobrodošlico novemu uporabniku ni uspelo.';
$string['noauthpluginforinstitution'] = 'Skrbnik spletišča za to ustanovo ni nastavil vtičnika za overjanje.';
$string['nofiltersinstalled'] = 'Ni nameščenih HTML filtrov.';
$string['noinstitutions'] = 'Brez ustanov';
$string['noinstitutionsdescription'] = 'Če želite pridružiti uporabnike k ustanovi, morate ustanovo najprej ustvariti.';
$string['noleap2axmlfiledetected'] = 'Ne najdem datoteke leap2a.xml - ponovno preverite izvozno datoteko';
$string['nositefiles'] = 'Na voljo ni nobene datoteke spletišča';
$string['notadminforinstitution'] = 'Niste skrbnik te ustanove';
$string['nothingtoupgrade'] = 'Ničesar za nadgradnjo';
$string['notificationssaved'] = 'Nastavitve obveščanja shranjene';
$string['notinstalled'] = 'Ni nameščeno';
$string['noupgrades'] = 'Ničesar za nadgradnjo! Uporabljate najnovejšo različico!';
$string['nousersselected'] = 'Ni izbranih uporabnikov';
$string['nousersupdated'] = 'Noben uporabnik ni posodobljen';
$string['onlyshowingfirst'] = 'Prikazujem samo prvih';
$string['pagename'] = 'Ime strani';
$string['pagesaved'] = 'Stran shranjena';
$string['pagetext'] = 'Besedilo strani';
$string['pathtoclam'] = 'Pot do ClamAV';
$string['pathtoclamdescription'] = 'Sistemska pot do protivirusnega programa clamscan ali clamdscan';
$string['performinginstallation'] = 'Izvajanje namestitve...';
$string['performinginstallsandupgrades'] = 'Izvajam namestitve in nadgradnje...';
$string['performingupgrades'] = 'Izvajanje nadgradnje...';
$string['pluginadmin'] = 'Upravljanje vtičnikov';
$string['pluginadmindescription'] = 'Namestitev in nastavitev vtičnikov';
$string['potentialadmins'] = 'Možni skrbniki';
$string['potentialstaff'] = 'Možno osebje';
$string['privacy'] = 'Izjava o zasebnosti';
$string['promiscuousmode'] = 'Samo-registriraj vse gostitelje';
$string['promiscuousmodedescription'] = 'Ustvari evidenco ustanov za vsakega gostitelja, ki povezuje z vami, in omogoča uporabnikom, da se prijavijo v Maharo';
$string['promiscuousmodedisabled'] = 'Samo-registracija je onemogočena.';
$string['promiscuousmodeenabled'] = 'Samo-registracija je omogočena.';
$string['proxyaddress'] = 'Proxy naslov';
$string['proxyaddressdescription'] = 'Če vaša stran uporablja proxy strežnik za dostop do interneta, določite pooblastila v <em>ime_gostitelja:številka_vrat</em> zapisu';
$string['proxyaddressset'] = 'Proxy naslov nastavljen';
$string['proxyauthcredentials'] = 'Proxy podatki';
$string['proxyauthcredentialsdescription'] = 'Navedite podatke v zapisu <em>uporabniško_ime:geslo</em>, ki so potrebni, da lahko proxy  overi vaš spletni strežnik';
$string['proxyauthcredntialsset'] = 'Proxy podatki za overjanje nastavljeni';
$string['proxyauthmodel'] = 'Proxy model overjanja';
$string['proxyauthmodeldescription'] = 'Izberite proxy model overjanja, če je to primerno';
$string['proxyauthmodelset'] = 'Proxy model overjanja je določen';
$string['proxysettings'] = 'Proxy nastavitve';
$string['public'] = 'javno';
$string['publickey'] = 'Javni ključ';
$string['publickeydescription2'] = 'Javni ključ se ustvari samodejno in zamenja vsakih %s dni';
$string['publickeyexpires'] = 'Javni ključ poteče';
$string['registerthismaharasite'] = 'Registriraj to Mahara spletišče';
$string['registeryourmaharasite'] = 'Registracija Mahara spletišča';
$string['registeryourmaharasitedetail'] = '<p>Lahko se odločite za registracijo vašega Mahara spletišča pri <a href="http://mahara.org/">mahara.org</a>. Registracija je brezplačna in pomaga pri oblikovanju slike o osnovnih namestitvah Mahare po celem svetu.</p>
<p>Lahko si ogledate informacije, ki bodo poslane na mahara.org - poslano ne bo nič, po čemer bi lahko osebno identificirali katerega koli izmed vaših uporabnikov.</p>
<p>Če obkljukate možnost "Pošlji tedenske posodobitve", bo sistem na mahara.org samodejno pošiljal tedenske posodobitve posodobljenih informacij.</p>
<p>Registracija bo odpravila to obvestilo. Na strani <a href="%sadmin/site/options.php">Možnosti spletišča</a> lahko kadarkoli spremenite, ali želite pošiljati tedenske posodobitve.</p>';
$string['registrationallowed'] = 'Dovoljena registracija?';
$string['registrationalloweddescription2'] = 'Ali se lahko uporabniki s pomočjo obrazca registrirajo za uporabo spletišča v okviru te ustanove';
$string['registrationfailedtrylater'] = 'Neuspela registracija s kodo napake %s. Prosimo, poskusite znova pozneje.';
$string['registrationsuccessfulthanksforregistering'] = 'Registracija uspela - hvala, da ste se registrirali!';
$string['reinstall'] = 'Ponovno namesti';
$string['release'] = 'Izdaja %s (%s)';
$string['remoteusername'] = 'Uporabniško ime za zunanje overjanje';
$string['remoteusernamedescription'] = 'Uporabniško ime, ki ga ima uporabnik v oddaljenem sistemu';
$string['removeuserfrominstitution'] = 'Odstrani uporabnika iz ustanove';
$string['removeusers'] = 'Odstrani uporabnike';
$string['removeusersfrominstitution'] = 'Odstrani uporabnike iz ustanove';
$string['reopensite'] = 'Ponovno odpri spletišče';
$string['reopensitedetail'] = 'Vaše spletišče je zaprto. Skrbniki spletišča lahko ostanejo prijavljeni, dokler ne bo zaznana nadgradnja.';
$string['requestto'] = 'Prošnja za';
$string['resetpassword'] = 'Ponastavi geslo';
$string['resetpassworddescription'] = 'Če sem vpišete besedilo, bo to zamenjalo uporabnikovo trenutno geslo.';
$string['resultsof'] = 'rezultatov od';
$string['runupgrade'] = 'Začni nadgradnjo';
$string['savechanges'] = 'Shrani spremembe';
$string['savefailed'] = 'Shranjenvanje neuspešno';
$string['savingmenuitem'] = 'Shranjevanje elementa';
$string['searchplugin'] = 'Vtičnik za iskanje';
$string['searchplugindescription'] = 'Kateri vtičnik bo uporabljen za iskanje';
$string['sendweeklyupdates'] = 'Pošlji tedenske posodobitve?';
$string['sendweeklyupdatesdescription'] = 'Če je obkljukano, bo sistem pošiljal tedenske posodobitve na mahara.org s statističnimi podatki o spletišču';
$string['sessionlifetime'] = 'Življenjska doba seje';
$string['sessionlifetimedescription'] = 'Čas v minutah, po katerem bodo neaktivni prijavljeni uporabniki samodejno odjavljeni iz sistema';
$string['setsiteoptionsfailed'] = 'Neuspešno nastavljanje možnosti %s';
$string['settingsfor'] = 'Nastavitve za:';
$string['showselfsearchsideblock'] = 'Omogoči preiskovanje listovnika';
$string['showselfsearchsideblockdescription'] = 'Prikaži stranski blok "Preišči Moj listovnik" v razdelku Moj listovnik';
$string['showtagssideblock'] = 'Omogoči oblak ključnih besed';
$string['showtagssideblockdescription'] = 'Če je obkljukano, bodo uporabniki v razdelku Moj listovnik videli stranski blok, s seznamom njihovih najpogosteje uporabljenih ključnih besed';
$string['siteaccountsettings'] = 'Nastavitve računa spletišča';
$string['siteadmin'] = 'Skrbnik spletišča';
$string['siteadmins'] = 'Skrbniki spletišča';
$string['sitedefault'] = 'Privzeto za spletišče';
$string['sitefile'] = 'Datoteka spletišča';
$string['sitefiles'] = 'Datoteke spletišča';
$string['sitefilesdescription'] = 'Nalaganje in upravljanje z datotekami, ki so lahko v Meniju povezav in virov in Pogledov spletišča';
$string['sitelanguagedescription'] = 'Privzeti jezik spletišča';
$string['sitename'] = 'Ime spletišča';
$string['sitenamedescription'] = 'Ime spletišča se pojavlja na nekaterih mestih po spletišču in v epoštnih sporočilih, poslanih iz spletišča';
$string['siteoptions'] = 'Možnosti spletišča';
$string['siteoptionsdescription'] = 'Nastavljanje osnovnih možnosti spletišča, npr. ime, jezik in tema';
$string['siteoptionspagedescription'] = 'Tukaj lahko določite nekatere splošne možnosti, ki bodo uporabljane kot privzeta vrednost skozi celotno spletišče.';
$string['siteoptionsset'] = 'Možnosti spletišča posodobljene.';
$string['sitepageloaded'] = 'Stran spletišča naložena';
$string['sitestaff'] = 'Osebje spletišča';
$string['sitethemedescription'] = 'Privzeta tema spletišča';
$string['siteviews'] = 'Pogledi spletišča';
$string['siteviewsdescription'] = 'Ustvarjanje in upravljanje Pogledov in Predlog pogledov za celotno spletišče';
$string['staffusers'] = 'Osebje';
$string['staffusersdescription'] = 'Določanje uporabnikov z dostopom za osebje';
$string['staffuserspagedescription'] = 'Tukaj lahko izberete, kateri uporabniki so osebje spletišča. Trenutno osebje je na desni, možno osebje pa na levi.';
$string['staffusersupdated'] = 'Osebje posodobljeno';
$string['strchoosefile'] = 'Izberite datoteko...';
$string['stringid'] = 'Identifikator';
$string['stringoriginal'] = 'Izvirnik';
$string['stringstatus'] = 'Stanje';
$string['stringtranslated'] = 'Prevod';
$string['strloadfile'] = 'Naloži';
$string['studentid'] = 'ID številka';
$string['successfullyinstalled'] = 'Mahara uspešno nameščena!';
$string['suspenddeleteuser'] = 'Blokiraj/Izbriši uporabnika';
$string['suspenddeleteuserdescription'] = 'Tukaj lahko blokirate ali popolnoma izbrišete uporabniški račun. Blokirani uporabniki se ne morejo prijaviti v sistem, dokler niso ponovno aktivirani. Prosimo, zapomnite si, da lahko blokado računa prekličete, brisanja pa <strong>ne morete</strong> preklicati.';
$string['suspended'] = 'Blokiran';
$string['suspendedby'] = 'Uporabnika je blokiral/a %s';
$string['suspendedinstitution'] = 'BLOKIRANA';
$string['suspendedinstitutionmessage'] = 'Ta ustanova je blokirana';
$string['suspendedreason'] = 'Razlog blokade';
$string['suspendedreasondescription'] = 'Besedilo, ki bo prikazano uporabniku ob njegovem naslednjem poskusu prijave.';
$string['suspendedusers'] = 'Blokirani uporabniki';
$string['suspendedusersdescription'] = 'Blokiranje ali ponovno aktiviranje uporabe spletišča za uporabnike';
$string['suspendingadmin'] = 'Blokiral skrbnik';
$string['suspendinstitution'] = 'Blokiraj ustanovo';
$string['suspendinstitutiondescription'] = 'Tukaj lahko blokirate ustanovo. Uporabniki blokiranih ustanov se ne bodo mogli prijaviti, dokler ustanove ne bodo ponovno aktivirane.';
$string['suspenduser'] = 'Blokiraj uporabnika';
$string['suspensionreason'] = 'Razlog blokade';
$string['tagssideblockmaxtags'] = 'Največ ključnih besed v oblaku';
$string['tagssideblockmaxtagsdescription'] = 'Privzeto število ključnih besed, za prikaz v uporabniškem oblaku ključnih besed';
$string['termsandconditions'] = 'Pravila in pogoji';
$string['thefollowingupgradesareready'] = 'Pripravljene so naslednje nadgradnje:';
$string['theme'] = 'Tema';
$string['thisuserissuspended'] = 'Uporabnik je blokiran';
$string['toversion'] = 'V različico';
$string['trustedsites'] = 'Zaupanja vredna spletišča';
$string['type'] = 'Tip';
$string['unsuspendinstitution'] = 'Aktiviraj ustanovo';
$string['unsuspendinstitutiondescription'] = 'Tukaj lahko ponovno aktivirate ustanovo. Uporabniki blokiranih ustanov se ne bodo mogli prijaviti, dokler ustanove ne bodo ponovno aktivirane.<br /><strong>Pozor:</strong> Ponovno aktiviranje ustanove, ne da bi ponovno nastavili ali izključili datum njenega izteka, lahko povzoči ponavljajočo vsakodnevno blokado.';
$string['unsuspendinstitutiondescription_top'] = '<em>Pozor:</em> Ponovno aktiviranje ustanove, ne da bi ponastavili ali izključili datum poteka, lahko povzroči vsakodnevno blokiranje ustanove.';
$string['unsuspenduser'] = 'Aktiviraj uporabnika';
$string['unsuspendusers'] = 'Aktiviraj uporabnike';
$string['updatesiteoptions'] = 'Posodobi možnosti spletišča';
$string['upgradefailure'] = 'Ne morem nadgraditi!';
$string['upgradeloading'] = 'Nalaganje...';
$string['upgrades'] = 'Nadgradnje';
$string['upgradesuccess'] = 'Uspešno nadgrajeno';
$string['upgradesuccesstoversion'] = 'Uspešno nadgrajeno v različico';
$string['uploadcopyright'] = 'Avtorska izjava pri nalaganju';
$string['uploadcsv'] = 'Dodaj uporabnike s CSV';
$string['uploadcsvdescription'] = 'Nalaganje CSV datoteke, ki vsebuje nove uporabnike';
$string['uploadcsverroremailaddresstaken'] = 'Vrstica %s CSV datoteke vsebuje epoštni naslov "%s", ki ga je že uporabil drug uporabnik';
$string['uploadcsverrorincorrectnumberoffields'] = 'Napaka CSV datoteke v vrstici %s: Vrstica nima pravilnega števila polj';
$string['uploadcsverrorinvalidemail'] = 'Napaka CSV datoteke v vrstici %s: Epoštni naslov tega uporabnika ni pravilne oblike';
$string['uploadcsverrorinvalidfieldname'] = 'Ime polja "%s" je neveljavno';
$string['uploadcsverrorinvalidpassword'] = 'Napaka CSV datoteke v vrstici %s: Geslo tega uporabnika ni pravilne oblike';
$string['uploadcsverrorinvalidusername'] = 'Napaka CSV datoteke v vrstici %s: Uporabniško ime tega uporabnika ni pravilne oblike';
$string['uploadcsverrormandatoryfieldnotspecified'] = 'Vrstica %s CSV datoteke nima zahtevanega polja "%s"';
$string['uploadcsverrornorecords'] = 'Zdi se, da datoteka ne vsebujejo nobenih zapisov (čeprav je glava v redu)';
$string['uploadcsverrorrequiredfieldnotspecified'] = 'Zahtevano polje "%s" ni bilo določeno v vrstici, ki vsebuje obliko';
$string['uploadcsverrorunspecifiedproblem'] = 'Zapisov iz CSV datoteke zaradi neznanega razloga ni mogoče vstaviti. Če je CVS datoteka pravilne oblike, potem je to hrošč, zato vas prosimo, da <a href="https://eduforge.org/tracker/?func=add&group_id=176&atid=739"> ustvarite poročilo o hrošču</a>, ki mu priložite CSV datoteko (ne pozabite izbrisati gesel!) in, če je mogoče, datoteko z dnevnikom napak.';
$string['uploadcsverroruseralreadyexists'] = 'Vrstica %s CSV datoteke vsebuje uporabniško ime "%s", ki že obstaja';
$string['uploadcsvfailedusersexceedmaxallowed'] = 'Noben uporabnik ni dodan, saj CSV datoteka vsebuje preveč uporabnikov. Število uporabnikov v ustanovi bi tako preseglo največje dovoljeno število.';
$string['uploadcsvinstitution'] = 'Ustanova in način overjanja novih uporabnikov';
$string['uploadcsvpagedescription2'] = '<p>To možnost lahko uporabite, če želite naložiti nove uporabnike s pomočjo <acronym title="Comma separated Values">CSV</acronym> datoteke.</p>
   
<p>Prva vrstica CSV datoteke mora določiti obliko CSV podatkov. Izgledala naj bi na primer takole:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>Ta vrstica mora vsebovati polja <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> in <tt>lastname</tt>. Vsebovati mora tudi polja, ki jih morajo obvezno izpolniti vsi uporabniki ter vsa polja, ki so zaklenjena za ustanovo, kateri nalagate uporabnike. Lahko <a href="%s">nastavite obvezna polja</a> za vse ustanove, ali <a href="%s">nastavite zaklenjena polja za vsako ustanovo</a>.</p>

<p>CSV datoteka lahko vsebuje katera koli druga polja profila, ki jih potrebujete. Celoten seznam polj:</p>

% s';
$string['uploadcsvpagedescription2institutionaladmin'] = '<p>To možnost lahko uporabite, če želite naložiti nove uporabnike s pomočjo <acronym title="Comma separated Values">CSV</acronym> datoteke.</p>
   
<p>Prva vrstica CSV datoteke mora določiti obliko CSV podatkov. Izgledala naj bi na primer takole:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>Ta vrstica mora vsebovati polja <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> in <tt>lastname</tt>. Vsebovati mora tudi vsa polja, ki jih je skrbni spletišča določil kot obvezna ter vsa polja, ki so zaklenjena za ustanovo. Lahko <a href="%s">nastavite zaklenjena polja</a> za ustanovo, katere skrbnik ste.</p>

<p>CSV datoteka lahko vsebuje katera koli druga polja profila, ki jih potrebujete. Celoten seznam polj:</p>

% s';
$string['uploadcsvsomeuserscouldnotbeemailed'] = 'Nekaterim uporabnikom ni bilo mogoče poslati epošte. Njihovi epoštni naslovi so morda neveljavni, ali pa strežnik, na katerem se izvaja Mahara, ni pravilno nastavljen za pošiljanje epošte. Dnevnik napak strežnika vsebuje več podrobnosti. Za zdaj, boste morali epošto poslati ročno naslednjim uporabnikom:';
$string['uploadcsvusersaddedsuccessfully'] = 'Uporabniki v CSV datoteki so uspešno dodani';
$string['uploadleap2afile'] = 'Naloži LEAP2A datoteko';
$string['useradded'] = 'Uporabnik dodan';
$string['usercreationmethod'] = '1 - Način ustvarjanja uporabnika';
$string['userdeletedsuccessfully'] = 'Uporabnik uspešno izbrisan';
$string['usereditdescription'] = 'Tukaj si lahko ogledate in nastavite podrobnosti uporabniškega računa. Spodaj lahko tudi <a href="#suspend">blokirate ali izbrišete ta račun</a>, ali spremenite nastavitve za tega uporabnika glede na <a href="#institutions">ustanovo, katere član je</a>.';
$string['usersallowedmultipleinstitutions'] = 'Uporabniki v večih ustanovah';
$string['usersallowedmultipleinstitutionsdescription'] = 'Če je obkljukano, so lahko uporabniki hkrati člani večih različnih ustanov';
$string['usersdeletedsuccessfully'] = 'Uporabniki uspešno izbrisani';
$string['usersearch'] = 'Iskanje uporabnikov';
$string['usersearchdescription'] = 'Iskanje vseh uprabnikov in opravljanje skrbniških dejanj nad njimi';
$string['usersearchinstructions'] = 'Uporabnike lahko iščete tako, da kliknete inicialko njihovega imena ali priimka, ali pa vpišete njihovo ime v iskalno polje. V iskalno polje lahko vpišete tudi epoštni naslov, če želite preiskovati po epoštnih naslovih.';
$string['usersrequested'] = 'Uporabniki, ki so zaprosili za članstvo';
$string['usersseenewthemeonlogin'] = 'Uporabniki bodo videli novo temo naslednjič, ko se bodo prijavili.';
$string['userstoaddorreject'] = 'Uporabniki, ki bodo dodani/zavrnjeni';
$string['userstobeadded'] = 'Uporabniki, ki bodo dodani kot člani';
$string['userstobeinvited'] = 'Uporabniki, ki jih je treba povabiti';
$string['userstoberemoved'] = 'Uporabniki, ki jih je treba odstraniti';
$string['userstodisplay'] = 'Prikaži uporabnike:';
$string['usersunsuspendedsuccessfully'] = 'Uporabniki uspešno znova aktivirani';
$string['usersuspended'] = 'Uporabnik blokiran';
$string['userunsuspended'] = 'Uporabnik znova aktiviran';
$string['userwillreceiveemailandhastochangepassword'] = 'Prejel/a bo epošto z obvestilom o podrobnostih njegovega/njenega računa. Ob prvi prijavi bo prisiljen/a spremeniti svoje geslo.';
$string['viruschecking'] = 'Iskanje virusov';
$string['viruscheckingdescription'] = 'Če je obkljukano, bo vklopljeno preverjanje vseh naloženih datotek s pomočjo protivirusnega programa ClamAV';
$string['whocancreategroups'] = 'Kdo lahko ustvari skupine';
$string['whocancreategroupsdescription'] = 'Kateri uporabniki lahko ustvarijo nove skupine';
$string['whocancreatepublicgroups'] = 'Kdo lahko ustvari javne skupine';
$string['whocancreatepublicgroupsdescription'] = 'Kateri uporabniki lahko ustvarijo skupine, ki so javno vidne';
$string['wwwroot'] = 'WWW korenska mapa';
$string['wwwrootdescription'] = 'To je URL, na katerem lahko uporabniki dostopajo do Mahara namestitve in URL za katerega so ustvarjeni SSL ključi';
$string['youcannotadministerthisuser'] = 'Ne morete upravljati tega uporabnika';
$string['youcanupgrade'] = 'Maharo lahko nadgradite iz %s (%s) v %s (%s)!';
?>
