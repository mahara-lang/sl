<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['About'] = 'O skupini';
$string['Admin'] = 'Skrbniki';
$string['Created'] = 'Ustvarjeno';
$string['Files'] = 'Datoteke';
$string['Friends'] = 'Prijateljev';
$string['Group'] = 'Skupina';
$string['Joined'] = 'Pridružen/a';
$string['Members'] = 'Člani';
$string['Reply'] = 'Odgovori';
$string['Role'] = 'Vloga';
$string['Views'] = 'Pogledi';
$string['acceptinvitegroup'] = 'Sprejmi';
$string['addedtofriendslistmessage'] = '%s vas je dodal/a kot prijatelja! To pomeni, da je sedaj tudi %s na vašem seznamu prijateljev.  Kliknite na spodnjo povezavo, za ogled strani profila';
$string['addedtofriendslistsubject'] = 'Nov prijatelj';
$string['addedtogroupmessage'] = '%s vas je dodal/a v skupino, \'%s\'.  Za ogled skupine, kliknite spodnjo povezavo';
$string['addedtogroupsubject'] = 'Dodani ste v skupino';
$string['addnewinteraction'] = 'Dodaj nov/novo %s';
$string['addtofriendslist'] = 'Dodaj k prijateljem';
$string['addtomyfriends'] = 'Dodaj k mojim prijateljem!';
$string['adduserfailed'] = 'Dodajanje uporabnika neuspešno';
$string['addusertogroup'] = 'Dodaj v';
$string['allfriends'] = 'Vsi prijatelji';
$string['allgroupmembers'] = 'Vsi člani skupine';
$string['allgroups'] = 'Vse skupine';
$string['allmygroups'] = 'Vse moje skupine';
$string['approve'] = 'Odobri';
$string['approverequest'] = 'Odobri prošnjo!';
$string['backtofriendslist'] = 'Nazaj na seznam prijateljev';
$string['cannotinvitetogroup'] = 'Uporabnika ne morete povabiti v skupino';
$string['cannotrequestfriendshipwithself'] = 'Ne morete zaprositi za prijateljstvo s samim seboj';
$string['cannotrequestjoingroup'] = 'Ne morete zaprositi za članstvo v skupini';
$string['cantdeletegroup'] = 'Ne morete izbrisati skupine';
$string['cantdenyrequest'] = 'Prošnja za prijateljstvo ni veljavna';
$string['canteditdontown'] = 'Ne morete urejati skupine, ker niste njen lastnik';
$string['cantleavegroup'] = 'Skupine ne morete zapustiti';
$string['cantmessageuser'] = 'Uporabniku ne morete poslati sporočila';
$string['cantremovefriend'] = 'Uporabika ne morete odstraniti iz seznama prijateljev';
$string['cantrequestfriendship'] = 'Tega uporabnika ne morete zaprositi za prijateljstvo';
$string['cantrequestfrienship'] = 'Uporabnika ne morete zaprositi za prijateljstvo';
$string['cantviewmessage'] = 'Tega sporočila ne morete videti';
$string['changerole'] = 'Spremeni vlogo';
$string['changeroleofuseringroup'] = 'Spremeni vlogo iz %s v %s';
$string['changeroleto'] = 'Spremeni vlogo v';
$string['confirmremovefriend'] = 'Ali zares želite odstraniti uporabnika iz seznama prijateljev?';
$string['couldnotjoingroup'] = 'Skupini se ne morete pridružiti';
$string['couldnotleavegroup'] = 'Skupine ne morete zapustiti';
$string['couldnotrequestgroup'] = 'Ne morem poslati prošnje za članstvo v skupini';
$string['creategroup'] = 'Ustvari skupino';
$string['currentfriends'] = 'Trenutni prijatelji';
$string['currentrole'] = 'Trenutna vloga';
$string['declineinvitegroup'] = 'Zavrni';
$string['declinerequest'] = 'Zavrni prošnjo';
$string['deletegroup'] = 'Skupina uspešno izbrisana';
$string['deleteinteraction'] = 'Izbriši %s \'%s\'';
$string['deleteinteractionsure'] = 'Ali zares želite narediti to? Brisanja ne bo mogoče razveljaviti.';
$string['deletespecifiedgroup'] = 'Izbriši skupino \'%s\'';
$string['denyfriendrequest'] = 'Zavrni prošnjo za prijateljstvo';
$string['denyfriendrequestlower'] = 'Zavrni prošnjo za prijateljstvo';
$string['denyrequest'] = 'Zavrni prošnjo';
$string['editgroup'] = 'Uredi skupino';
$string['existingfriend'] = 'obstoječi prijatelj';
$string['findnewfriends'] = 'Najdi nove prijatelje';
$string['friend'] = 'prijatelj';
$string['friendformacceptsuccess'] = 'Prošnja za prijateljstvo sprejeta';
$string['friendformaddsuccess'] = '%s dodan/a na seznam prijateljev';
$string['friendformrejectsuccess'] = 'Prošnja za prijateljstvo zavrnjena';
$string['friendformremovesuccess'] = '%s odstranjen/a iz seznama prijateljev';
$string['friendformrequestsuccess'] = 'Prošnja za prjateljstvo poslana osebi %s';
$string['friendlistfailure'] = 'Neuspešno spreminjanje seznama prijateljev';
$string['friendrequestacceptedmessage'] = '%s je sprejel/a vašo prošnjo za prijateljstvo ter je dodan/a na vaš seznam prijateljev.';
$string['friendrequestacceptedsubject'] = 'Prošnja za prijateljstvo sprejeta';
$string['friendrequestrejectedmessage'] = '%s je zavrnil/a vašo prošnjo za prijateljstvo.';
$string['friendrequestrejectedmessagereason'] = '%s je zavrnil/a vašo prošnjo za prijateljstvo. Razlog:';
$string['friendrequestrejectedsubject'] = 'Prošnja za prijateljstvo zavrnjena';
$string['friends'] = 'prijateljev';
$string['friendshipalreadyrequested'] = 'Zaprosili ste, da vas %s doda na seznam prijateljev';
$string['friendshipalreadyrequestedowner'] = '%s je zaprosil/a, da ga/jo dodate na seznam prijateljev';
$string['friendshiprequested'] = 'Zaprosil/a za prijateljstvo!';
$string['group'] = 'skupina';
$string['groupadmins'] = 'Skrbniki skupine';
$string['groupalreadyexists'] = 'Skupina s tem imenom že obstaja';
$string['groupconfirmdelete'] = 'Ali zares želite izbrisati to skupino?';
$string['groupconfirmdeletehasviews'] = 'Ali zares želite izbrisati skupino? Nekateri izmed vaših pogledov uporabljajo to skupino za nadzor dostopa. Odstranitev te skupine bi pomenila, da člani skupine ne bi več imeli dostopa do teh pogledov.';
$string['groupconfirmleave'] = 'Ali zares želite zapustiti skupino?';
$string['groupconfirmleavehasviews'] = 'Ali zares želite zapustiti skupino? Nekateri izmed vaših pogledov uporabljajo to skupino za nadzor dostopa. Zaputitev te skupine bi pomenila, da člani skupine ne bi več imeli dostopa do teh pogledov.';
$string['groupdescription'] = 'Opis skupine';
$string['grouphaveinvite'] = 'Povabbljeni ste, da se pridružite skupini';
$string['grouphaveinvitewithrole'] = 'Povabljeni ste, da se pridružite skupini z vlogo';
$string['groupinteractions'] = 'Dejavnosti skupine';
$string['groupinviteaccepted'] = 'Povabilo uspešno sprejeto! Postali ste član skupine';
$string['groupinvitedeclined'] = 'Povabilo uspešno zavrnjeno!';
$string['groupinvitesfrom'] = 'Vabljeni, da se pridružite:';
$string['groupjointypecontrolled'] = 'Članstvo v tej skupini je nadzorovano. Skupini se ne morete pridružiti.';
$string['groupjointypeinvite'] = 'Članstvo v tej skupini je samo s povabilom.';
$string['groupjointypeopen'] = 'Članstvo v tej skupini je odprto. Lahko se pridružite!';
$string['groupjointyperequest'] = 'Članstvo v tej skupini je samo s prošnjo.';
$string['groupmemberrequests'] = 'Čakajoče prošnje za članstvo';
$string['groupmembershipchangedmessageaddedmember'] = 'Dodani ste kot član te skupine';
$string['groupmembershipchangedmessageaddedtutor'] = 'Dodani ste kot mentor te skupine';
$string['groupmembershipchangedmessagedeclinerequest'] = 'Prošnja za članstvo v tej skupini je zavrnjena';
$string['groupmembershipchangedmessagemember'] = 'Nazadovali ste iz mentorja te skupine';
$string['groupmembershipchangedmessageremove'] = 'Odstranili so vas iz skupine';
$string['groupmembershipchangedmessagetutor'] = 'Napredovali ste v mentorja te skupine';
$string['groupmembershipchangesubject'] = 'Članstvo skupine: %s';
$string['groupname'] = 'Ime skupine';
$string['groupnotfound'] = 'Ne najdem skupine z id številko %s';
$string['groupnotinvited'] = 'Niste povabljeni, da se pridružite skupini';
$string['grouprequestmessage'] = '%s bi se rad/a pridružil/a vaši skupini %s';
$string['grouprequestmessagereason'] = '%s bi se rad/a pridružil/a vaši skupini %s. Razlog, zaradi katerega se želi pridružiti:

%s';
$string['grouprequestsent'] = 'Prošnja za članstvo v skupini poslana';
$string['grouprequestsubject'] = 'Nova prošnja za članstvo v skupini';
$string['groups'] = 'skupine';
$string['groupsaved'] = 'Skupina uspešno shranjena';
$string['groupsimin'] = 'Skupine, katerih član sem';
$string['groupsiminvitedto'] = 'Skupine, v katere sem povabljen';
$string['groupsiown'] = 'Skupine, katerih lastnik sem';
$string['groupsiwanttojoin'] = 'Skupine, ki se jim želim pridružiti';
$string['groupsnotin'] = 'Skupine, katerih član nisem';
$string['grouptype'] = 'Vrsta skupine';
$string['hasbeeninvitedtojoin'] = 'je povabljen/a, da se pridruži tej skupini';
$string['hasrequestedmembership'] = 'je zaprosil/a za članstvo v tej skupini';
$string['instructions:controlled'] = 'To je skupina z nadzorovanim članstvom. Uporabnike dodajte preko njihove strani s profilom.';
$string['instructions:invite'] = 'To je skupina samo povabljenih. Uporabnike povabite preko njihove strani s profilom.';
$string['interactiondeleted'] = '%s uspešno izbrisano';
$string['interactionsaved'] = '%s uspešno shranjeno';
$string['invalidgroup'] = 'Skupina ne obstaja';
$string['invite'] = 'Povabi';
$string['invitemembertogroup'] = 'Povabite %s naj se pridruži \'%s\'';
$string['invitetogroupmessage'] = '%s vas je povabil/a, da se pridružite skupini \'%s\'. Za več informacij kliknite na spodnjo povezavo.';
$string['invitetogroupsubject'] = 'Povabljeni ste, da se pridružite skupini';
$string['inviteuserfailed'] = 'Povabilo uporabnika neuspešno';
$string['inviteusertojoingroup'] = 'Povabi v';
$string['joinedgroup'] = 'Postali ste član skupine';
$string['joingroup'] = 'Pridruži se skupini';
$string['leavegroup'] = 'Zapusti skupino';
$string['leavespecifiedgroup'] = 'Zapusti skupino \'%s\'';
$string['leftgroup'] = 'Zapustili ste skupino';
$string['leftgroupfailed'] = 'Zapuščanje skupine neuspešno';
$string['member'] = 'član';
$string['memberchangefailed'] = 'Posodobitev nekaterih podatkov o članstvu neuspešna';
$string['memberchangesuccess'] = 'Stanje članstva uspešno spremenjeno';
$string['memberrequests'] = 'Prošnje za članstvo';
$string['members'] = 'člani';
$string['membershiprequests'] = 'Prošnje za članstvo';
$string['membershiptype'] = 'Vrsta članstva v skupini';
$string['membershiptype.controlled'] = 'Nadzorovano članstvo';
$string['membershiptype.invite'] = 'Samo povabljeni';
$string['membershiptype.open'] = 'Odprto članstvo';
$string['membershiptype.request'] = 'Prošnja za članstvo';
$string['memberslist'] = 'Člani:';
$string['messagebody'] = 'Vsebina sporočila';
$string['messagenotsent'] = 'Neuspešno pošiljanje sporočila';
$string['messagesent'] = 'Sporočilo poslano!';
$string['newusermessage'] = 'Novo sporočilo od %s';
$string['newusermessageemailbody'] = '%s vam je poslal/a novo sporočilo. Za ogled sporočila, obiščite

%s';
$string['nobodyawaitsfriendapproval'] = 'Nihče ne čaka na vašo odobritev prijateljstva';
$string['nogroups'] = 'Ni skupin';
$string['nogroupsfound'] = 'Ne najdem skupin :(';
$string['nointeractions'] = 'V tej skupini ni dejavnosti';
$string['nosearchresultsfound'] = 'Ne najdem rezultatov iskanja :(';
$string['notallowedtodeleteinteractions'] = 'Nimate dovoljenj za brisanje dejavnosti v tej skupini';
$string['notallowedtoeditinteractions'] = 'Nimate dovoljenj za dodajanje ali urejanje dejavnosti v tej skupini';
$string['notamember'] = 'Niste član te skupine';
$string['notinanygroups'] = 'V nobeni skupini';
$string['notmembermayjoin'] = 'Če si želite ogledati stran, se morate pridružiti skupini \'%s\'.';
$string['noviewstosee'] = 'Nobenega pogleda, ki bi ga lahko videl :(';
$string['pending'] = 'čakajoče/i';
$string['pendingfriends'] = 'Čakajoči prijatelji';
$string['pendingmembers'] = 'Čakajoči člani';
$string['publiclyviewablegroup'] = 'Javno vidna skupina?';
$string['publiclyviewablegroupdescription'] = 'Dovoli komurkoli (tudi ljudem, ki niso člani spletišča), da vidi to skupino, vključno z njenimi forumi?';
$string['reason'] = 'Razlog';
$string['reasonoptional'] = 'Razlog (neobvezno)';
$string['reject'] = 'Zavrni';
$string['rejectfriendshipreason'] = 'Razlog za zavrnitev prošnje';
$string['releaseview'] = 'Sprosti pogled';
$string['remove'] = 'Odstrani';
$string['removedfromfriendslistmessage'] = '%s vas je odstranil/a iz svojega seznama prijateljev.';
$string['removedfromfriendslistmessagereason'] = '%s vas je odstranil/a iz svojega seznama prijateljev. Razlog:';
$string['removedfromfriendslistsubject'] = 'Odstranjen/a iz seznama prijateljev';
$string['removefriend'] = 'Odstrani prijatelja';
$string['removefromfriends'] = 'Odstrani %s iz prijateljev';
$string['removefromfriendslist'] = 'Odstrani iz prijateljev';
$string['removefromgroup'] = 'Odstrani iz skupine';
$string['request'] = 'Prošnja';
$string['requestedfriendlistmessage'] = '%s je zaprosil/a, da ga/jo dodate kot prijatelja. To lahko naredite s spodnjo povezavo, ali na strani s seznamom prijateljev.';
$string['requestedfriendlistmessagereason'] = '%s je zaprosil/a, da ga/jo dodate kot prijatelja. To lahko naredite s spodnjo povezavo, ali na strani s seznamom prijateljev. Razlog:';
$string['requestedfriendlistsubject'] = 'Nova prošnja za prijateljstvo';
$string['requestedfriendship'] = 'prošnja za prijateljstvo';
$string['requestedmembershipin'] = 'Prosili ste za članstvo v:';
$string['requestedtojoin'] = 'Zaprosili ste za članstvo v skupini';
$string['requestfriendship'] = 'Zaprosi za prijateljstvo';
$string['requestjoingroup'] = 'Prošnja za članstvo v skupini';
$string['requestjoinspecifiedgroup'] = 'Prošnja za pridružitev skupini \'%s\'';
$string['rolechanged'] = 'Vloga spremenjena';
$string['savegroup'] = 'Shrani skupino';
$string['seeallviews'] = 'Prikaži vse poglede osebe %s...';
$string['sendfriendrequest'] = 'Pošlji prošnjo za prijateljstvo!';
$string['sendfriendshiprequest'] = 'Zaprosi %s za prijateljstvo';
$string['sendinvitation'] = 'Pošlji povabilo';
$string['sendmessage'] = 'Pošlji sporočilo';
$string['sendmessageto'] = 'Pošlji sporočilo %s';
$string['submittedviews'] = 'Oddani pogledi';
$string['therearependingrequests'] = 'Za to skupino obstaja %s čakajočih prošenj za članstvo';
$string['thereispendingrequest'] = 'Za to skupino obstaja 1 čakajoča prošnja za članstvo';
$string['title'] = 'Naslov';
$string['trysearchingforfriends'] = 'Poizkusite %spoiskati nove prijatelje%s!';
$string['trysearchingforgroups'] = 'Poizkusite %spoiskati skupine%s, v katere bi se včlanili!';
$string['updatemembership'] = 'Posodobi članstvo';
$string['user'] = 'uporabnik';
$string['useradded'] = 'Uporabnik dodan';
$string['useralreadyinvitedtogroup'] = 'Uporabnik je že bil povabljen, ali pa je že član skupine.';
$string['usercannotchangetothisrole'] = 'Uporabnik ne more zamenjati svoje vloge';
$string['usercantleavegroup'] = 'Uporabnik ne more zapustiti skupine';
$string['userdoesntwantfriends'] = 'Uporabnik ne želi novih prijateljev';
$string['userinvited'] = 'Povabilo poslano';
$string['userremoved'] = 'Uporabnik odstranjen';
$string['users'] = 'uporabnikov';
$string['usersautoadded'] = 'Uporabniki samodejno dodani?';
$string['usersautoaddeddescription'] = 'Samodejno dodaj vse nove uporabnike v to skupino?';
$string['viewmessage'] = 'Ogled sporočila';
$string['viewreleasedmessage'] = 'Pogled, ki ste ga oddali skupini %s za ocenitev, je v ponovno uporabo sprostil/a %s';
$string['viewreleasedsubject'] = 'Pogled je sproščen';
$string['viewreleasedsuccess'] = 'Pogled je uspešno sproščen';
$string['whymakemeyourfriend'] = 'Za prijatelja naj bi me sprejel zaradi:';
$string['youaregroupmember'] = 'Ste član skupine';
$string['youowngroup'] = 'Ste lastnik skupine';
?>
