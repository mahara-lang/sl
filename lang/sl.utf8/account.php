<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['accountdeleted'] = 'Račun je bil izbrisan.';
$string['accountoptionsdesc'] = 'Splošne možnosti računa';
$string['changepassworddesc'] = 'Spremeni geslo';
$string['changepasswordotherinterface'] = 'Svoje geslo lahko  <a href="%s">spremenite</a> preko drugega vmesnika';
$string['changeusername'] = 'Novo uporabniško ime';
$string['changeusernamedesc'] = 'Uporabniško ime s katerim se prijavite v sistem %s. Uporabniška imena so dolga 3-30 znakov in lahko vsebujejo črke, številke ter večino običajnih simbolov, razen presledka.';
$string['changeusernameheading'] = 'Spremeni uporabniško ime';
$string['deleteaccount'] = 'Izbriši račun';
$string['deleteaccountdescription'] = 'Če izbrišete vaš račun, potem drugi uporabniki ne bodo več mogli videti vašega profila in vaših pogledov. Vsebina vaših objav v forumih bo še vedno vidna, vendar ime avtorja ne bo več izpisano.';
$string['friendsauth'] = 'Novi prijatelji potrebujejo mojo odobritev';
$string['friendsauto'] = 'Novi prijatelji so samodejno odobreni';
$string['friendsdescr'] = 'Nadzor prijateljev';
$string['friendsnobody'] = 'Nihče me ne sme dodati kot prijatelja';
$string['language'] = 'Jezik';
$string['messagesallow'] = 'Kdorkoli mi lahko pošlje sporočilo';
$string['messagesdescr'] = 'Sporočila drugih uporabnikov';
$string['messagesfriends'] = 'Dovoli prijateljem, da mi pošiljajo sporočila';
$string['messagesnobody'] = 'Nikomur ne dovoli, da bi mi poslal sporočilo';
$string['off'] = 'Izklopljeno';
$string['oldpasswordincorrect'] = 'To ni vaše trenutno geslo';
$string['on'] = 'Vklopljeno';
$string['prefsnotsaved'] = 'Vaših nastavitev ne morem shraniti!';
$string['prefssaved'] = 'Nastavitve shranjene';
$string['showviewcolumns'] = 'Prikaži gumbe za dodajanje ali brisanje stolpcev pri urejanju pogleda';
$string['tagssideblockmaxtags'] = 'Največ ključnih besed v oblaku';
$string['tagssideblockmaxtagsdescription'] = 'Največje število ključnih besed, prikazanih v oblaku ključnih besed';
$string['updatedfriendcontrolsetting'] = 'Posodobljen nadzor prijateljev';
$string['wysiwygdescr'] = 'HTML urejevalnik';
?>
