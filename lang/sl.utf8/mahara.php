<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['All'] = 'Vse';
$string['Artefact'] = 'Izdelek';
$string['Artefacts'] = 'Izdelki';
$string['Close'] = 'Zapri';
$string['Copyof'] = 'Kopija %s';
$string['Failed'] = 'Neuspešno';
$string['From'] = 'Od';
$string['Help'] = 'Pomoč';
$string['Invitations'] = 'Povabila';
$string['Memberships'] = 'Članstva';
$string['Permissions'] = 'Dovoljenja';
$string['Query'] = 'Poizvedba';
$string['Requests'] = 'Prošnje';
$string['Results'] = 'Rezultati';
$string['Site'] = 'Spletišče';
$string['Tag'] = 'Ključna beseda';
$string['To'] = 'Do';
$string['about'] = 'O Mahari';
$string['accept'] = 'Sprejmi';
$string['accessforbiddentoadminsection'] = 'Dostop do skrbniškega dela vam ni dovoljen';
$string['accesstotallydenied_institutionsuspended'] = 'Vaša ustanova %s, je bila blokirana. Dokler ne bo ponovno aktivirana, se v %s ne boste mogli prijaviti. Prosimo, da se za pomoč obrnete na ustanovo.';
$string['account'] = 'Moj račun';
$string['accountcreated'] = 'Mahara: Nov račun';
$string['accountcreatedchangepasswordhtml'] = '<p>Spoštovani %s</p>

<p>Za vas je bil ustvarjen nov račun na <a href="http://localhost/mahara120lang/">%s</a>. Podrobnosti računa:</p>

<ul>
    <li><strong>Uporabniško ime:</strong> %s</li>
    <li><strong>Geslo:</strong> %s</li>
</ul>

<p>Ko se boste prvič prijavili, boste morali spremeniti geslo.</p>

<p>Obiščite <a href="http://localhost/mahara120lang/">http://localhost/mahara120lang/</a> in začnite!</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['accountcreatedchangepasswordtext'] = 'Spoštovani %s,

Za vas je bil ustvarjen nov račun na %s. Podrobnosti računa:

Uporabniško ime: %s
Geslo: %s

Ko se boste prvič prijavili, boste morali spremeniti geslo.

Obiščite http://localhost/mahara120lang/ in začnite!

S spoštovanjem, %s skrbnik spletišča';
$string['accountcreatedhtml'] = '<p>Spoštovani %s</p>

<p>Za vas je bil ustvarjen nov račun na <a href="http://localhost/mahara120lang/">%s</a>. Podrobnosti računa:</p>

<ul>
    <li><strong>Uporabniško ime:</strong> %s</li>
    <li><strong>Geslo:</strong> %s</li>
</ul>

<p>Obiščite <a href="http://localhost/mahara120lang/">http://localhost/mahara120lang/</a> in začnite!</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['accountcreatedtext'] = 'Spoštovani %s,

Za vas je bil ustvarjen nov račun na %s. Podrobnosti računa:

Uporabniško ime: %s
Geslo: %s

Obiščite http://localhost/mahara120lang/ in začnite!

S spoštovanjem, %s skrbnik spletišča';
$string['accountdeleted'] = 'Oprostite, vaš račun je bil izbrisan';
$string['accountexpired'] = 'Oprostite, vaš račun je potekel';
$string['accountexpirywarning'] = 'Opozorilo: Račun bo potekel';
$string['accountexpirywarninghtml'] = '<p>Spoštovani %s,</p>
    
<p>Vaš račun na %s bo potekel v %s.</p>

<p>Priporočamo vam, da si s pomočjo orodja Export shranite vsebino listovnika oz. portfelja. Navodila za uporabo tega orodja lahko najdete v uporabniškem priročniku.</p>

<p>Če želite podaljšati vaš račun, ali imate kakšno drugo vprašanje, se <a href="%s">obrnite na nas</a>.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['accountexpirywarningtext'] = 'Spoštovani %s,

Vaš račun na %s bo potekel v %s.

Priporočamo vam, da si s pomočjo orodja Export shranite vsebino listovnika oz. portfelja. Navodila za uporabo tega orodja lahko najdete v uporabniškem priročniku.

Če želite podaljšati vaš račun, ali imate kakšno drugo vprašanje, se obrnite na:

%s

S spoštovanjem, %s skrbnik spletišča';
$string['accountinactive'] = 'Oprostite, vaš račun trenutno ni aktiven';
$string['accountinactivewarning'] = 'Opozorilo: Neaktivnost računa';
$string['accountinactivewarninghtml'] = '<p>Spoštovani %s,</p>

<p>Vaš račun na %s bo postal neaktiven v %s.</p>

<p>Ko bo račun neaktivne, se ne boste mogli prijaviti v sistem, dokler ne bo skrbnik ponovno omogočil vašega računa.</p>

<p>Nekativnost računa lahko preprečite tako, da se prijavite.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['accountinactivewarningtext'] = 'Spoštovani %s,

Vaš račun na %s bo postal neaktiven v %s.

Ko bo račun neaktiven, se ne boste mogli prijaviti v sistem, dokler ne bo skrbnik ponovno omogočil vašega računa.

Nekativnost računa lahko preprečite tako, da se prijavite.

S spoštovanjem, %s skrbnik spletišča';
$string['accountprefs'] = 'Nastavitve';
$string['accountsuspended'] = 'Račun je blokiran od %s. Razlog blokade je: <blockquote>%s</blockquote>';
$string['activityprefs'] = 'Nastavitve dejavnosti';
$string['add'] = 'Dodaj';
$string['addemail'] = 'Dodaj epoštni naslov';
$string['adminphpuploaderror'] = 'Napako pri nalaganju datoteke je verjetno povzročila nastavitev vašega strežnika.';
$string['allowpublicaccess'] = 'Dovoli javni (nepooblaščeni) dostop';
$string['alltags'] = 'Vse ključne besede';
$string['allusers'] = 'Vsi uporabniki';
$string['alphabet'] = 'A,B,C,Č,D,E,F,G,H,I,J,K,L,M,N,O,P,R,S,Š,T,U,V,Z,Ž';
$string['artefact'] = 'izdelek';
$string['artefactnotfound'] = 'Ne najdem izdelka, ki ima id %s';
$string['artefactnotpublishable'] = 'Izdelek %s ne more biti objavljen v pogledu %s';
$string['artefactnotrendered'] = 'Izdelek ni prikazan';
$string['at'] = 'pri';
$string['back'] = 'Nazaj';
$string['backto'] = 'Nazaj na %s';
$string['belongingto'] = 'Pripadajo';
$string['bytes'] = 'bajtov';
$string['cancel'] = 'Prekliči';
$string['cancelrequest'] = 'Prekliči zahtevo';
$string['cannotremovedefaultemail'] = 'Privzetega epoštnega naslova ne morete odstraniti';
$string['cantchangepassword'] = 'Oprostite, gesla ne boste mogli spremeniti preko tega vmesnika - prosimo, da namesto tega, uporabite vmesnik ustanove';
$string['captchadescription'] = 'Vnesite znake, ki jih vidite na sliki. Sistem ne loči med velikimi in malimi črkami.';
$string['captchaimage'] = 'CAPTCHA slika';
$string['captchaincorrect'] = 'Vnesite črke, kot so prikazane na sliki';
$string['captchatitle'] = 'CAPTCHA slika';
$string['change'] = 'Spremeni';
$string['changepassword'] = 'Spremeni geslo';
$string['changepasswordinfo'] = 'Pred nadaljevanjem morate spremeniti geslo.';
$string['chooseusernamepassword'] = 'Izberite uporabniško ime in geslo';
$string['chooseusernamepasswordinfo'] = 'Za prijavo v %s potrebujete uporabniško ime in geslo. Prosim, da ju izberete.';
$string['clambroken'] = 'Skrbnik je omogočil preverjanje virusov naloženih predstavnostnih datotek, vendar je nekaj nastavil narobe. Nalaganje datoteke ni bilo uspešno. Skrbnik je bil obveščen po epošti o tej napaki, tako da jo lahko popravi. Poskusite datoteko naložiti pozneje.';
$string['clamdeletedfile'] = 'Datoteka je izbrisana';
$string['clamdeletedfilefailed'] = 'Datoteka ne more biti izbrisana';
$string['clamemailsubject'] = '%s :: Clam AV obvestilo';
$string['clamfailed'] = 'Clam AV se ni zagnal.  Sporočilo o napaki: %s. Tukaj so izhodni podatki, kot jih vrne Clam AV:';
$string['clamlost'] = 'Clam AV je nastavljen tako, da se zažene ob nalaganju datotek, toda navedena pot do Clam AV, %s, je neveljavna.';
$string['clammovedfile'] = 'Datoteka je premaknjena v mapo karantene.';
$string['clamunknownerror'] = 'Clam AV ne deluje zaradi neznane napake.';
$string['collapse'] = 'Skrči';
$string['complete'] = 'Končano';
$string['config'] = 'Nastavi';
$string['confirmdeletetag'] = 'Ali zares želite izbrisati to ključno besedo iz vsega v vašem listovniku?';
$string['confirminvitation'] = 'Potrdi povabilo';
$string['confirmpassword'] = 'Potrdi geslo';
$string['contactus'] = 'Kontakt';
$string['cookiesnotenabled'] = 'Vaš brskalnik nima omogočenih piškotkov, ali pa so piškotki s tega spletišča blokirani. Mahara potrebuje omogočene piškotke, preden se lahko prijavite.';
$string['couldnotgethelp'] = 'Pri prejemanju strani s pomočjo se je zgodila napaka';
$string['country.ad'] = 'Andora';
$string['country.ae'] = 'Združeni arabski emirati';
$string['country.af'] = 'Afghanistan';
$string['country.ag'] = 'Antigva in Barbuda';
$string['country.ai'] = 'Angvila';
$string['country.al'] = 'Albanija';
$string['country.am'] = 'Armenija';
$string['country.an'] = 'Nizozemski Antili';
$string['country.ao'] = 'Angola';
$string['country.aq'] = 'Antarktika';
$string['country.ar'] = 'Argentina';
$string['country.as'] = 'Ameriška Samoa';
$string['country.at'] = 'Avstrija';
$string['country.au'] = 'Avstralija';
$string['country.aw'] = 'Aruba';
$string['country.ax'] = 'Ålandnski otoki';
$string['country.az'] = 'Azerbajdžan';
$string['country.ba'] = 'Bosna in Hercegovina';
$string['country.bb'] = 'Barbados';
$string['country.bd'] = 'Bangladeš';
$string['country.be'] = 'Belgija';
$string['country.bf'] = 'Burkina Faso';
$string['country.bg'] = 'Bolgarija';
$string['country.bh'] = 'Bahrajn';
$string['country.bi'] = 'Burundi';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermudi';
$string['country.bn'] = 'Brunej';
$string['country.bo'] = 'Bolivija';
$string['country.br'] = 'Brazilija';
$string['country.bs'] = 'Bahami';
$string['country.bt'] = 'Butan';
$string['country.bv'] = 'Bouvetovi otoki';
$string['country.bw'] = 'Bocvana';
$string['country.by'] = 'Belorusija';
$string['country.bz'] = 'Belize';
$string['country.ca'] = 'Kanada';
$string['country.cc'] = 'Kokosovi (Keelingovi) otoki';
$string['country.cd'] = 'Kongo, Demokratična republika';
$string['country.cf'] = 'Srednjeafriška republika';
$string['country.cg'] = 'Kongo';
$string['country.ch'] = 'Švica';
$string['country.ci'] = 'Slonokoščena obala';
$string['country.ck'] = 'Cookovi otoki';
$string['country.cl'] = 'Čile';
$string['country.cm'] = 'Kamerun';
$string['country.cn'] = 'Kitajska';
$string['country.co'] = 'Kolumbija';
$string['country.cr'] = 'Kostarika';
$string['country.cs'] = 'Srbija in Črna gora';
$string['country.cu'] = 'Kuba';
$string['country.cv'] = 'Zelenortski otoki';
$string['country.cx'] = 'Božični otoki';
$string['country.cy'] = 'Ciper';
$string['country.cz'] = 'Češka';
$string['country.de'] = 'Nemčija';
$string['country.dj'] = 'Džibuti';
$string['country.dk'] = 'Danska';
$string['country.dm'] = 'Dominika';
$string['country.do'] = 'Dominikanska republika';
$string['country.dz'] = 'Alžirija';
$string['country.ec'] = 'Ekvador';
$string['country.ee'] = 'Estonija';
$string['country.eg'] = 'Egipt';
$string['country.eh'] = 'Zahodna Sahara';
$string['country.er'] = 'Eritreja';
$string['country.es'] = 'Španija';
$string['country.et'] = 'Etiopija';
$string['country.fi'] = 'Finska';
$string['country.fj'] = 'Fidži';
$string['country.fk'] = 'Falklandski otoki (Malvinas)';
$string['country.fm'] = 'Mikronezija';
$string['country.fo'] = 'Ferski otoki';
$string['country.fr'] = 'Francija';
$string['country.ga'] = 'Gabon';
$string['country.gb'] = 'Združeno kraljestvo';
$string['country.gd'] = 'Grenada';
$string['country.ge'] = 'Gruzija';
$string['country.gf'] = 'Francoska Gvajana';
$string['country.gg'] = 'Guernsey';
$string['country.gh'] = 'Gana';
$string['country.gi'] = 'Gibraltar';
$string['country.gl'] = 'Grenlandija';
$string['country.gm'] = 'Gambija';
$string['country.gn'] = 'Gvineja';
$string['country.gp'] = 'Gvadalup';
$string['country.gq'] = 'Ekvatorialna Gvineja';
$string['country.gr'] = 'Grčija';
$string['country.gs'] = 'South Georgia and The South Sandwich Islands';
$string['country.gt'] = 'Gvatemala';
$string['country.gu'] = 'Guam';
$string['country.gw'] = 'Gvineja Bissau';
$string['country.gy'] = 'Gvajana';
$string['country.hk'] = 'Hong Kong';
$string['country.hm'] = 'Heardov otok in McDonaldovi otoki';
$string['country.hn'] = 'Honduras';
$string['country.hr'] = 'Hrvaška';
$string['country.ht'] = 'Haiti';
$string['country.hu'] = 'Madžarska';
$string['country.id'] = 'Indonezija';
$string['country.ie'] = 'Irska';
$string['country.il'] = 'Izrael';
$string['country.im'] = 'Otok Man';
$string['country.in'] = 'Indija';
$string['country.io'] = 'Britansko ozemlje v indijskem oceanu';
$string['country.iq'] = 'Irak';
$string['country.ir'] = 'Iran';
$string['country.is'] = 'Islandija';
$string['country.it'] = 'Italija';
$string['country.je'] = 'Jersey';
$string['country.jm'] = 'Jamajka';
$string['country.jo'] = 'Jordanija';
$string['country.jp'] = 'Japonska';
$string['country.ke'] = 'Kenija';
$string['country.kg'] = 'Kirgizistan';
$string['country.kh'] = 'Kambodža';
$string['country.ki'] = 'Kiribati';
$string['country.km'] = 'Komori';
$string['country.kn'] = 'Saint Kitts in Nevis';
$string['country.kp'] = 'Koreja, Severna';
$string['country.kr'] = 'Koreja, Južna';
$string['country.kw'] = 'Kuvajt';
$string['country.ky'] = 'Kajmanski otoki';
$string['country.kz'] = 'Kazahstan';
$string['country.la'] = 'Laos';
$string['country.lb'] = 'Libanon';
$string['country.lc'] = 'Sveta Lucija';
$string['country.li'] = 'Lihtenštajn';
$string['country.lk'] = 'Šrilanka';
$string['country.lr'] = 'Liberija';
$string['country.ls'] = 'Lesoto';
$string['country.lt'] = 'Litva';
$string['country.lu'] = 'Luksemburg';
$string['country.lv'] = 'Latvija';
$string['country.ly'] = 'Libija';
$string['country.ma'] = 'Maroko';
$string['country.mc'] = 'Monako';
$string['country.md'] = 'Moldavija';
$string['country.mg'] = 'Madagaskar';
$string['country.mh'] = 'Marshallovi otoki';
$string['country.mk'] = 'Makedonija';
$string['country.ml'] = 'Mali';
$string['country.mm'] = 'Mjanmar';
$string['country.mn'] = 'Mongolija';
$string['country.mo'] = 'Makao';
$string['country.mp'] = 'Severni Marijanini otoki';
$string['country.mq'] = 'Martinik';
$string['country.mr'] = 'Mavretanija';
$string['country.ms'] = 'Montserrat';
$string['country.mt'] = 'Malta';
$string['country.mu'] = 'Mauritius';
$string['country.mv'] = 'Maldivi';
$string['country.mw'] = 'Malavi';
$string['country.mx'] = 'Mexico';
$string['country.my'] = 'Malezija';
$string['country.mz'] = 'Mozambik';
$string['country.na'] = 'Namibija';
$string['country.nc'] = 'Nova Kaledonija';
$string['country.ne'] = 'Niger';
$string['country.nf'] = 'Otok Norfolk';
$string['country.ng'] = 'Nigerija';
$string['country.ni'] = 'Nikaragva';
$string['country.nl'] = 'Nizozemska';
$string['country.no'] = 'Norveška';
$string['country.np'] = 'Nepal';
$string['country.nr'] = 'Nauru';
$string['country.nu'] = 'Niue';
$string['country.nz'] = 'Nova Zelandija';
$string['country.om'] = 'Oman';
$string['country.pa'] = 'Panama';
$string['country.pe'] = 'Peru';
$string['country.pf'] = 'Francoska Polinezija';
$string['country.pg'] = 'Papua Nova Gvineja';
$string['country.ph'] = 'Filipini';
$string['country.pk'] = 'Pakistan';
$string['country.pl'] = 'Poljska';
$string['country.pm'] = 'Saint Pierre in Miquelon';
$string['country.pn'] = 'Pitcairn';
$string['country.pr'] = 'Portoriko';
$string['country.ps'] = 'Palestinsko ozemlje, okupirano';
$string['country.pt'] = 'Portugalska';
$string['country.pw'] = 'Palau';
$string['country.py'] = 'Paragvaj';
$string['country.qa'] = 'Katar';
$string['country.re'] = 'Reunion';
$string['country.ro'] = 'Romunija';
$string['country.ru'] = 'Rusija';
$string['country.rw'] = 'Ruanda';
$string['country.sa'] = 'Saudova Arabija';
$string['country.sb'] = 'Salomonovi otoki';
$string['country.sc'] = 'Sejšeli';
$string['country.sd'] = 'Sudan';
$string['country.se'] = 'Švedska';
$string['country.sg'] = 'Singapur';
$string['country.sh'] = 'Sveta Helena';
$string['country.si'] = 'Slovenija';
$string['country.sj'] = 'Svalbard in Jan Mayen';
$string['country.sk'] = 'Slovaška';
$string['country.sl'] = 'Sierra Leone';
$string['country.sm'] = 'San Marino';
$string['country.sn'] = 'Senegal';
$string['country.so'] = 'Somalija';
$string['country.sr'] = 'Surinam';
$string['country.st'] = 'Sao Tome in Principe';
$string['country.sv'] = 'Salvador';
$string['country.sy'] = 'Sirija';
$string['country.sz'] = 'Svazi';
$string['country.tc'] = 'Turks in Caicos otoki';
$string['country.td'] = 'Čad';
$string['country.tf'] = 'Francoska južna ozemlja';
$string['country.tg'] = 'Togo';
$string['country.th'] = 'Tajska';
$string['country.tj'] = 'Tadžikistan';
$string['country.tk'] = 'Tokelau';
$string['country.tl'] = 'Timor-leste';
$string['country.tm'] = 'Turkmenistan';
$string['country.tn'] = 'Tunizija';
$string['country.to'] = 'Tonga';
$string['country.tr'] = 'Turčija';
$string['country.tt'] = 'Trinidad in Tobago';
$string['country.tv'] = 'Tuvalu';
$string['country.tw'] = 'Tajvan';
$string['country.tz'] = 'Tanzanija';
$string['country.ua'] = 'Ukrajina';
$string['country.ug'] = 'Uganda';
$string['country.um'] = 'United States Minor Outlying Islands';
$string['country.us'] = 'Združene države Amerike';
$string['country.uy'] = 'Urugvaj';
$string['country.uz'] = 'Uzbekistan';
$string['country.va'] = 'Sveti sedež (Mestna država Vatikan)';
$string['country.vc'] = 'Saint Vincent in Grenadines';
$string['country.ve'] = 'Venezuela';
$string['country.vg'] = 'Deviški otoki, Britanski';
$string['country.vi'] = 'Deviški otoki, Ameriški';
$string['country.vn'] = 'Vietnam';
$string['country.vu'] = 'Vanuatu';
$string['country.wf'] = 'Wallis in Futuna';
$string['country.ws'] = 'Samoa';
$string['country.ye'] = 'Jemen';
$string['country.yt'] = 'Mayotte';
$string['country.za'] = 'Južna Afrika';
$string['country.zm'] = 'Zambija';
$string['country.zw'] = 'Zimbabve';
$string['date'] = 'Datum';
$string['day'] = 'dan';
$string['days'] = 'dni';
$string['debugemail'] = 'OBVESTILO: To epoštno sporočilo naj bi dobil/a %s (%s) vendar je bilo zaradi nastavitve "sendallemailto" poslano vam.';
$string['decline'] = 'Zavrni';
$string['default'] = 'Privzeto';
$string['delete'] = 'Izbriši';
$string['deleteduser'] = 'Izbrisani uporabnik';
$string['deletetag'] = 'Izbriši <a href="%s">%s</a>';
$string['deletetagdescription'] = 'Vsem enotam v listovniku odstrani to ključno besedo';
$string['description'] = 'Opis';
$string['disable'] = 'Onemogoči';
$string['displayname'] = 'Prikazno ime';
$string['divertingemailto'] = 'Preusmerjanje epošte na %s';
$string['done'] = 'Narejeno';
$string['edit'] = 'Uredi';
$string['editing'] = 'Urejanje';
$string['editmyprofilepage'] = 'Uredi stran profila';
$string['edittag'] = 'Uredi <a href="%s">%s</a>';
$string['edittagdescription'] = 'Posodobljene bodo vse enote v listovniku, ki so označene z "%s"';
$string['edittags'] = 'Uredi ključne besede';
$string['editthistag'] = 'Uredi ključno besedo';
$string['email'] = 'Epošta';
$string['emailaddress'] = 'Epoštni naslov';
$string['emailaddressorusername'] = 'Epoštni naslov ali uporabniško ime';
$string['emailname'] = 'Mahara eListovnik';
$string['emailnotsent'] = 'Pošiljanje kontaktne epošte neuspešno. Sporočilo o napaki: "%s"';
$string['emailtoolong'] = 'Epoštni naslov ne sme biti daljši od 255 znakov';
$string['enable'] = 'Omogoči';
$string['errorprocessingform'] = 'Pri oddajanju obrazca je prišlo do napake. Prosimo, preglejte označena polja, nato poizkusite ponovno.';
$string['expand'] = 'Razširi';
$string['filenotimage'] = 'Datoteka, ki ste jo naložili, ni veljavna slika. Datoteka mora biti v PNG, JPEG ali GIF obliki.';
$string['filetypenotallowed'] = 'Nimate dovoljenj za nalaganje datotek tega tipa. Za več informacij se obrnite na skrbnika spletišča.';
$string['fileunknowntype'] = 'Tipa naložene datoteke ni mogoče določiti. Datoteka je morda pokvarjena ali pa gre za problem z nastavitvami. Prosimo, obrnite se na skrbnika spletišča.';
$string['filter'] = 'Filter';
$string['filterresultsby'] = 'Filtriraj rezultate:';
$string['findfriends'] = 'Najdi prijatelje';
$string['findgroups'] = 'Najdi skupine';
$string['first'] = 'Začetek';
$string['firstname'] = 'Ime';
$string['firstpage'] = 'Prva stran';
$string['forgotpassemaildisabled'] = 'Oprostite, vendar je za vnešeni epoštni naslov ali uporabniško ime epošta onemogočena. Prosimo obrnite se na skrbnika, da vam ponastavi geslo.';
$string['forgotpassemailsendunsuccessful'] = 'Oprostite, zdi se, da epošte ne moremo uspešno poslati. To je naša napaka, prosimo, da v kratkem poskusite znova.';
$string['forgotpassnosuchemailaddressorusername'] = 'Epoštni naslov ali uporabniško ime, ki ste ga vnesli, se ne ujema z nobenim uporabnikom tega spletišča';
$string['forgotpasswordenternew'] = 'Prosimo, vnesite vaše novo geslo za nadaljevanje';
$string['forgotusernamepassword'] = 'Ste pozabili vaše uporabniško ime ali geslo?';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Spoštovani %s,</p>

<p>zahtevali ste uporabniško ime/geslo za račun pri %s.</p>

<p>Uporabniško ime je <strong>%s</strong>.</p>

<p>Če želite ponastaviti geslo, sledite spodnji povezavi:</p>

<p><a href="%s">%s</a></p>

<p>Če niste zahtevali ponastavitve gesla, prosimo, ignorirajte to sporočilo.</p>

<p>Če imate kakršno koli vprašanje v zvezi s prijavnimi podatki, se <a href="%s">obrnite na nas</a>.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['forgotusernamepasswordemailmessagetext'] = 'Spoštovani %s,

zahtevali ste uporabniško ime/geslo za račun pri %s.

Uporabniško ime je %s.

Če želite ponastaviti geslo, sledite spodnji povezavi:

%s

Če niste zahtevali ponastavitve gesla, prosimo, ignorirajte to sporočilo.

Če imate kakršno koli vprašanje v zvezi s prijavnimi podatki, se obrnite na:

%s

S spoštovanjem, %s skrbnik spletišča';
$string['forgotusernamepasswordemailsubject'] = 'Podrobnosti o uporabniškem imenu/geslu za %s';
$string['forgotusernamepasswordtext'] = '<p>Če ste pozabili uporabniško ime ali geslo, vpišite epoštni naslov, naveden v vašem profilu in vam bomo poslali sporočilo, s pomočjo katerega si lahko ustvarite novo geslo.</p> 
<p>Če poznate uporabniško ime in ste pozabili geslo, lahko namesto epoštnega naslova vnesete vaše uporabniško ime.</ p>';
$string['formatpostbbcode'] = 'Svojo objavo lahko oblikujete z  uporabo BBCode. %sVeč informacij%s';
$string['fullname'] = 'Ime in priimek';
$string['go'] = 'Pojdi';
$string['groups'] = 'Skupine';
$string['height'] = 'Višina';
$string['heightshort'] = 'v';
$string['home'] = 'Domov';
$string['image'] = 'Slika';
$string['importedfrom'] = 'Uvoženo iz %s';
$string['incomingfolderdesc'] = 'Datoteke, uvožene iz drugih omrežnih gostiteljev';
$string['installedplugins'] = 'Nameščeni vtičniki';
$string['institution'] = 'Ustanova';
$string['institutionexpirywarning'] = 'Opozorilo: Članstvo v ustanovi bo poteklo';
$string['institutionexpirywarninghtml'] = '<p>Spoštovani %s,</p>
    
<p>Vaše članstvo %s v ustanovi %s bo poteklo v %s.</p>

<p>Če želite podaljšati vaše članstvo, ali imate kakšno drugo vprašanje, se <a href="%s">obrnite na nas</a>.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['institutionexpirywarninghtml_institution'] = '<p>Spoštovani %s,</p>

<p>članstvo osebe %s v ustanovi %s bo poteklo %s.</p>

<p>Če želite podaljšati članstvo v vaši ustanovi ali imate kakršno koli vprašanje v zvezi s tem, se <a href="%s">obrnite na nas</a>.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['institutionexpirywarninghtml_site'] = '<p>Spoštovani %s,</p>

<p>ustanova \'%s\' bo potekla %s.</p>
<p>Morda jih želite obvestiti, naj podaljšajo svoje članstvo %s.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['institutionexpirywarningtext'] = 'Spoštovani %s,

Vaše članstvo %s v ustanovi %s bo poteklo v %s.

Če želite podaljšati vaše članstvo, ali imate kakšno drugo vprašanje, se obrnite na:

%s

S spoštovanjem, %s skrbnik spletišča';
$string['institutionexpirywarningtext_institution'] = 'Spoštovani %s,

članstvo osebe %s v ustanovi %s bo poteklo %s.

Če želite podaljšati članstvo v vaši ustanovi ali imate kakršno koli vprašanje v zvezi s tem, se obrnite na nas.

S spoštovanjem, %s skrbnik spletišča';
$string['institutionexpirywarningtext_site'] = 'Spoštovani %s, ustanova \'%s\' bo potekla v %s. Morda jih želite obvestiti, naj podaljšajo svoje članstvo %s.

S spoštovanjem, %s skrbnik spletišča';
$string['institutionfull'] = 'Ustanova, ki ste jo izbrali, registracij žal ne sprejema več.';
$string['institutionmemberconfirmmessage'] = 'Postali ste član %s.';
$string['institutionmemberconfirmsubject'] = 'Potrditev članstva v ustanovi';
$string['institutionmemberrejectmessage'] = 'Zahteva za članstvo v %s je bila zavrnjena.';
$string['institutionmemberrejectsubject'] = 'Zavrnitev zahteve za članstvo v ustanovi';
$string['institutionmembership'] = 'Članstvo v ustanovah';
$string['institutionmembershipdescription'] = 'Ustanove, katerih član ste, bodo navedene tukaj. Lahko tudi zahtevate članstvo v ustanovi, in če vas je določena ustanova povabila, da se pridružite, lahko sprejmete ali zavrnete povabilo.';
$string['institutionmembershipexpirywarning'] = 'Opozorilo o izteku članstva v ustanovi';
$string['institutionmembershipexpirywarninghtml'] = '<p>Spoštovani %s,</p>

<p>Vaše članstvo v %s na %s bo poteklo %s.</p> 

<p>Če želite podaljšati članstvo ali imate kakršno koli vprašanje v zvezi s tem, se <a href="%s">obrnite na nas</a>.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>';
$string['institutionmembershipexpirywarningtext'] = 'Spoštovani %s,

Vaše članstvo v %s na %s bo poteklo %s.

Če želite podaljšati članstvo ali imate kakršno koli vprašanje v zvezi s tem, se obrnite na nas.

S spoštovanjem, %s skrbnik spletišča';
$string['invalidsesskey'] = 'Napačen ključ seje';
$string['invitedgroup'] = 'povabljen/a v skupino';
$string['invitedgroups'] = 'povabljen/a v skupine';
$string['itemstaggedwith'] = 'Enote označene z "%s"';
$string['javascriptnotenabled'] = 'Vaš brskalnik za to spletišče nima omogočenega JavaScript. Mahara zahteva, da je JavaScript omogočen, preden se lahko prijavite.';
$string['joininstitution'] = 'Pridruži se ustanovi';
$string['language'] = 'Jezik';
$string['last'] = 'Konec';
$string['lastminutes'] = 'Zadnjih %s minut';
$string['lastname'] = 'Priimek';
$string['lastpage'] = 'Zadnja stran';
$string['leaveinstitution'] = 'Zapusti ustanovo';
$string['linksandresources'] = 'Povezave in viri';
$string['loading'] = 'Nalaganje ...';
$string['loggedinusersonly'] = 'Samo prijavljeni uporabniki';
$string['loggedoutok'] = 'Odjava je bila uspešna';
$string['login'] = 'Prijava';
$string['loginfailed'] = 'Niste navedli ustreznih podatkov za prijavo. Prosimo, preverite, da sta uporabniško ime in geslo pravilna.';
$string['loginto'] = 'Prijavite se v sistem %s';
$string['logout'] = 'Odjava';
$string['lostusernamepassword'] = 'Izgubljeno uporabniško ime/geslo';
$string['memberofinstitutions'] = 'Član %s';
$string['membershipexpiry'] = 'Članstvo poteče';
$string['message'] = 'Sporočilo';
$string['messagesent'] = 'Sporočilo je bilo poslano';
$string['months'] = 'mesecev';
$string['more...'] = 'Več...';
$string['mustspecifyoldpassword'] = 'Navesti morate vaše trenutno geslo';
$string['myfriends'] = 'Moji prijatelji';
$string['mygroups'] = 'Moje skupine';
$string['myportfolio'] = 'Moj listovnik';
$string['mytags'] = 'Moje ključne besede';
$string['myviews'] = 'Moji pogledi';
$string['name'] = 'Ime';
$string['namedfieldempty'] = 'Zahtevano polje "%s" je prazno';
$string['newpassword'] = 'Novo geslo';
$string['next'] = 'Naprej';
$string['nextpage'] = 'Naslednja stran';
$string['no'] = 'Ne';
$string['nodeletepermission'] = 'Nimate dovoljenj za brisanje tega izdelka';
$string['noeditpermission'] = 'Nimate dovoljenj za urejanje tega izdelka';
$string['noenddate'] = 'Ni zaključka';
$string['nohelpfound'] = 'Ne najdem pomoči za ta element';
$string['nohelpfoundpage'] = 'Ne najdem pomoči za to stran';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Spoštovani %s,</p>

<p>Niste več član %s.</p>
<p>Še naprej lahko uporabljate %s z vašim trenutnim uporabniškim imenom %s, toda spremeniti morate geslo računa.</p>

<p>Prosimo, sledite spodnji povezavi za nadaljevanje ponastavitve.</p>

<p><a href="http://localhost/mahara120lang/forgotpass.php?key=%s">http://localhost/mahara120lang/forgotpass.php?key=%s</a></p>

<p>Če imate kakršno koli vprašanje v zvezi s postopkom ponastavitve, se <a href="http://localhost/mahara120lang/contact.php">obrnite na nas</a>.</p>

<p>S spoštovanjem, %s skrbnik spletišča</p>

<p><a href="http://localhost/mahara120lang/forgotpass.php?key=%s">http://localhost/mahara120lang/forgotpass.php?key=%s</a></p>';
$string['noinstitutionsetpassemailmessagetext'] = 'Spoštovani %s,

Niste več član %s.
Še naprej lahko uporabljate %s z vašim trenutnim uporabniškim imenom %s, toda spremeniti morate geslo računa.

Prosimo, sledite spodnji povezavi za nadaljevanje ponastavitve.

http://localhost/mahara120lang/forgotpass.php?key=%s

Če imate kakršno koli vprašanje v zvezi s postopkom ponastavitve, se obrnite na:

http://localhost/mahara120lang/contact.php

S spoštovanjem, %s skrbnik spletišča

http://localhost/mahara120lang/forgotpass.php?key=%s';
$string['noinstitutionsetpassemailsubject'] = '%s: Članstvo v %s';
$string['none'] = 'Brez';
$string['noresultsfound'] = 'Ne najdem rezultatov';
$string['nosendernamefound'] = 'Oddano brez imena pošiljatelja';
$string['nosessionreload'] = 'Osveži stran za prijavo';
$string['nosuchpasswordrequest'] = 'Ni take zahteve za geslo';
$string['notifications'] = 'Obvestila';
$string['notinstallable'] = 'Namestitev ni možna!';
$string['notinstalledplugins'] = 'Nenameščeni vtičniki';
$string['notphpuploadedfile'] = 'Datoteka se je med nalaganjem izgubila. To se ne bi smelo zgoditi. Za več informacij se obrnite na skrbnika spletišča.';
$string['numitems'] = '%s enot';
$string['oldpassword'] = 'Trenutno geslo';
$string['onlineusers'] = 'Prijavljeni uporabniki';
$string['optionalinstitutionid'] = 'ID ustanove (neobvezno)';
$string['password'] = 'Geslo';
$string['passwordchangedok'] = 'Geslo je uspešno spremenjeno';
$string['passwordhelp'] = 'Geslo, ki ga uporabite za dostop do sistema.';
$string['passwordnotchanged'] = 'Niste spremenili gesla. Prosimo, izberite novo geslo';
$string['passwordsaved'] = 'Vaše novo geslo je shranjeno';
$string['passwordsdonotmatch'] = 'Gesli se ne ujemata';
$string['passwordtooeasy'] = 'Vaše geslo je preveč enostavno! Prosimo, izberite težje geslo';
$string['pendingfriend'] = 'čakajoči prijatelj';
$string['pendingfriends'] = 'čakajoči prijatelji';
$string['phpuploaderror'] = 'Med nalaganjem datoteke se je zgodila napaka: %s (Koda napake %s)';
$string['phpuploaderror_1'] = 'Naložena datoteka presega navodilo MAX_FILE_SIZE v datoteki php.ini.';
$string['phpuploaderror_2'] = 'Naložena datoteka presega navodilo MAX_FILE_SIZE, ki je bilo določeno v HTML obrazcu.';
$string['phpuploaderror_3'] = 'Datoteka je bila naložena le delno.';
$string['phpuploaderror_4'] = 'Nobena datoteka ni bila naložena.';
$string['phpuploaderror_6'] = 'Manjkajoča začasna mapa.';
$string['phpuploaderror_7'] = 'Neuspešno zapisovanje datoteke na disk.';
$string['phpuploaderror_8'] = 'Nalaganje datoteke ustavljeno zaradi končnice.';
$string['pleasedonotreplytothismessage'] = 'Prosimo, ne odgovarjajte na to sporočilo.';
$string['plugindisabled'] = 'Vtičnik onemogočen';
$string['pluginenabled'] = 'Vtičnik omogočen';
$string['pluginnotenabled'] = 'Vtičnik ni omogočen. Najprej morate omogočiti vtičnik %s.';
$string['plugintype'] = 'Tip vtičnika';
$string['preferences'] = 'Nastavitve';
$string['preferredname'] = 'Prikazno ime';
$string['previous'] = 'Nazaj';
$string['prevpage'] = 'Prejšnja stran';
$string['primaryemailinvalid'] = 'Vaš privzeti epoštni naslov je napačen';
$string['privacystatement'] = 'Izjava o zasebnosti';
$string['processing'] = 'Obdelujem';
$string['profile'] = 'profil';
$string['profileimage'] = 'Slika profila';
$string['pwchangerequestsent'] = 'V kratkem bi morali prejeti epošto s povezavo, ki jo lahko uporabite za spremembo gesla za vaš račun';
$string['quarantinedirname'] = 'karantena';
$string['query'] = 'poizvedba';
$string['querydescription'] = 'Iskane besede';
$string['quota'] = 'Kvota';
$string['quotausage'] = 'Uporabljeno <span id="quota_used">%s</span> od kvote <span id="quota_total">%s</span>.';
$string['reallyleaveinstitution'] = 'Ali ste prepričani, da želite zapustiti to ustanovo?';
$string['reason'] = 'Razlog';
$string['recentupdates'] = 'Nedavne nadgradnje';
$string['register'] = 'Registracija';
$string['registeringdisallowed'] = 'Oprostite, trenutno se ne morete registrirati';
$string['registerstep1description'] = 'Dobrodošli! Za uporabo spletišča se morate najprej registrirati. Strinjati se morate tudi s <a href="terms.php">pravili in pogoji</a>. Podatki, ki jih zbiramo tukaj, bodo shranjeni v skladu z <a href="privacy.php">izjavo o zasebnosti</a>.';
$string['registerstep3fieldsmandatory'] = '<h3>Izpolnite obvezna polja profila</h3><p>Naslednja polja so obvezna. Morate jih izpolniti, sicer registracija ni popolna.</p>';
$string['registerstep3fieldsoptional'] = '<h3>Izberite neobvezno sliko profila</h3><p>Uspešno ste registrirani pri Mahara! Izberete lahko tudi ikono profila, ki se prikaže kot vaš avatar.</p>';
$string['registrationcomplete'] = 'Zahvaljujemo se vam za registracijo pri %s';
$string['registrationnotallowed'] = 'Ustanova, ki ste jo izbrali, žal ne dovoljuje samo-registracije.';
$string['reject'] = 'Zavrni';
$string['remotehost'] = 'Oddaljeni gostitelj %s';
$string['remove'] = 'Odstrani';
$string['republish'] = 'Objavi';
$string['requestmembershipofaninstitution'] = 'Zahteva za članstvo v ustanovi';
$string['requiredfieldempty'] = 'Zahtevano polje je prazno';
$string['result'] = 'rezultat';
$string['results'] = 'rezultati';
$string['returntosite'] = 'Nazaj na stran';
$string['save'] = 'Shrani';
$string['search'] = 'Iskanje';
$string['searchresultsfor'] = 'Preišči rezultate';
$string['searchusers'] = 'Iskanje uporabnikov';
$string['select'] = 'Izberi';
$string['selectatagtoedit'] = 'Izberite ključno besedo za urejanje';
$string['selfsearch'] = 'Preišči Moj listovnik';
$string['send'] = 'Pošlji';
$string['sendmessage'] = 'Pošlji sporočilo';
$string['sendrequest'] = 'Pošlji zahtevo';
$string['sessiontimedout'] = 'Seja je potekla, zato vas prosimo, da za nadaljevanje vnesete svoje prijavne podatke.';
$string['sessiontimedoutpublic'] = 'Seja je potekla. Lahko se <a href="%s">prijavite</a> za nadaljevanje z brskanjem.';
$string['sessiontimedoutreload'] = 'Seja je potekla. Osvežite stran in se ponovno prijavite.';
$string['settings'] = 'Nastavitve';
$string['settingssaved'] = 'Nastavitve shranjene';
$string['settingssavefailed'] = 'Ne morem shraniti nastavitev';
$string['showtags'] = 'Prikaži moje ključne besede';
$string['siteadministration'] = 'Skrbništvo';
$string['siteclosed'] = 'Stran je začasno zaprta zaradi nadgradenj podatkovne baze. Samo administratorji strani se lahko prijavijo.';
$string['siteclosedlogindisabled'] = 'Stran je začasno zaprta zaradi nadgradenj podatkovne baze. <a href="%s">Izvedite nadgradnjo zdaj.</a>';
$string['sitecontentnotfound'] = '%s besedilo ni na voljo';
$string['sizeb'] = 'B';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'kB';
$string['sizemb'] = 'MB';
$string['sortalpha'] = 'Uredi ključne besede po abecedi';
$string['sortfreq'] = 'Uredi ključne besede po pogostosti';
$string['sortresultsby'] = 'Uredi rezultate po:';
$string['strftimenotspecified'] = 'Ni določeno';
$string['studentid'] = 'ID številka';
$string['subject'] = 'Predmet';
$string['submit'] = 'Oddaj';
$string['system'] = 'Sistem';
$string['tagdeletedsuccessfully'] = 'Ključna beseda uspešno izbrisana';
$string['tagfilter_all'] = 'Vse';
$string['tagfilter_file'] = 'Datoteke';
$string['tagfilter_image'] = 'Slike';
$string['tagfilter_text'] = 'Besedilo';
$string['tagfilter_view'] = 'Pogledi';
$string['tags'] = 'Ključne besede';
$string['tagsdesc'] = 'Vpišite ključne besede tega elementa, ločene z vejico.';
$string['tagsdescprofile'] = 'Vpišite ključne besede tega elementa, ločene z vejico. Elementi, označeni s ključno besedo \'profile\', bodo prikazani v stranski vrstici.';
$string['tagupdatedsuccessfully'] = 'Ključna beseda uspešno posodobljena';
$string['termsandconditions'] = 'Pravila in pogoji';
$string['thisistheprofilepagefor'] = 'To je stran s profilom osebe %s';
$string['unknownerror'] = 'Zgodila se je neznana napaka (0x20f91a0)';
$string['unreadmessage'] = 'neprebrano sporočilo';
$string['unreadmessages'] = 'neprebranih sporočil';
$string['update'] = 'Posodobi';
$string['updatefailed'] = 'Posodabljanje neuspešno';
$string['upload'] = 'Naloži';
$string['uploadedfiletoobig'] = 'Datoteka je prevelika. Prosite skrbnika spletišča, da vam posreduje več informacij.';
$string['useradministration'] = 'Skrbništvo uporabnikov';
$string['username'] = 'Uporabniško ime';
$string['usernamehelp'] = 'Uporabniško ime, ki ste ga dobili za dostop do sistema.';
$string['users'] = 'Uporabniki';
$string['view'] = 'Pogled';
$string['viewmyprofilepage'] = 'Ogled strani profila';
$string['views'] = 'Pogledi';
$string['virusfounduser'] = 'Naloženo datoteko, %s, je preiskal protivirusni program in ugotovil, da je okužena! Zato nalaganje datoteke NI bilo uspešno.';
$string['virusrepeatmessage'] = 'Uporabnik %s je naložil več datotek, ki jih je preiskal protivirusni program in ugotovil, da so okužene.';
$string['virusrepeatsubject'] = 'Opozorilo: %s ponavljajoče nalaga viruse.';
$string['weeks'] = 'tednov';
$string['width'] = 'Širina';
$string['widthshort'] = 'š';
$string['years'] = 'let';
$string['yes'] = 'Da';
$string['youareamemberof'] = 'Ste član %s';
$string['youaremasqueradingas'] = 'Izdajate se za %s.';
$string['youhavebeeninvitedtojoin'] = 'Povabljeni ste bili, da se pridružite %s';
$string['youhavenottaggedanythingyet'] = 'Ničesar še niste označili';
$string['youhaverequestedmembershipof'] = 'Zahtevali ste članstvo v %s';
$string['youraccounthasbeensuspended'] = 'Račun je blokiran';
$string['youraccounthasbeensuspendedreasontext'] = 'Račun pri %s je blokiral/a %s. Razlog:

%s';
$string['youraccounthasbeensuspendedtext2'] = 'Račun pri %s je blokiral/a %s.';
$string['youraccounthasbeenunsuspended'] = 'Račun je znova aktiviran';
$string['youraccounthasbeenunsuspendedtext2'] = 'Račun pri %s je znova aktiviran. Ponovno se lahko prijavite in uporabljate spletišče.';
$string['yournewpassword'] = 'Vaše novo geslo';
$string['yournewpasswordagain'] = 'Vaše novo geslo ponovno';
?>
