<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['internal'] = 'Notranje';
$string['title'] = 'Notranje';
$string['description'] = 'Overjanje glede na Maharino podatkovno bazo';

$string['completeregistration'] = 'Popolna registracija';
$string['emailalreadytaken'] = 'Ta epoštni naslov je tukaj že registriran';
$string['iagreetothetermsandconditions'] = 'Strinjam se s Pravili in pogoji';
$string['passwordformdescription'] = 'Geslo mora biti dolgo vsaj šest znakov in mora vsebovati vsaj eno številko in dve črki';
$string['passwordinvalidform'] = 'Geslo mora biti dolgo vsaj šest znakov in mora vsebovati vsaj eno številko in dve črki';
$string['registeredemailsubject'] = 'Registrirali ste se pri %s';
$string['registeredemailmessagetext'] = 'Pozdravljeni %s,

Hvala za registracijo računa na %s. Prosimo, sledite spodnji povezavi
za dokončanje registracije:

%sregister.php?key=%s

Povezava bo potekla v 24 urah.

--
S spoštovanjem,
%s ekipa';
$string['registeredemailmessagehtml'] = '<p>Pozdravljeni %s,</p>
<p>Hvala za registracijo računa na %s. Prosimo, sledite spodnji povezavi
za dokončanje registracije:</p>
<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>
<p>Povezava bo potekla v 24 urah.</p>

<pre>--
S spoštovanjem,
%s ekipa</pre>';
$string['registeredok'] = '<p>Registracija je bila uspešna. Prosimo, preverite epoštni račun za navodila, kako aktivirati vsš račun</p>';
$string['registrationnosuchkey'] = 'Oprostite, izgleda, da registracija s tem ključem ne obstaja. Mogoče ste čakali več kot 24 ur za dokončanje registracije? Drugače gre za našo krivdo.';
$string['registrationunsuccessful'] = 'Oprostite, vaš poizkus registracije je bil neuspešen. To je naša krivda, ne vaša. Prosimo, poizkusite ponovno kasneje.';
$string['usernamealreadytaken'] = 'Oprostite, to uporabniško ime je že zasedeno';
$string['usernameinvalidform'] = 'Uporabniško ime lahko vsebuje črke, številke in najobičajnejše simbole, in mora biti dolgo od 3 do 30 znakov. Presledki niso dovoljeni.';
$string['youmaynotregisterwithouttandc'] = 'Ne morete se registrirati, dokler se ne strinjate s spoštovanjem <a href="terms.php">Pravil in pogojev</a>';
$string['youmustagreetothetermsandconditions'] = 'Strinjati se morate s <a href="terms.php">Pravili in pogoji</a>';

?>
