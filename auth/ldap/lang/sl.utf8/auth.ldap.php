<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['contexts'] = 'Konteksti';
$string['description'] = 'Overjanje glede na LDAP strežnik';
$string['distinguishedname'] = 'Razločevalno ime';
$string['hosturl'] = 'URL gostitelja';
$string['ldapfieldforemail'] = 'LDAP polje za epošto';
$string['ldapfieldforfirstname'] = 'LDAP polje za ime';
$string['ldapfieldforsurname'] = 'LDAP polje za priimek';
$string['ldapversion'] = 'LDAP različica';
$string['notusable'] = 'Prosimo, namestite PHP LDAP razširitev';
$string['password'] = 'Geslo';
$string['searchsubcontexts'] = 'Iskanje podskladnosti';
$string['title'] = 'LDAP';
$string['updateuserinfoonlogin'] = 'Ob prijavi posodobi uporabniške podatke';
$string['userattribute'] = 'Lastnosti uporabnika';
$string['usertype'] = 'Tip uporabnika';
$string['weautocreateusers'] = 'Mi samodejno ustvarimo uporabnike';

$string['cannotconnect'] = 'Ne morem se povezati na noben LDAP strežnik';
?>
