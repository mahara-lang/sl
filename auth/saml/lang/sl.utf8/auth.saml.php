<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anželj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['defaultinstitution'] = 'Privzeta ustanova';
$string['description'] = 'Overjanje glede na SAML 2.0 IdP storitev';
$string['errnosamluser'] = 'Ne najdem nobenega uporabnika';
$string['errorbadconfig'] = 'SimpleSAMLPHP config mapa %s ni pravilna.';
$string['errorbadcombo'] = 'Samodejno ustvarjanje uporabnikov lahko izberete le, če niste izbrali možnosti oddaljeni uporabnik';
$string['errorbadinstitution'] = 'Ustanova za povezovanje uporabnikov ni razrešena';
$string['errorbadlib'] = 'SimpleSAMLPHP lib mapa %s ni pravilna.';
$string['errorretryexceeded'] = 'Preseženo največje število ponovnih poizkusov (%s) - verjetno gre za težave s storitvijo Identity Service';
$string['institutionattribute'] = 'Lastnost ustanove (vsebuje "%s")';
$string['institutionregex'] = 'Izvedi delno ujemanje nizov z imenom ustanove';
$string['institutionvalue'] = 'Vrednost ustanove za preverjanje glede na lastnosti';
$string['notusable'] = 'Prosimo, namestite SimpleSAMLPHP SP knjižnice';
$string['samlfieldforemail'] = 'SSO polje za epošto';
$string['samlfieldforfirstname'] = 'SSO polje za ime';
$string['samlfieldforsurname'] = 'SSO polje za priimek';
$string['simplesamlphpconfig'] = 'SimpleSAMLPHP config mapa';
$string['simplesamlphplib'] = 'SimpleSAMLPHP lib mapa';
$string['title'] = 'SAML';
$string['updateuserinfoonlogin'] = 'Ob prijavi posodobi lastnosti uporabnika';
$string['userattribute'] = 'Lastnosti uporabnika';
$string['weautocreateusers'] = 'Mi samodejno ustvarimo uporabnike';
$string['remoteuser'] = 'Ujemanje polja uporabniško ime z uporabniškim imenom oddaljenega uporabnika';
?>
