<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/sl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @copyright  (C) 2006-2010 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Body'] = 'Telo';
$string['Close'] = 'Zapri';
$string['Closed'] = 'Zaprto';
$string['Count'] = 'Število';
$string['Key'] = 'Ključ';
$string['Moderators'] = 'Moderatorji';
$string['Open'] = 'Odpri';
$string['Order'] = 'Vrsti red';
$string['Post'] = 'Objava';
$string['Poster'] = 'Objavil/a';
$string['Posts'] = 'Objave';
$string['Reply'] = 'Odgovor';
$string['Sticky'] = 'Lepljivo';
$string['Subject'] = 'Predmet';
$string['Subscribe'] = 'Naroči se';
$string['Subscribed'] = 'Naročen/a';
$string['Topic'] = 'Tema';
$string['Topics'] = 'Teme';
$string['Unsticky'] = 'Nelepljivo';
$string['Unsubscribe'] = 'Odjavi naročnino';
$string['addpostsuccess'] = 'Objava dodana uspešno';
$string['addtitle'] = 'Dodaj forum';
$string['addtopic'] = 'Dodaj temo';
$string['addtopicsuccess'] = 'Tema uspešno dodana';
$string['autosubscribeusers'] = 'Samodejno naročim uporabnike?';
$string['autosubscribeusersdescription'] = 'Izberite, ali bodo uporabniki v skupini samodejno naročeni na ta forum';
$string['cantaddposttoforum'] = 'Nimate dovoljenj za objavljanje v tem forumu';
$string['cantaddposttotopic'] = 'Nimate dovoljenj za objavljanje v tej temi';
$string['cantaddtopic'] = 'Nimate dovoljenj za dodajanje tem v ta forum';
$string['cantdeletepost'] = 'Nimate dovoljenj za brisanje objav v tem forumu';
$string['cantdeletethispost'] = 'Nimate dovoljenj za brisanje te objave';
$string['cantdeletetopic'] = 'Nimate dovoljenj za brisanje tem v tem forumu';
$string['canteditpost'] = 'Nimate dovoljenj za urejanje te objave';
$string['cantedittopic'] = 'Nimate dovoljenj za urejanje te teme';
$string['cantfindforum'] = 'Ne najdem foruma z id številko %s';
$string['cantfindpost'] = 'Ne najdem objave z id številko %s';
$string['cantfindtopic'] = 'Ne najdem teme z id številko %s';
$string['cantviewforums'] = 'Nimate dovoljenj za ogled forumov v tej skupini';
$string['cantviewtopic'] = 'Nimate dovoljenj za ogled tem v tem forumu';
$string['chooseanaction'] = 'Izberi dejanje';
$string['clicksetsubject'] = 'Kliknite za nastavitev predmeta';
$string['closeddescription'] = 'Na zaprte teme lahko odgovarjajo samo moderatorji in skrbniki skupin';
$string['createtopicusersdescription'] = 'Če je nastavljeno na "Vsi člani skupine", potem lahko kdorkoli ustvarja nove teme in odgovarja na obstoječe teme. Če je nastavljeno na "Moderatorji in skrbniki skupin", potem lahko odprejo nove teme samo moderatorji in skrbniki skupin, ko pa se teme ustvarjene, lahko kdorkoli odgovarja nanje.';
$string['currentmoderators'] = 'Trenutni moderatorji';
$string['defaultforumdescription'] = '%s forum splošnih razprav';
$string['defaultforumtitle'] = 'Splošna razprava';
$string['deleteforum'] = 'Izbriši forum';
$string['deletepost'] = 'Izbriši objavo';
$string['deletepostsuccess'] = 'Objava uspešno izbrisana';
$string['deletepostsure'] = 'Ali zares želite narediti to? Brisanja ne bo mogoče razveljaviti.';
$string['deletetopic'] = 'Izbriši temo';
$string['deletetopicsuccess'] = 'Tema uspešno izbrisana';
$string['deletetopicsure'] = 'Ali zares želite narediti to? Brisanja ne bo mogoče razveljaviti.';
$string['deletetopicvariable'] = 'Izbriši temo \'%s\'';
$string['editpost'] = 'Uredi objavo';
$string['editpostsuccess'] = 'Objava uspešno urejena';
$string['editstothispost'] = 'Urejanja te objave:';
$string['edittitle'] = 'Uredi forum';
$string['edittopic'] = 'Uredi temo';
$string['edittopicsuccess'] = 'Tema uspešno urejena';
$string['forumname'] = 'Ime foruma';
$string['forumposthtmltemplate'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s osebe %s</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">Odgovori na objavo na spletu</a></p> <p><a href="%s">Odjavi se od %s</a></p> </div>';
$string['forumposttemplate'] = '%s osebe %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ 
Za ogled in odgovor na objavo na spletu, kliknite povezavo: %s Za odjavo od %s, kliknite: %s';
$string['forumsuccessfulsubscribe'] = 'Naročanje na forum uspešno';
$string['forumsuccessfulunsubscribe'] = 'Odjavljanje naročnine na forum uspešno';
$string['gotoforums'] = 'Obišči forume';
$string['groupadminlist'] = 'Skrbniki skupine:';
$string['groupadmins'] = 'Skrbniku skupine';
$string['lastpost'] = 'Zadnja objava';
$string['latestforumposts'] = 'Zadnje objave na forumu';
$string['moderatorsandgroupadminsonly'] = 'Samo moderatorji in skrbniki skupin';
$string['moderatorsdescription'] = 'Moderatorji lahko urejajo in brišejo teme in objave. Lahko pa tudi odprejo, zaprejo, označijo in odznačijo lepljive teme';
$string['moderatorslist'] = 'Moderatorji:';
$string['name'] = 'Forum';
$string['nameplural'] = 'Forumi';
$string['newforum'] = 'Nov forum';
$string['newforumpostby'] = '%s: %s: Nova objava osebe %s v forumu';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newpost'] = 'Nova objava:';
$string['newtopic'] = 'Nova tema';
$string['noforumpostsyet'] = 'V tej skupini še ni objav';
$string['noforums'] = 'V tej skupini še ni forumov';
$string['notopics'] = 'V tem forumu še ni tem';
$string['orderdescription'] = 'Izberite na katerem mestu naj bo forum, v primerjavi z drugimi forumi';
$string['postbyuserwasdeleted'] = 'Objava osebe %s je bila izbrisana';
$string['postdelay'] = 'Odlog objave';
$string['postdelaydescription'] = 'Najkrajši čas (v minutah), ki mora preteči, preden je nova objava poslana vsem naročnikom foruma. Avtor objave lahko med tem časom objavo ureja.';
$string['postedin'] = '%s je objavil/a v %s';
$string['postreply'] = 'Odgovor na objavo';
$string['postsvariable'] = 'Objave: %s';
$string['potentialmoderators'] = 'Možni moderatorji';
$string['re'] = 'Odg: %s';
$string['regulartopics'] = 'Običajne teme';
$string['replyforumpostnotificationsubject'] = 'Odg: %s: %s: %s';
$string['replyto'] = 'Odgovori:';
$string['replytotopicby'] = '%s: %s: Odgovor na "%s" osebe %s';
$string['stickydescription'] = 'Lepljive teme se na vrhu vsake strani';
$string['stickytopics'] = 'Lepljive teme';
$string['strftimerecentfullrelative'] = '%%v, %%k:%%M';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['subscribetoforum'] = 'Naroči se na forum';
$string['subscribetotopic'] = 'Naroči se na temo';
$string['today'] = 'Danes';
$string['topicclosedsuccess'] = 'Teme uspešno zaprte';
$string['topicisclosed'] = 'Tema je zaprta. Samo moderatorji in skrbniki skupin lahko objavljajo nove odgovore';
$string['topiclower'] = 'tema';
$string['topicopenedsuccess'] = 'Teme uspešno odprte';
$string['topicslower'] = 'teme';
$string['topicstickysuccess'] = 'Tema uspešno označena kot lepljiva';
$string['topicsubscribesuccess'] = 'Naročanje na teme uspešno';
$string['topicsuccessfulunsubscribe'] = 'Odjava naročnine uspešna';
$string['topicunstickysuccess'] = 'Tema uspešno odznačena kot lepljiva';
$string['topicunsubscribesuccess'] = 'Odjavljanje naročnine na teme uspešno';
$string['topicupdatefailed'] = 'Posodobitev tem neuspešna';
$string['typenewpost'] = 'Nova objava na forumu';
$string['unsubscribefromforum'] = 'Odjavi naročnino foruma';
$string['unsubscribefromtopic'] = 'Odjavi naročnino teme';
$string['updateselectedtopics'] = 'Posodobi izbrane teme';
$string['whocancreatetopics'] = 'Kdo lahko ustvari teme';
$string['yesterday'] = 'Včeraj';
$string['youarenotsubscribedtothisforum'] = 'Na forum niste naročeni';
$string['youarenotsubscribedtothistopic'] = 'Na temo niste naročeni';
$string['youcannotunsubscribeotherusers'] = 'Drugim uporabnikom ne morete odjaviti naročnine';
?>
